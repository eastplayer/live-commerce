function changeImage(that) {
    var image = $(that).attr("data-image");
    if (image) {
        $("#serviceImage").attr("src", image);
    }
}