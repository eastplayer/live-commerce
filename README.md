# Installation

* PHP version 7.2 or higher
* PDO PHP Extension (and relevant driver for the database you want to connect to)
* cURL PHP Extension
* OpenSSL PHP Extension
* Mbstring PHP Extension
* ZipArchive PHP Extension
* GD PHP Extension
* SimpleXML PHP Extension

* Migrate database: database.sql
* Config Database string: config/config.php

For development, please also install docker & docker-compose
## Running in local
