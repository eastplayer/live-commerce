<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/404.htm */
class __TwigTemplate_d3c12a1234af951be635700d3c4fc2655ea2dad59b57e84fb2bc752bf1182ecf extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"bizagn--404 d-flex align-items-center justify-content-center\" data-bg-img=\"";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["assets/images/404.png"]);
        echo "\">
    <div class=\"bizagn--404-cover\">
        <h1>404</h1>
        <h2>";
        // line 4
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Xin lỗi! Không tìm tháy trang"]);
        echo "</h2>
        <p>";
        // line 5
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Oops! Trang bạn tìm có thể đã bị thay đổi, bị xóa hoặc không tồn tại. Vui lòng quay về trang chủ!"]);
        echo "</p>
        <div class=\"bizagn--search\">
            <a class=\"btn btn-primary\" href=\"";
        // line 7
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\">
                <i class=\"fa fa-home\"></i> ";
        // line 8
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Trang chủ"]);
        echo "
            </a>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/404.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 8,  55 => 7,  50 => 5,  46 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"bizagn--404 d-flex align-items-center justify-content-center\" data-bg-img=\"{{'assets/images/404.png'|_}}\">
    <div class=\"bizagn--404-cover\">
        <h1>404</h1>
        <h2>{{'Xin lỗi! Không tìm tháy trang'|_}}</h2>
        <p>{{'Oops! Trang bạn tìm có thể đã bị thay đổi, bị xóa hoặc không tồn tại. Vui lòng quay về trang chủ!'|_}}</p>
        <div class=\"bizagn--search\">
            <a class=\"btn btn-primary\" href=\"{{'index'|page}}\">
                <i class=\"fa fa-home\"></i> {{'Trang chủ'|_}}
            </a>
        </div>
    </div>
</div>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/404.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array();
        static $filters = array("_" => 1, "page" => 7);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                [],
                ['_', 'page'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
