<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/plugins/pcms/seo/components/seo/social.htm */
class __TwigTemplate_795cbab0f378f1b21e7023cddc15eca6e78c9ff1fd3065bff2ec74b9e50360bd extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
<meta property=\"og:type\" content=\"";
        // line 2
        echo ((twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [$this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_type", [], "any", false, false, true, 2), 2, $this->source)]))) ? (twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [$this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_type", [], "any", false, false, true, 2), 2, $this->source)]))) : ("website"));
        echo "\" /> 
<meta property=\"og:title\" content=\"";
        // line 3
        echo twig_escape_filter($this->env, _twig_default_filter(twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [((twig_get_attribute($this->env, $this->source,         // line 4
($context["viewBag"] ?? null), "og_title", [], "any", false, false, true, 4)) ? (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_title", [], "any", false, false, true, 4)) : (((twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "meta_title", [], "any", false, false, true, 4)) ? (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "meta_title", [], "any", false, false, true, 4)) : (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "title", [], "any", false, false, true, 4)))))])), "SEO"), "html", null, true);
        // line 6
        echo "\" />
<meta property=\"og:description\" content=\"";
        // line 7
        echo twig_escape_filter($this->env, _twig_default_filter(twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [((twig_get_attribute($this->env, $this->source,         // line 8
($context["viewBag"] ?? null), "og_description", [], "any", false, false, true, 8)) ? (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_description", [], "any", false, false, true, 8)) : (((twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "meta_description", [], "any", false, false, true, 8)) ? (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "meta_description", [], "any", false, false, true, 8)) : (twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "site_description", [], "any", false, false, true, 8)))))])), "SEO"), "html", null, true);
        // line 10
        echo "\" />
<meta property=\"og:image\" content=\"";
        // line 11
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source,         // line 12
($context["viewBag"] ?? null), "meta_image", [], "any", false, false, true, 12)) ? (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "meta_image", [], "any", false, false, true, 12)) : (((twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_ref_image", [], "any", false, false, true, 12)) ? (call_user_func_array($this->env->getFilter('url')->getCallable(), [$this->extensions['System\Twig\Extension']->mediaFilter(twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [$this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_ref_image", [], "any", false, false, true, 12), 12, $this->source)])))])) : (_twig_default_filter(_twig_default_filter("", ((twig_get_attribute($this->env, $this->source,         // line 13
($context["viewBag"] ?? null), "og_image", [], "any", false, false, true, 13)) ? (call_user_func_array($this->env->getFilter('url')->getCallable(), [$this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_image", [], "any", false, false, true, 13), 13, $this->source))])) : (""))), ((twig_get_attribute($this->env, $this->source,         // line 14
($context["settings"] ?? null), "site_image", [], "any", false, false, true, 14)) ? (call_user_func_array($this->env->getFilter('url')->getCallable(), [$this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "site_image", [], "any", false, false, true, 14), 14, $this->source))])) : (""))))))), "html", null, true);
        // line 15
        echo "\" />
<meta property=\"og:url\" content=\"";
        // line 16
        echo call_user_func_array($this->env->getFilter('url')->getCallable(), [twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [$this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "canonical_url", [], "any", false, false, true, 16), 16, $this->source)]))]);
        echo "\" />
<meta property=\"og:locale\" content=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "og_locale", [], "any", false, false, true, 17), 17, $this->source), "html", null, true);
        echo "\" />
";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["meta_locales"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["locale"]) {
            // line 19
            echo "    <meta property=\"og:locale:alternate\" content=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["locale"], 19, $this->source), "html", null, true);
            echo "\" />
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['locale'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "<meta property=\"fb:app_id\" content=\"";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "fb_app_id", [], "any", false, false, true, 21), 21, $this->source), "html", null, true);
        echo "\" />
<meta name=\"twitter:title\" content=\"";
        // line 22
        echo twig_escape_filter($this->env, _twig_default_filter(twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [((twig_get_attribute($this->env, $this->source,         // line 23
($context["viewBag"] ?? null), "og_title", [], "any", false, false, true, 23)) ? (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_title", [], "any", false, false, true, 23)) : (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "meta_title", [], "any", false, false, true, 23)))])), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source,         // line 24
($context["viewBag"] ?? null), "title", [], "any", false, false, true, 24), 24, $this->source)), "html", null, true);
        // line 25
        echo "\">
<meta name=\"twitter:description\" content=\"";
        // line 26
        echo twig_escape_filter($this->env, _twig_default_filter(twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [((twig_get_attribute($this->env, $this->source,         // line 27
($context["viewBag"] ?? null), "og_description", [], "any", false, false, true, 27)) ? (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_description", [], "any", false, false, true, 27)) : (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "meta_description", [], "any", false, false, true, 27)))])), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source,         // line 28
($context["settings"] ?? null), "site_description", [], "any", false, false, true, 28), 28, $this->source)), "html", null, true);
        // line 29
        echo "\">
<meta name=\"twitter:image\" content=\"";
        // line 30
        echo twig_escape_filter($this->env, ((twig_get_attribute($this->env, $this->source,         // line 31
($context["viewBag"] ?? null), "meta_image", [], "any", false, false, true, 31)) ? (twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "meta_image", [], "any", false, false, true, 31)) : (((twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_ref_image", [], "any", false, false, true, 31)) ? (call_user_func_array($this->env->getFilter('url')->getCallable(), [$this->extensions['System\Twig\Extension']->mediaFilter(twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [$this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_ref_image", [], "any", false, false, true, 31), 31, $this->source)])))])) : (_twig_default_filter(_twig_default_filter("", ((twig_get_attribute($this->env, $this->source,         // line 32
($context["viewBag"] ?? null), "og_image", [], "any", false, false, true, 32)) ? (call_user_func_array($this->env->getFilter('url')->getCallable(), [$this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "og_image", [], "any", false, false, true, 32), 32, $this->source))])) : (""))), ((twig_get_attribute($this->env, $this->source,         // line 33
($context["settings"] ?? null), "site_image", [], "any", false, false, true, 33)) ? (call_user_func_array($this->env->getFilter('url')->getCallable(), [$this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "site_image", [], "any", false, false, true, 33), 33, $this->source))])) : (""))))))), "html", null, true);
        // line 34
        echo "\" />
";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/plugins/pcms/seo/components/seo/social.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 34,  109 => 33,  108 => 32,  107 => 31,  106 => 30,  103 => 29,  101 => 28,  100 => 27,  99 => 26,  96 => 25,  94 => 24,  93 => 23,  92 => 22,  87 => 21,  78 => 19,  74 => 18,  70 => 17,  66 => 16,  63 => 15,  61 => 14,  60 => 13,  59 => 12,  58 => 11,  55 => 10,  53 => 8,  52 => 7,  49 => 6,  47 => 4,  46 => 3,  42 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
<meta property=\"og:type\" content=\"{{ include(template_from_string(viewBag.og_type))  ?: 'website' }}\" /> 
<meta property=\"og:title\" content=\"{{ 
    include(template_from_string(viewBag.og_title ?: viewBag.meta_title ?: viewBag.title )) 
    | default('SEO')
}}\" />
<meta property=\"og:description\" content=\"{{ 
    include(template_from_string(viewBag.og_description ?: viewBag.meta_description ?: settings.site_description )) 
    | default('SEO')
}}\" />
<meta property=\"og:image\" content=\"{{ 
    viewBag.meta_image ? viewBag.meta_image: (viewBag.og_ref_image ? include(template_from_string(viewBag.og_ref_image)) | media | url  : \"\" 
    | default( viewBag.og_image ? ( viewBag.og_image | media | url ) : \"\" ) 
    | default( settings.site_image ? ( settings.site_image | media | url ) : \"\" ))
}}\" />
<meta property=\"og:url\" content=\"{{ include(template_from_string(viewBag.canonical_url)) | url }}\" />
<meta property=\"og:locale\" content=\"{{ settings.og_locale }}\" />
{% for locale in meta_locales %}
    <meta property=\"og:locale:alternate\" content=\"{{ locale }}\" />
{% endfor %}
<meta property=\"fb:app_id\" content=\"{{ settings.fb_app_id }}\" />
<meta name=\"twitter:title\" content=\"{{
    include(template_from_string(viewBag.og_title ?: viewBag.meta_title)) 
    | default(viewBag.title)
}}\">
<meta name=\"twitter:description\" content=\"{{  
    include(template_from_string(viewBag.og_description ?: viewBag.meta_description))
    | default(settings.site_description)
}}\">
<meta name=\"twitter:image\" content=\"{{ 
    viewBag.meta_image ? viewBag.meta_image: (viewBag.og_ref_image ? include(template_from_string( viewBag.og_ref_image )) | media | url : \"\" 
    | default( viewBag.og_image ? ( viewBag.og_image | media | url ) : \"\" ) 
    | default( settings.site_image ? ( settings.site_image | media | url ) : \"\" ))
}}\" />
", "/www/wwwroot/aiosales.vothanhdanh.info/plugins/pcms/seo/components/seo/social.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("for" => 18);
        static $filters = array("escape" => 5, "default" => 5, "url" => 12, "media" => 12);
        static $functions = array("include" => 2, "template_from_string" => 2);

        try {
            $this->sandbox->checkSecurity(
                ['for'],
                ['escape', 'default', 'url', 'media'],
                ['include', 'template_from_string']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
