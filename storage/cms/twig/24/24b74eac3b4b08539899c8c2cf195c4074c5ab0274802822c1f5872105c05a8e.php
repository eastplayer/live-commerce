<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/blog/post.htm */
class __TwigTemplate_779a515a68f3aba3e91e3449fc62f0b01a0dcb5b4471f9b7052f87b214bf56d5 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"bizagn--blog-item\">
    ";
        // line 2
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "featured_images", [], "any", false, false, true, 2), "count", [], "any", false, false, true, 2)) {
            // line 3
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "featured_images", [], "any", false, false, true, 3), 0, 1));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 4
                echo "    <a href=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "url", [], "any", false, false, true, 4), 4, $this->source), "html", null, true);
                echo "\" class=\"bizagn--post-thumb\">
        <img src=\"";
                // line 5
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["image"], "getThumb", [0 => 540, 1 => 300, 2 => "crop"], "method", false, false, true, 5), 5, $this->source), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, true, 5), 5, $this->source), "html", null, true);
                echo "\" class=\"img-fluid\">
    </a>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 8
            echo "    ";
        }
        // line 9
        echo "    <div class=\"bizagn--blog-info\">
        <h3> <a href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "url", [], "any", false, false, true, 10), 10, $this->source), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, true, 10), 10, $this->source), "html", null, true);
        echo "</a></h3>
        <p>";
        // line 11
        echo call_user_func_array($this->env->getFilter('truncate')->getCallable(), [strip_tags($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "summary", [], "any", false, false, true, 11), 11, $this->source)), 100]);
        echo "</p>
        <div class=\"bizagn--blog-meta\">
            <ul class=\"list-inline\">
                <li><a href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "url", [], "any", false, false, true, 14), 14, $this->source), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "published_at", [], "any", false, false, true, 14), 14, $this->source), "d m, Y"), "html", null, true);
        echo "</a></li>
                ";
        // line 15
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "categories", [], "any", false, false, true, 15), "count", [], "any", false, false, true, 15)) {
            // line 16
            echo "                <li>
                    ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "categories", [], "any", false, false, true, 17));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 18
                echo "                    <a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs", ["c" => twig_get_attribute($this->env, $this->source, $context["category"], "slug", [], "any", false, false, true, 18)]);
                echo "\">";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, true, 18), 18, $this->source), "html", null, true);
                echo "</a>";
                if ( !twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, true, 18)) {
                    echo ", ";
                }
                // line 20
                echo "                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "                </li>
                ";
        }
        // line 23
        echo "            </ul>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/blog/post.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 23,  134 => 21,  120 => 20,  111 => 18,  94 => 17,  91 => 16,  89 => 15,  83 => 14,  77 => 11,  71 => 10,  68 => 9,  65 => 8,  54 => 5,  49 => 4,  44 => 3,  42 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<div class=\"bizagn--blog-item\">
    {% if post.featured_images.count %}
    {% for image in post.featured_images|slice(0, 1) %}
    <a href=\"{{ post.url }}\" class=\"bizagn--post-thumb\">
        <img src=\"{{ image.getThumb(540, 300, 'crop') }}\" alt=\"{{ post.title }}\" class=\"img-fluid\">
    </a>
    {% endfor %}
    {% endif %}
    <div class=\"bizagn--blog-info\">
        <h3> <a href=\"{{ post.url }}\">{{ post.title }}</a></h3>
        <p>{{ post.summary | striptags | truncate(100) }}</p>
        <div class=\"bizagn--blog-meta\">
            <ul class=\"list-inline\">
                <li><a href=\"{{ post.url }}\">{{ post.published_at|date('d m, Y') }}</a></li>
                {% if post.categories.count %}
                <li>
                    {% for category in post.categories %}
                    <a href=\"{{ 'blogs'|page({c:category.slug}) }}\">{{ category.name }}</a>{% if not loop.last %}, {%
                    endif %}
                    {% endfor %}
                </li>
                {% endif %}
            </ul>
        </div>
    </div>
</div>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/blog/post.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("if" => 2, "for" => 3);
        static $filters = array("slice" => 3, "escape" => 4, "truncate" => 11, "striptags" => 11, "date" => 14, "page" => 18);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['if', 'for'],
                ['slice', 'escape', 'truncate', 'striptags', 'date', 'page'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
