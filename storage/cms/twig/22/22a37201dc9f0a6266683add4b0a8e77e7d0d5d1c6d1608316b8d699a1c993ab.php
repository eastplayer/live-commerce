<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/banners.htm */
class __TwigTemplate_530e8ed35c3f2b4dc1fe8099cacd02144ba25946f9eb6f3737e1ff7692a1dea6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["bannersBlock"] = twig_get_attribute($this->env, $this->source, ($context["BannersBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        echo "<section class=\"bizagn--bnner-cover\" data-bg-img=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/banner/banner-bg.png");
        echo "\">
    <div class=\"container\">
        <div class=\"bizagn--banner bizagn--owl-btn owl-carousel style-2\" data-owl-nav=\"true\" data-owl-dots=\"true\"
            data-owl-animate-out=\"fadeOut\" data-owl-animate-in=\"fadeIn\" data-owl-margin=\"5\">
            ";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["bannersBlock"] ?? null), "banners", [], "any", false, false, true, 6));
        foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
            // line 7
            echo "            <div class=\"bizagn--banner-slide\">
                <div class=\"row\">
                    <div class=\"col-md-6\">
                        <div class=\"bizagn--banner-slide-text\">
                            <h1>";
            // line 11
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["banner"], "title", [], "any", false, false, true, 11), 11, $this->source), "html", null, true);
            echo "</h1>
                            <p>";
            // line 12
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["banner"], "overview", [], "any", false, false, true, 12), 12, $this->source), "html", null, true);
            echo "</p>
                            ";
            // line 13
            if ((twig_get_attribute($this->env, $this->source, $context["banner"], "url", [], "any", false, false, true, 13) && twig_get_attribute($this->env, $this->source, $context["banner"], "url_anchor", [], "any", false, false, true, 13))) {
                // line 14
                echo "                            <a href=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["banner"], "url", [], "any", false, false, true, 14), 14, $this->source), "html", null, true);
                echo "\" class=\"btn bizagn--base-btn\">
                                <img src=\"";
                // line 15
                echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/btn-right-arrow.svg");
                echo "\" alt=\"\" class=\"svg left\"> About
                                ";
                // line 16
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["banner"], "url_anchor", [], "any", false, false, true, 16), 16, $this->source), "html", null, true);
                echo "
                                <img src=\"";
                // line 17
                echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/btn-right-arrow.svg");
                echo "\" alt=\"\" class=\"svg right\">
                            </a>
                            ";
            }
            // line 20
            echo "                        </div>
                    </div>
                    <div class=\"col-md-6\">
                        ";
            // line 23
            if (twig_get_attribute($this->env, $this->source, $context["banner"], "image", [], "any", false, false, true, 23)) {
                // line 24
                echo "                        <div class=\"bizagn--banner-slide-images d-flex justify-content-md-end\">
                            <div class=\"main-image\">
                                <img src=\"";
                // line 26
                echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["banner"], "image", [], "any", false, false, true, 26), 26, $this->source));
                echo "\" alt=\"\" class=\"img-fluid\">
                            </div>
                        </div>
                        ";
            }
            // line 30
            echo "                    </div>
                </div>
            </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 34
        echo "        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/banners.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 34,  106 => 30,  99 => 26,  95 => 24,  93 => 23,  88 => 20,  82 => 17,  78 => 16,  74 => 15,  69 => 14,  67 => 13,  63 => 12,  59 => 11,  53 => 7,  49 => 6,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set bannersBlock = BannersBlock.data %}
<section class=\"bizagn--bnner-cover\" data-bg-img=\"{{'assets/images/banner/banner-bg.png'|theme}}\">
    <div class=\"container\">
        <div class=\"bizagn--banner bizagn--owl-btn owl-carousel style-2\" data-owl-nav=\"true\" data-owl-dots=\"true\"
            data-owl-animate-out=\"fadeOut\" data-owl-animate-in=\"fadeIn\" data-owl-margin=\"5\">
            {% for banner in bannersBlock.banners %}
            <div class=\"bizagn--banner-slide\">
                <div class=\"row\">
                    <div class=\"col-md-6\">
                        <div class=\"bizagn--banner-slide-text\">
                            <h1>{{banner.title}}</h1>
                            <p>{{banner.overview}}</p>
                            {% if banner.url and banner.url_anchor %}
                            <a href=\"{{banner.url}}\" class=\"btn bizagn--base-btn\">
                                <img src=\"{{'assets/images/btn-right-arrow.svg'|theme}}\" alt=\"\" class=\"svg left\"> About
                                {{banner.url_anchor}}
                                <img src=\"{{'assets/images/btn-right-arrow.svg'|theme}}\" alt=\"\" class=\"svg right\">
                            </a>
                            {% endif %}
                        </div>
                    </div>
                    <div class=\"col-md-6\">
                        {% if banner.image %}
                        <div class=\"bizagn--banner-slide-images d-flex justify-content-md-end\">
                            <div class=\"main-image\">
                                <img src=\"{{banner.image|media}}\" alt=\"\" class=\"img-fluid\">
                            </div>
                        </div>
                        {% endif %}
                    </div>
                </div>
            </div>
            {% endfor %}
        </div>
    </div>
</section>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/banners.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "for" => 6, "if" => 13);
        static $filters = array("theme" => 2, "escape" => 11, "media" => 26);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'for', 'if'],
                ['theme', 'escape', 'media'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
