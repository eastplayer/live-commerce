<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/contact.htm */
class __TwigTemplate_9f0052ed1309c403bffe67e8ac55b12fe5981b47491e95b28528321a77b27329 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["contactBlock"] = twig_get_attribute($this->env, $this->source, ($context["ContactBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        echo "<div class=\"bizagn--page-title\">
    <div class=\"bizagn--page-bc\">
        <ul>
            <li><a href=\"";
        // line 5
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Trang chủ"]);
        echo "</a></li>
            <li>";
        // line 6
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Liên hệ"]);
        echo "</li>
        </ul>
    </div>
</div>
<section class=\"pt-120 pb-80\">
    <div class=\"container\">
        <div class=\"row\">
            ";
        // line 13
        if ((twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "email_anchor", [], "any", false, false, true, 13) && twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "email", [], "any", false, false, true, 13))) {
            // line 14
            echo "            <div class=\"col-lg-4 col-md-6\">
                <div class=\"bizagn--contact-info mb-30\">
                    <div class=\"icon\">
                        <i class=\"fa fa-envelope\"></i>
                    </div>
                    <div class=\"data\">
                        <h3>";
            // line 20
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Địa chỉ Email"]);
            echo "</h3>
                        <p><a href=\"mailto:";
            // line 21
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "email", [], "any", false, false, true, 21), 21, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "email_anchor", [], "any", false, false, true, 21), 21, $this->source), "html", null, true);
            echo "</a></p>
                    </div>
                </div>
            </div>
            ";
        }
        // line 26
        echo "            ";
        if ((twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "phone_anchor", [], "any", false, false, true, 26) && twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "phone", [], "any", false, false, true, 26))) {
            // line 27
            echo "            <div class=\"col-lg-4 col-md-6\">
                <div class=\"bizagn--contact-info mb-30\">
                    <div class=\"icon\">
                        <i class=\"fa fa-phone\"></i>
                    </div>
                    <div class=\"data\">
                        <h3>";
            // line 33
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Số điện thoại"]);
            echo "</h3>
                        <p><a href=\"tel:";
            // line 34
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "phone", [], "any", false, false, true, 34), 34, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "phone_anchor", [], "any", false, false, true, 34), 34, $this->source), "html", null, true);
            echo "</a></p>
                    </div>
                </div>
            </div>
            ";
        }
        // line 39
        echo "            ";
        if (twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "address", [], "any", false, false, true, 39)) {
            // line 40
            echo "            <div class=\"col-lg-4 col-md-6\">
                <div class=\"bizagn--contact-info mb-30\">
                    <div class=\"icon\">
                        <i class=\"fa fa-map-marker\"></i>
                    </div>
                    <div class=\"data\">
                        <h3>";
            // line 46
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Địa chỉ"]);
            echo "</h3>
                        <p>";
            // line 47
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "address", [], "any", false, false, true, 47), 47, $this->source), "html", null, true);
            echo "</p>
                    </div>
                </div>
            </div>
            ";
        }
        // line 52
        echo "        </div>
    </div>
</section>
<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"row align-items-center\">
            <div class=\"col-md-6\">
                <div class=\"bizagn--section-title\">
                    <p>";
        // line 60
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Liên hệ"]);
        echo "</p>
                    <h2>";
        // line 61
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Hãy để lại lời nhắn <br> Chúng tôi sẽ liên hệ lại ngay"]);
        echo "</h2>
                </div>
                <div class=\"bizagn--contact-form\">
                    ";
        // line 64
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("customForm"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 65
        echo "                </div>
            </div>
            ";
        // line 67
        if (twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "map_iframe", [], "any", false, false, true, 67)) {
            // line 68
            echo "            <div class=\"col-md-6\">
                ";
            // line 69
            echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "map_iframe", [], "any", false, false, true, 69), 69, $this->source);
            echo "
            </div>
            ";
        }
        // line 72
        echo "        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/contact.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 72,  167 => 69,  164 => 68,  162 => 67,  158 => 65,  154 => 64,  148 => 61,  144 => 60,  134 => 52,  126 => 47,  122 => 46,  114 => 40,  111 => 39,  101 => 34,  97 => 33,  89 => 27,  86 => 26,  76 => 21,  72 => 20,  64 => 14,  62 => 13,  52 => 6,  46 => 5,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set contactBlock = ContactBlock.data %}
<div class=\"bizagn--page-title\">
    <div class=\"bizagn--page-bc\">
        <ul>
            <li><a href=\"{{'index'|page}}\">{{'Trang chủ'|_}}</a></li>
            <li>{{'Liên hệ'|_}}</li>
        </ul>
    </div>
</div>
<section class=\"pt-120 pb-80\">
    <div class=\"container\">
        <div class=\"row\">
            {% if contactBlock.email_anchor and contactBlock.email %}
            <div class=\"col-lg-4 col-md-6\">
                <div class=\"bizagn--contact-info mb-30\">
                    <div class=\"icon\">
                        <i class=\"fa fa-envelope\"></i>
                    </div>
                    <div class=\"data\">
                        <h3>{{'Địa chỉ Email'|_}}</h3>
                        <p><a href=\"mailto:{{contactBlock.email}}\">{{contactBlock.email_anchor}}</a></p>
                    </div>
                </div>
            </div>
            {% endif %}
            {% if contactBlock.phone_anchor and contactBlock.phone %}
            <div class=\"col-lg-4 col-md-6\">
                <div class=\"bizagn--contact-info mb-30\">
                    <div class=\"icon\">
                        <i class=\"fa fa-phone\"></i>
                    </div>
                    <div class=\"data\">
                        <h3>{{'Số điện thoại'|_}}</h3>
                        <p><a href=\"tel:{{contactBlock.phone}}\">{{contactBlock.phone_anchor}}</a></p>
                    </div>
                </div>
            </div>
            {% endif %}
            {% if contactBlock.address %}
            <div class=\"col-lg-4 col-md-6\">
                <div class=\"bizagn--contact-info mb-30\">
                    <div class=\"icon\">
                        <i class=\"fa fa-map-marker\"></i>
                    </div>
                    <div class=\"data\">
                        <h3>{{'Địa chỉ'|_}}</h3>
                        <p>{{contactBlock.address}}</p>
                    </div>
                </div>
            </div>
            {%endif %}
        </div>
    </div>
</section>
<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"row align-items-center\">
            <div class=\"col-md-6\">
                <div class=\"bizagn--section-title\">
                    <p>{{'Liên hệ'|_}}</p>
                    <h2>{{'Hãy để lại lời nhắn <br> Chúng tôi sẽ liên hệ lại ngay'|_}}</h2>
                </div>
                <div class=\"bizagn--contact-form\">
                    {% component 'customForm' %}
                </div>
            </div>
            {% if contactBlock.map_iframe %}
            <div class=\"col-md-6\">
                {{contactBlock.map_iframe|raw}}
            </div>
            {% endif %}
        </div>
    </div>
</section>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/contact.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 13, "component" => 64);
        static $filters = array("page" => 5, "_" => 5, "escape" => 21, "raw" => 69);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'component'],
                ['page', '_', 'escape', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
