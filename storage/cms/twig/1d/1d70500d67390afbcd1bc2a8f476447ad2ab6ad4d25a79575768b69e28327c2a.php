<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/tags.htm */
class __TwigTemplate_adbc6017a51d52d4ae0b590aef6a8b5102f8d95a785368184e10d35dacb9f64e extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["blogPosts"] = ($context["blogTagSearch"] ?? null);
        // line 2
        $context["posts"] = twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 2);
        // line 3
        echo "<div class=\"bizagn--page-title\">
    <div class=\"bizagn--page-bc\">
        <ul>
            <li><a href=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Trang chủ"]);
        echo "</a></li>
            <li><a href=\"{'blogs'|page({c: ''})}}\">";
        // line 7
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tin tức"]);
        echo "</a></li>
            <li>";
        // line 8
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogTagSearch"] ?? null), "tag", [], "any", false, false, true, 8), "name", [], "any", false, false, true, 8), 8, $this->source), "html", null, true);
        echo "</li>
        </ul>
    </div>
</div>
<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-9\">
                <div class=\"row bizagn--blog-section\">
                    <div class=\"col-12\">
                        <div class=\"sort-box mb-3\">
                            <div class=\"sort-box-item\">
                                <div class=\"sort-box__tab\">
                                    <h6 class=\"m-0\">";
        // line 21
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tìm kiếm tag"]);
        echo " \"";
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogTagSearch"] ?? null), "tag", [], "any", false, false, true, 21), "name", [], "any", false, false, true, 21), 21, $this->source), "html", null, true);
        echo "\":</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 27
            echo "                    <div class=\"col-md-6\">
                        ";
            // line 28
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['post'] = $context["post"]            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 29
            echo "                    </div>
                    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 31
            echo "                    <div class=\"col-12\">
                        <div class=\"sort-box mb-3 border-danger\">
                            <div class=\"sort-box-item\">
                                <div class=\"sort-box__tab\">
                                    <h6 class=\"m-0 text-danger\">";
            // line 35
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Không có bài viết nào"]);
            echo "</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 41
        echo "                </div>
                ";
        // line 42
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 42), "lastPage", [], "any", false, false, true, 42) > 1)) {
            // line 43
            echo "                <div class=\"bizagn--pagination\">
                    <a
                        href=\"";
            // line 45
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 45), "currentPage", [], "any", false, false, true, 45) > 1)) {
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("tags", ["tag" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogTagSearch"] ?? null), "tag", [], "any", false, false, true, 45), "slug", [], "any", false, false, true, 45)]);
                echo "?page=";
                echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 45), "currentPage", [], "any", false, false, true, 45) - 1), "html", null, true);
            } else {
                echo "javascript:;";
            }
            echo "\"><i
                            class=\"fa fa-angle-left\"></i></a>
                    ";
            // line 47
            $context["applyDot"] = true;
            // line 48
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 48), "lastPage", [], "any", false, false, true, 48)));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 49
                echo "                    ";
                if (((($context["page"] == 1) || ($context["page"] == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 49), "lastPage", [], "any", false, false, true, 49))) || (($context["page"] >= (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 49), "currentPage", [], "any", false, false, true, 49) - 1)) && (                // line 50
$context["page"] <= (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 50), "currentPage", [], "any", false, false, true, 50) + 1))))) {
                    echo " ";
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 50), "currentPage", [], "any", false, false, true, 50) == $context["page"])) {
                        echo " <span
                        class=\"current\">";
                        // line 51
                        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["page"], 51, $this->source), "html", null, true);
                        echo "</span>
                        ";
                    } else {
                        // line 53
                        echo "                        <a href=\"";
                        echo $this->extensions['Cms\Twig\Extension']->pageFilter("tags", ["tag" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogTagSearch"] ?? null), "tag", [], "any", false, false, true, 53), "slug", [], "any", false, false, true, 53)]);
                        echo "?page=";
                        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["page"], 53, $this->source), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["page"], 53, $this->source), "html", null, true);
                        echo "</a>
                        ";
                    }
                    // line 55
                    echo "                        ";
                    $context["applyDot"] = true;
                    // line 56
                    echo "                        ";
                } else {
                    // line 57
                    echo "                        ";
                    if ((($context["applyDot"] ?? null) == true)) {
                        // line 58
                        echo "                        <span class=\"current\">...</span>
                        ";
                        // line 59
                        $context["applyDot"] = false;
                        // line 60
                        echo "                        ";
                    }
                    // line 61
                    echo "                        ";
                }
                // line 62
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 63
            echo "                        <a
                            href=\"";
            // line 64
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 64), "currentPage", [], "any", false, false, true, 64) != twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 64), "lastPage", [], "any", false, false, true, 64))) {
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("tags", ["tag" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogTagSearch"] ?? null), "tag", [], "any", false, false, true, 64), "slug", [], "any", false, false, true, 64)]);
                echo "?page=";
                echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 64), "currentPage", [], "any", false, false, true, 64) + 1), "html", null, true);
            } else {
                echo "javascript:;";
            }
            echo "\"><i
                                class=\"fa fa-angle-right\"></i></a>
                </div>
                ";
        }
        // line 68
        echo "            </div>

            <div class=\"col-lg-3\">
                ";
        // line 71
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['category'] = ($context["category"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/sidebar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 72
        echo "            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/tags.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 72,  218 => 71,  213 => 68,  200 => 64,  197 => 63,  191 => 62,  188 => 61,  185 => 60,  183 => 59,  180 => 58,  177 => 57,  174 => 56,  171 => 55,  161 => 53,  156 => 51,  150 => 50,  148 => 49,  143 => 48,  141 => 47,  130 => 45,  126 => 43,  124 => 42,  121 => 41,  109 => 35,  103 => 31,  97 => 29,  92 => 28,  89 => 27,  84 => 26,  74 => 21,  58 => 8,  54 => 7,  48 => 6,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set blogPosts = blogTagSearch %}
{% set posts = blogPosts.posts %}
<div class=\"bizagn--page-title\">
    <div class=\"bizagn--page-bc\">
        <ul>
            <li><a href=\"{{'index'|page}}\">{{'Trang chủ'|_}}</a></li>
            <li><a href=\"{'blogs'|page({c: ''})}}\">{{'Tin tức'|_}}</a></li>
            <li>{{blogTagSearch.tag.name}}</li>
        </ul>
    </div>
</div>
<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-9\">
                <div class=\"row bizagn--blog-section\">
                    <div class=\"col-12\">
                        <div class=\"sort-box mb-3\">
                            <div class=\"sort-box-item\">
                                <div class=\"sort-box__tab\">
                                    <h6 class=\"m-0\">{{'Tìm kiếm tag'|_}} \"{{blogTagSearch.tag.name}}\":</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    {% for post in posts %}
                    <div class=\"col-md-6\">
                        {% partial 'blog/post' post=post %}
                    </div>
                    {% else %}
                    <div class=\"col-12\">
                        <div class=\"sort-box mb-3 border-danger\">
                            <div class=\"sort-box-item\">
                                <div class=\"sort-box__tab\">
                                    <h6 class=\"m-0 text-danger\">{{ 'Không có bài viết nào'|_ }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    {% endfor %}
                </div>
                {% if blogPosts.posts.lastPage > 1 %}
                <div class=\"bizagn--pagination\">
                    <a
                        href=\"{% if blogPosts.posts.currentPage > 1 %}{{ 'tags'|page({tag: blogTagSearch.tag.slug})}}?page={{blogPosts.posts.currentPage - 1}}{% else %}javascript:;{% endif %}\"><i
                            class=\"fa fa-angle-left\"></i></a>
                    {% set applyDot = true %}
                    {% for page in 1..blogPosts.posts.lastPage %}
                    {% if page==1 or page == blogPosts.posts.lastPage or (page >= blogPosts.posts.currentPage-1
                    and page <= blogPosts.posts.currentPage+1) %} {% if blogPosts.posts.currentPage==page %} <span
                        class=\"current\">{{page}}</span>
                        {% else %}
                        <a href=\"{{ 'tags'|page({tag: blogTagSearch.tag.slug})}}?page={{page}}\">{{page}}</a>
                        {% endif %}
                        {% set applyDot = true %}
                        {% else %}
                        {% if applyDot == true %}
                        <span class=\"current\">...</span>
                        {% set applyDot = false %}
                        {% endif %}
                        {% endif %}
                        {% endfor %}
                        <a
                            href=\"{% if blogPosts.posts.currentPage != blogPosts.posts.lastPage %}{{ 'tags'|page({tag: blogTagSearch.tag.slug})}}?page={{blogPosts.posts.currentPage + 1}}{% else %}javascript:;{% endif %}\"><i
                                class=\"fa fa-angle-right\"></i></a>
                </div>
                {%endif %}
            </div>

            <div class=\"col-lg-3\">
                {% partial 'blog/sidebar' category=category %}
            </div>
        </div>
    </div>
</section>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/tags.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "for" => 26, "partial" => 28, "if" => 42);
        static $filters = array("page" => 6, "_" => 6, "escape" => 8);
        static $functions = array("range" => 48);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'for', 'partial', 'if'],
                ['page', '_', 'escape'],
                ['range']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
