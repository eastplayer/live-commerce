<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/teams.htm */
class __TwigTemplate_586cda4613f696d50d723eb500ecfc5bbffed2232e72d93623a72c4752ddb61d extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["teamsBlock"] = twig_get_attribute($this->env, $this->source, ($context["TeamsBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["teamsBlock"] ?? null), "teams", [], "any", false, false, true, 2))) {
            // line 3
            echo "<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"bizagn--section-title text-center\">
            <p>";
            // line 6
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Team"]);
            echo "</p>
            <h2>";
            // line 7
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Gặp gỡ thành viên của chúng tôi"]);
            echo "</h2>
        </div>

        <div class=\"bizagn--team-slider bizagn--owl-btn owl-carousel bizagn--nav-95px\" data-owl-items=\"4\"
            data-owl-autoplay=\"false\" data-owl-nav=\"true\" data-owl-margin=\"30\"
            data-owl-responsive='{\"0\": {\"items\": \"1\"}, \"480\": {\"items\": \"2\"}, \"768\": {\"items\": \"3\"}, \"992\": {\"items\": \"4\"}}'>
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["teamsBlock"] ?? null), "teams", [], "any", false, false, true, 13));
            foreach ($context['_seq'] as $context["_key"] => $context["team"]) {
                // line 14
                echo "            <div class=\"bizagn--team-slider-item\">
                <div class=\"bizagn--team-item text-center\">
                    <img src=\"";
                // line 16
                echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["team"], "avatar", [], "any", false, false, true, 16), 16, $this->source));
                echo "\" alt=\"\" class=\"img-fluid\">
                    <div class=\"bizagn--team-item-info\">
                        <h3>";
                // line 18
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["team"], "name", [], "any", false, false, true, 18), 18, $this->source), "html", null, true);
                echo "</h3>
                        <p>";
                // line 19
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["team"], "title", [], "any", false, false, true, 19), 19, $this->source), "html", null, true);
                echo "</p>
                    </div>
                </div>
            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['team'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "        </div>
    </div>
</section>
";
        }
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/teams.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 24,  78 => 19,  74 => 18,  69 => 16,  65 => 14,  61 => 13,  52 => 7,  48 => 6,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set teamsBlock = TeamsBlock.data %}
{% if teamsBlock.teams | length %}
<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"bizagn--section-title text-center\">
            <p>{{'Team'|_}}</p>
            <h2>{{'Gặp gỡ thành viên của chúng tôi'|_}}</h2>
        </div>

        <div class=\"bizagn--team-slider bizagn--owl-btn owl-carousel bizagn--nav-95px\" data-owl-items=\"4\"
            data-owl-autoplay=\"false\" data-owl-nav=\"true\" data-owl-margin=\"30\"
            data-owl-responsive='{\"0\": {\"items\": \"1\"}, \"480\": {\"items\": \"2\"}, \"768\": {\"items\": \"3\"}, \"992\": {\"items\": \"4\"}}'>
            {% for team in teamsBlock.teams %}
            <div class=\"bizagn--team-slider-item\">
                <div class=\"bizagn--team-item text-center\">
                    <img src=\"{{team.avatar|media}}\" alt=\"\" class=\"img-fluid\">
                    <div class=\"bizagn--team-item-info\">
                        <h3>{{team.name}}</h3>
                        <p>{{team.title}}</p>
                    </div>
                </div>
            </div>
            {% endfor %}
        </div>
    </div>
</section>
{% endif %}", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/teams.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 2, "for" => 13);
        static $filters = array("length" => 2, "_" => 6, "media" => 16, "escape" => 18);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['length', '_', 'media', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
