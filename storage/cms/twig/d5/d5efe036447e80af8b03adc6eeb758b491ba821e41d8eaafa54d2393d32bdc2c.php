<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/partners.htm */
class __TwigTemplate_9949c7c7f444ca7af01fafb54e7608b27d508100db38ade1986ab65a1a5fa483 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["partnersBlock"] = twig_get_attribute($this->env, $this->source, ($context["PartnersBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["partnersBlock"] ?? null), "partners", [], "any", false, false, true, 2))) {
            // line 3
            echo "<div class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"bizagn--section-title text-center\">
            <p>";
            // line 6
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["partnersBlock"] ?? null), "subtitle", [], "any", false, false, true, 6), 6, $this->source), "html", null, true);
            echo "</p>
            <h2>";
            // line 7
            echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["partnersBlock"] ?? null), "title", [], "any", false, false, true, 7), 7, $this->source);
            echo "</h2>
        </div>
        <div class=\"bizagn--brand-slider bizagn--owl-btn owl-carousel\" data-owl-rtl=\"true\" data-owl-autoplay=\"true\"
            data-owl-items=\"6\" data-owl-mouse-drag=\"true\"
            data-owl-responsive='{\"0\": {\"items\": \"2\"}, \"480\": {\"items\": \"3\"}, \"768\": {\"items\": \"4\"}, \"992\": {\"items\": \"6\"}}'>
            ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["partnersBlock"] ?? null), "partners", [], "any", false, false, true, 12));
            foreach ($context['_seq'] as $context["_key"] => $context["partner"]) {
                // line 13
                echo "            <div class=\"bizagn--brand-item d-flex justify-content-center\">
                <a target=\"_blank\" href=\"";
                // line 14
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["partner"], "url", [], "any", false, false, true, 14), 14, $this->source), "html", null, true);
                echo "\"><img src=\"";
                echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["partner"], "logo", [], "any", false, false, true, 14), 14, $this->source));
                echo "\" alt=\"\"
                        class=\"img-fluid\"></a>
            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['partner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 18
            echo "        </div>
    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/partners.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 18,  67 => 14,  64 => 13,  60 => 12,  52 => 7,  48 => 6,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set partnersBlock = PartnersBlock.data %}
{% if partnersBlock.partners | length %}
<div class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"bizagn--section-title text-center\">
            <p>{{partnersBlock.subtitle}}</p>
            <h2>{{partnersBlock.title|raw}}</h2>
        </div>
        <div class=\"bizagn--brand-slider bizagn--owl-btn owl-carousel\" data-owl-rtl=\"true\" data-owl-autoplay=\"true\"
            data-owl-items=\"6\" data-owl-mouse-drag=\"true\"
            data-owl-responsive='{\"0\": {\"items\": \"2\"}, \"480\": {\"items\": \"3\"}, \"768\": {\"items\": \"4\"}, \"992\": {\"items\": \"6\"}}'>
            {% for partner in partnersBlock.partners %}
            <div class=\"bizagn--brand-item d-flex justify-content-center\">
                <a target=\"_blank\" href=\"{{ partner.url }}\"><img src=\"{{ partner.logo|media }}\" alt=\"\"
                        class=\"img-fluid\"></a>
            </div>
            {% endfor %}
        </div>
    </div>
</div>
{% endif %}", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/partners.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 2, "for" => 12);
        static $filters = array("length" => 2, "escape" => 6, "raw" => 7, "media" => 14);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['length', 'escape', 'raw', 'media'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
