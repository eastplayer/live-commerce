<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/plugins/pcms/seo/components/seo/default.htm */
class __TwigTemplate_f1aaf3279f215493df43e59c8282275a39e5a6adc3ee77b92155f65fa5603a27 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "
";
        // line 2
        $context["self"] = ($context["__SELF__"] ?? null);
        // line 3
        $context["settings"] = twig_get_attribute($this->env, $this->source, ($context["__SELF__"] ?? null), "settings", [], "any", false, false, true, 3);
        // line 4
        $context["robots"] = [0 => twig_get_attribute($this->env, $this->source,         // line 5
($context["viewBag"] ?? null), "robot_index", [], "any", false, false, true, 5), 1 => twig_get_attribute($this->env, $this->source,         // line 6
($context["viewBag"] ?? null), "robot_follow", [], "any", false, false, true, 6), 2 => twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [$this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source,         // line 7
($context["viewBag"] ?? null), "robot_advanced", [], "any", false, false, true, 7), 7, $this->source)]))];
        // line 11
        if (twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "enable_site_meta", [], "any", false, false, true, 11)) {
            // line 12
            echo "<title>";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["site_name"] ?? null), 12, $this->source), "html", null, true);
            echo "</title>
<meta name=\"description\" content=\"";
            // line 13
            echo twig_escape_filter($this->env, _twig_default_filter(twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [$this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "meta_description", [], "any", false, false, true, 13), 13, $this->source)])), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "site_description", [], "any", false, false, true, 13), 13, $this->source)), "html", null, true);
            echo "\" />
<link rel=\"canonical\" href=\"";
            // line 14
            echo call_user_func_array($this->env->getFilter('url')->getCallable(), [twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [$this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "canonical_url", [], "any", false, false, true, 14), 14, $this->source)]))]);
            echo "\">
<meta name=\"keywords\" content=\"";
            // line 15
            echo twig_escape_filter($this->env, twig_join_filter($this->sandbox->ensureToStringAllowed(($context["keywords"] ?? null), 15, $this->source), ", "), "html", null, true);
            echo "\" />
";
        }
        // line 17
        if (twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "enable_robots_meta", [], "any", false, false, true, 17)) {
            echo "<meta name=\"robots\" content=\"";
            echo twig_escape_filter($this->env, twig_join_filter(call_user_func_array($this->env->getFilter('removenulls')->getCallable(), [$this->sandbox->ensureToStringAllowed(($context["robots"] ?? null), 17, $this->source)]), ", "), "html", null, true);
            echo "\" /> ";
        }
        // line 18
        echo "
";
        // line 19
        if ( !twig_get_attribute($this->env, $this->source, ($context["self"] ?? null), "disable_schema", [], "any", false, false, true, 19)) {
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "schemas", [], "any", false, false, true, 20));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["schema"]) {
                echo " 
";
                // line 21
                echo twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [$this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["schema"], "script", [], "any", false, false, true, 21), 21, $this->source)]));
                echo "
";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['schema'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "
";
            // line 24
            echo twig_include($this->env, $context, call_user_func_array($this->env->getFunction('template_from_string')->getCallable(), [Pcms\Seo\Classes\Schema::toScript($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["viewBag"] ?? null), "schemas", [], "any", false, false, true, 24), 24, $this->source))]));
            echo "
";
        }
        // line 26
        echo "
";
        // line 27
        if (twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "favicon_enabled", [], "any", false, false, true, 27)) {
            // line 28
            echo "<link rel=\"shortcut icon\" href=\"";
            echo $this->extensions['System\Twig\Extension']->appFilter("favicon.ico");
            echo "\" type=\"image/x-icon\">
<link rel=\"icon\" type=\"image/x-icon\" href=\"";
            // line 29
            echo $this->extensions['System\Twig\Extension']->appFilter("favicon.ico");
            echo "\" /> 
";
        }
        // line 31
        echo "
";
        // line 32
        if (twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "enable_og", [], "any", false, false, true, 32)) {
            // line 33
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['settings'] = ($context["settings"] ?? null)            ;
            $context['__cms_partial_params']['meta_locales'] = ($context["meta_locales"] ?? null)            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("@social.htm"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
        }
        // line 35
        echo "
";
        // line 36
        if (twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "extra_meta", [], "any", false, false, true, 36)) {
            echo " 
";
            // line 37
            echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["settings"] ?? null), "extra_meta", [], "any", false, false, true, 37), 37, $this->source);
            echo " 
";
        }
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/plugins/pcms/seo/components/seo/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 37,  155 => 36,  152 => 35,  145 => 33,  143 => 32,  140 => 31,  135 => 29,  130 => 28,  128 => 27,  125 => 26,  120 => 24,  117 => 23,  101 => 21,  82 => 20,  80 => 19,  77 => 18,  71 => 17,  66 => 15,  62 => 14,  58 => 13,  53 => 12,  51 => 11,  49 => 7,  48 => 6,  47 => 5,  46 => 4,  44 => 3,  42 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("
{% set self = __SELF__ %}
{% set settings = __SELF__.settings %}
{% set robots = [ 
    viewBag.robot_index, 
    viewBag.robot_follow, 
    include(template_from_string(viewBag.robot_advanced)), 
   ] 
%}

{%- if settings.enable_site_meta -%}
<title>{{site_name}}</title>
<meta name=\"description\" content=\"{{ include(template_from_string(viewBag.meta_description)) | default(settings.site_description) }}\" />
<link rel=\"canonical\" href=\"{{ include(template_from_string(viewBag.canonical_url)) | url  }}\">
<meta name=\"keywords\" content=\"{{ keywords | join(', ') }}\" />
{% endif %}
{%- if settings.enable_robots_meta -%} <meta name=\"robots\" content=\"{{ robots | removenulls|join(', ') }}\" /> {% endif %}

{% if not self.disable_schema %}
{% for schema in viewBag.schemas %} 
{{ include(template_from_string(schema.script)) | raw }}
{% endfor %}

{{ include(template_from_string(viewBag.schemas | pcms_seo_schema)) | raw }}
{% endif %}

{% if settings.favicon_enabled %}
<link rel=\"shortcut icon\" href=\"{{ 'favicon.ico' | app }}\" type=\"image/x-icon\">
<link rel=\"icon\" type=\"image/x-icon\" href=\"{{ 'favicon.ico' | app }}\" /> 
{% endif %}

{% if settings.enable_og %}
{% partial '@social.htm' settings=settings meta_locales=meta_locales %}
{% endif %}

{% if settings.extra_meta %} 
{{ settings.extra_meta | raw }} 
{% endif %}
", "/www/wwwroot/aiosales.vothanhdanh.info/plugins/pcms/seo/components/seo/default.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 2, "if" => 11, "for" => 20, "partial" => 33);
        static $filters = array("escape" => 12, "default" => 13, "url" => 14, "join" => 15, "removenulls" => 17, "raw" => 21, "pcms_seo_schema" => 24, "app" => 28);
        static $functions = array("include" => 7, "template_from_string" => 7);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for', 'partial'],
                ['escape', 'default', 'url', 'join', 'removenulls', 'raw', 'pcms_seo_schema', 'app'],
                ['include', 'template_from_string']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
