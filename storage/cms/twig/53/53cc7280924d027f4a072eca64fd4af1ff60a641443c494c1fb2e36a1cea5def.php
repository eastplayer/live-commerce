<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/header.htm */
class __TwigTemplate_cc8a5e789098c3b01be19379698683cd83bdab2fbad7c4a6e441148bfe570202 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["companyBlock"] = twig_get_attribute($this->env, $this->source, ($context["CompanyBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        $context["socialsBlock"] = twig_get_attribute($this->env, $this->source, ($context["SocialsBlock"] ?? null), "data", [], "any", false, false, true, 2);
        // line 3
        echo "<header class=\"bizagn--header\">
    <div class=\"nav-ol\"></div>
    <div class=\"bizagn--sticky-nav sticky-header-true\">
        <div class=\"container\">
            <div class=\"bizagn--navbar\">
                <div class=\"row align-items-center\">
                    <div class=\"col-md-3 col-6\">
                        <div class=\"bizagn--logo\">
                            <a href=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\"><img src=\"";
        echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["companyBlock"] ?? null), "logo", [], "any", false, false, true, 11), 11, $this->source));
        echo "\" alt=\"\" class=\"img-fluid\"
                                    data-rjs=\"2\"></a>
                        </div>
                    </div>
                    <div class=\"col-md-9 col-6 d-flex align-items-center justify-content-end position-static\">
                        <div class=\"bizagn--nav-menu\">

                            <ul>
                                <li><a href=\"";
        // line 19
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Trang chủ"]);
        echo "</a>
                                </li>
                                <li ";
        // line 21
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["blogCategories"] ?? null), "categories", [], "any", false, false, true, 21))) {
            echo "class=\"menu-item-has-children\" ";
        }
        echo ">
                                    <a href=\"";
        // line 22
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs", ["c" => ""]);
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tin tức"]);
        echo "</a>
                                    ";
        // line 23
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["blogCategories"] ?? null), "categories", [], "any", false, false, true, 23))) {
            // line 24
            echo "                                    <ul class=\"sub-menu py-2\">
                                        ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["blogCategories"] ?? null), "categories", [], "any", false, false, true, 25));
            foreach ($context['_seq'] as $context["_key"] => $context["cate"]) {
                // line 26
                echo "                                        <li><a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs", ["c" => twig_get_attribute($this->env, $this->source, $context["cate"], "slug", [], "any", false, false, true, 26)]);
                echo "\">";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cate"], "name", [], "any", false, false, true, 26), 26, $this->source), "html", null, true);
                echo "</a></li>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cate'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "                                    </ul>
                                    ";
        }
        // line 30
        echo "                                </li>
                                <li><a href=\"";
        // line 31
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Liên hệ"]);
        echo "</a></li>
                            </ul>

                        </div>
                        <div class=\"bizagn--mobile-nav\">
                            <ul class=\"inline-list\">
                                <li>
                                    <div class=\"bizagn--off-canvas-toggle\">
                                        <span class=\"line one\"></span>
                                        <span class=\"line two\"></span>
                                        <span class=\"line three\"></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"bizagn--off-canvas\">
        <div class=\"bizagn--off-canvas-cover\">
            <div class=\"bizagn--off-canvas-close\">
                ";
        // line 54
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Đóng"]);
        echo " <i class=\"fa fa-close\"></i>
            </div>
            <div class=\"bizagn--off-canvas-menu\">
                <ul>
                    <li><a href=\"";
        // line 58
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Trang chủ"]);
        echo "</a>
                    <li><a href=\"";
        // line 59
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tin tức"]);
        echo "</a></li>
                    <li><a href=\"";
        // line 60
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Liên hệ"]);
        echo "</a></li>
                </ul>
            </div>
            ";
        // line 63
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["socialsBlock"] ?? null), "socials", [], "any", false, false, true, 63))) {
            // line 64
            echo "            <div class=\"bizagn--off-canvas-social\">
                ";
            // line 65
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["socialsBlock"] ?? null), "socials", [], "any", false, false, true, 65));
            foreach ($context['_seq'] as $context["_key"] => $context["social"]) {
                // line 66
                echo "                <a target=\"_blank\" href=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["social"], "url", [], "any", false, false, true, 66), 66, $this->source), "html", null, true);
                echo "\"><i class=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["social"], "icon", [], "any", false, false, true, 66), 66, $this->source), "html", null, true);
                echo "\"></i></a>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['social'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 68
            echo "            </div>
            ";
        }
        // line 70
        echo "        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  191 => 70,  187 => 68,  176 => 66,  172 => 65,  169 => 64,  167 => 63,  159 => 60,  153 => 59,  147 => 58,  140 => 54,  112 => 31,  109 => 30,  105 => 28,  94 => 26,  90 => 25,  87 => 24,  85 => 23,  79 => 22,  73 => 21,  66 => 19,  53 => 11,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set companyBlock = CompanyBlock.data %}
{% set socialsBlock = SocialsBlock.data %}
<header class=\"bizagn--header\">
    <div class=\"nav-ol\"></div>
    <div class=\"bizagn--sticky-nav sticky-header-true\">
        <div class=\"container\">
            <div class=\"bizagn--navbar\">
                <div class=\"row align-items-center\">
                    <div class=\"col-md-3 col-6\">
                        <div class=\"bizagn--logo\">
                            <a href=\"{{'index'|page}}\"><img src=\"{{companyBlock.logo|media}}\" alt=\"\" class=\"img-fluid\"
                                    data-rjs=\"2\"></a>
                        </div>
                    </div>
                    <div class=\"col-md-9 col-6 d-flex align-items-center justify-content-end position-static\">
                        <div class=\"bizagn--nav-menu\">

                            <ul>
                                <li><a href=\"{{'index'|page}}\">{{'Trang chủ'|_}}</a>
                                </li>
                                <li {% if blogCategories.categories|length %}class=\"menu-item-has-children\" {% endif %}>
                                    <a href=\"{{'blogs'|page({c: ''})}}\">{{'Tin tức'|_}}</a>
                                    {% if blogCategories.categories|length %}
                                    <ul class=\"sub-menu py-2\">
                                        {% for cate in blogCategories.categories %}
                                        <li><a href=\"{{'blogs'|page({c: cate.slug})}}\">{{cate.name}}</a></li>
                                        {% endfor %}
                                    </ul>
                                    {% endif %}
                                </li>
                                <li><a href=\"{{'contact'|page}}\">{{'Liên hệ'|_}}</a></li>
                            </ul>

                        </div>
                        <div class=\"bizagn--mobile-nav\">
                            <ul class=\"inline-list\">
                                <li>
                                    <div class=\"bizagn--off-canvas-toggle\">
                                        <span class=\"line one\"></span>
                                        <span class=\"line two\"></span>
                                        <span class=\"line three\"></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"bizagn--off-canvas\">
        <div class=\"bizagn--off-canvas-cover\">
            <div class=\"bizagn--off-canvas-close\">
                {{'Đóng'|_}} <i class=\"fa fa-close\"></i>
            </div>
            <div class=\"bizagn--off-canvas-menu\">
                <ul>
                    <li><a href=\"{{'index'|page}}\">{{'Trang chủ'|_}}</a>
                    <li><a href=\"{{'blogs'|page}}\">{{'Tin tức'|_}}</a></li>
                    <li><a href=\"{{'contact'|page}}\">{{'Liên hệ'|_}}</a></li>
                </ul>
            </div>
            {% if socialsBlock.socials | length %}
            <div class=\"bizagn--off-canvas-social\">
                {% for social in socialsBlock.socials %}
                <a target=\"_blank\" href=\"{{social.url}}\"><i class=\"{{social.icon}}\"></i></a>
                {% endfor %}
            </div>
            {% endif %}
        </div>
    </div>
</header>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/header.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 21, "for" => 25);
        static $filters = array("page" => 11, "media" => 11, "_" => 19, "length" => 21, "escape" => 26);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['page', 'media', '_', 'length', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
