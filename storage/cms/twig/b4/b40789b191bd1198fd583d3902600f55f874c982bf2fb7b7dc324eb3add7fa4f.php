<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/blog/sidebar.htm */
class __TwigTemplate_f4c5617ed25fadecd35e131237f40b5ab7a6955d335b8907d86ac6fd05ff35e7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["socials"] = twig_get_attribute($this->env, $this->source, ($context["socialsBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        $context["posts"] = twig_get_attribute($this->env, $this->source, ($context["blogPostsSidebar"] ?? null), "posts", [], "any", false, false, true, 2);
        // line 3
        echo "<div class=\"bizagn--sidebar\">
    <div class=\"widget\">
        <div class=\"bizagn--widget widget_search\">
            <form action=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs");
        echo "\" method=\"get\">
                <div class=\"input-group\">
                    <input type=\"search\" name=\"search\" class=\"form-control\" required placeholder=\"";
        // line 8
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tìm bài viết"]);
        echo "\"
                        ";
        // line 9
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, true, 9), "search", [], "any", false, false, true, 9)) {
            echo "value=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, true, 9), "search", [], "any", false, false, true, 9), 9, $this->source), "html", null, true);
            echo "\" ";
        }
        echo ">
                    <div class=\"input-group-append\">
                        <button class=\"btn\" type=\"submit\">
                            <i class=\"fa fa-search\"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    ";
        // line 19
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["blogCategories"] ?? null), "categories", [], "any", false, false, true, 19))) {
            // line 20
            echo "    <div class=\"widget\">
        <div class=\"bizagn--widget widget_categories\">
            <div class=\"widget-title\">
                <div class=\"bizagn--widget-title\">
                    <h3>";
            // line 24
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Danh mục"]);
            echo "</h3>
                </div>
            </div>
            <div class=\"bizagn--widget-content\">
                <ul>
                    ";
            // line 29
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["blogCategories"] ?? null), "categories", [], "any", false, false, true, 29));
            foreach ($context['_seq'] as $context["_key"] => $context["cate"]) {
                // line 30
                echo "                    <li><a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs", ["c" => twig_get_attribute($this->env, $this->source, $context["cate"], "slug", [], "any", false, false, true, 30)]);
                echo "\">";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["cate"], "name", [], "any", false, false, true, 30), 30, $this->source), "html", null, true);
                echo "</a></li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cate'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "                </ul>
            </div>
        </div>
    </div>
    ";
        }
        // line 37
        echo "    ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["blogPostsSidebar"] ?? null), "posts", [], "any", false, false, true, 37))) {
            // line 38
            echo "    <div class=\"widget\">
        <div class=\"bizagn--widget widget_posts\">
            <div class=\"widget-title\">
                <div class=\"bizagn--widget-title\">
                    <h3>";
            // line 42
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Bài viết mới"]);
            echo "</h3>
                </div>
            </div>
            <div class=\"bizagn--widget-content\">
                ";
            // line 46
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["blogPostsSidebar"] ?? null), "posts", [], "any", false, false, true, 46));
            foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                // line 47
                echo "                <div class=\"bizagn--widget-post\">
                    ";
                // line 48
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["post"], "featured_images", [], "any", false, false, true, 48), "count", [], "any", false, false, true, 48)) {
                    // line 49
                    echo "                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, twig_get_attribute($this->env, $this->source, $context["post"], "featured_images", [], "any", false, false, true, 49), 0, 1));
                    foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                        // line 50
                        echo "                    <a href=\"";
                        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, true, 50), 50, $this->source), "html", null, true);
                        echo "\" class=\"bizagn--widget-thumb\">
                        <img src=\"";
                        // line 51
                        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["image"], "getThumb", [0 => 80, 1 => 80, 2 => "crop"], "method", false, false, true, 51), 51, $this->source), "html", null, true);
                        echo "\" alt=\"";
                        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, true, 51), 51, $this->source), "html", null, true);
                        echo "\" class=\"img-fluid\">
                    </a>
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 54
                    echo "                    ";
                }
                // line 55
                echo "                    <div class=\"bizagn--widget-meta-title\">
                        <a href=\"";
                // line 56
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, true, 56), 56, $this->source), "html", null, true);
                echo "\" class=\"w-meta\">";
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["post"], "published_at", [], "any", false, false, true, 56), 56, $this->source), "d m, Y"), "html", null, true);
                echo "</a>
                        <a href=\"";
                // line 57
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["post"], "url", [], "any", false, false, true, 57), 57, $this->source), "html", null, true);
                echo "\" class=\"w-title\">";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["post"], "title", [], "any", false, false, true, 57), 57, $this->source), "html", null, true);
                echo "</a>
                    </div>
                </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 61
            echo "            </div>
        </div>
    </div>
    ";
        }
        // line 65
        echo "    ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["blogTags"] ?? null), "tags", [], "any", false, false, true, 65))) {
            // line 66
            echo "    <div class=\"widget\">
        <div class=\"bizagn--widget widget-posts\">
            <div class=\"widget-title\">
                <div class=\"bizagn--widget-title\">
                    <h3>";
            // line 70
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tags"]);
            echo "</h3>
                </div>
            </div>
            <div class=\"bizagn--widget-content\">
                <ul>
                    ";
            // line 75
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["blogTags"] ?? null), "tags", [], "any", false, false, true, 75));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 76
                echo "                    <li><a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("tags", ["tag" => twig_get_attribute($this->env, $this->source, $context["tag"], "slug", [], "any", false, false, true, 76)]);
                echo "\">";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["tag"], "name", [], "any", false, false, true, 76), 76, $this->source), "html", null, true);
                echo "</a></li>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 78
            echo "                </ul>
            </div>
        </div>
    </div>
    ";
        }
        // line 83
        echo "    ";
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["BlogBanner"] ?? null), "banners", [], "any", false, false, true, 83))) {
            // line 84
            echo "    <div class=\"widget mb-4\">
        ";
            // line 85
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["BlogBanner"] ?? null), "banners", [], "any", false, false, true, 85));
            foreach ($context['_seq'] as $context["_key"] => $context["banner"]) {
                // line 86
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["banner"], "url", [], "any", false, false, true, 86), 86, $this->source), "html", null, true);
                echo "\" target=\"_blank\"><img src=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["banner"], "background_img", [], "any", false, false, true, 86), "getPath", [], "method", false, false, true, 86), 86, $this->source), "html", null, true);
                echo "\" width=\"100%\" alt=\"\"></a>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['banner'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "    </div>
    ";
        }
        // line 90
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["socials"] ?? null), "fb_widget", [], "any", false, false, true, 90)) {
            // line 91
            echo "    <div class=\"widget\">
        <div class=\"bizagn--widget widget-posts\">
            <div class=\"widget-title\">
                <div class=\"bizagn--widget-title\">
                    <h3>";
            // line 95
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Chúng tôi trên Facebook"]);
            echo "</h3>
                </div>
            </div>
            ";
            // line 98
            echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["socials"] ?? null), "fb_widget", [], "any", false, false, true, 98), 98, $this->source);
            echo "
        </div>
    </div>
    ";
        }
        // line 102
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/blog/sidebar.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  274 => 102,  267 => 98,  261 => 95,  255 => 91,  252 => 90,  248 => 88,  237 => 86,  233 => 85,  230 => 84,  227 => 83,  220 => 78,  209 => 76,  205 => 75,  197 => 70,  191 => 66,  188 => 65,  182 => 61,  170 => 57,  164 => 56,  161 => 55,  158 => 54,  147 => 51,  142 => 50,  137 => 49,  135 => 48,  132 => 47,  128 => 46,  121 => 42,  115 => 38,  112 => 37,  105 => 32,  94 => 30,  90 => 29,  82 => 24,  76 => 20,  74 => 19,  57 => 9,  53 => 8,  48 => 6,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set socials = socialsBlock.data %}
{% set posts = blogPostsSidebar.posts %}
<div class=\"bizagn--sidebar\">
    <div class=\"widget\">
        <div class=\"bizagn--widget widget_search\">
            <form action=\"{{'blogs'|page}}\" method=\"get\">
                <div class=\"input-group\">
                    <input type=\"search\" name=\"search\" class=\"form-control\" required placeholder=\"{{'Tìm bài viết'|_}}\"
                        {% if this.page.search %}value=\"{{this.page.search}}\" {% endif %}>
                    <div class=\"input-group-append\">
                        <button class=\"btn\" type=\"submit\">
                            <i class=\"fa fa-search\"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    {% if blogCategories.categories|length %}
    <div class=\"widget\">
        <div class=\"bizagn--widget widget_categories\">
            <div class=\"widget-title\">
                <div class=\"bizagn--widget-title\">
                    <h3>{{'Danh mục'|_}}</h3>
                </div>
            </div>
            <div class=\"bizagn--widget-content\">
                <ul>
                    {% for cate in blogCategories.categories %}
                    <li><a href=\"{{'blogs'|page({c: cate.slug})}}\">{{cate.name}}</a></li>
                    {% endfor %}
                </ul>
            </div>
        </div>
    </div>
    {% endif %}
    {% if blogPostsSidebar.posts|length %}
    <div class=\"widget\">
        <div class=\"bizagn--widget widget_posts\">
            <div class=\"widget-title\">
                <div class=\"bizagn--widget-title\">
                    <h3>{{'Bài viết mới'|_}}</h3>
                </div>
            </div>
            <div class=\"bizagn--widget-content\">
                {% for post in blogPostsSidebar.posts %}
                <div class=\"bizagn--widget-post\">
                    {% if post.featured_images.count %}
                    {% for image in post.featured_images|slice(0, 1) %}
                    <a href=\"{{post.url}}\" class=\"bizagn--widget-thumb\">
                        <img src=\"{{ image.getThumb(80, 80, 'crop') }}\" alt=\"{{post.title}}\" class=\"img-fluid\">
                    </a>
                    {% endfor %}
                    {% endif %}
                    <div class=\"bizagn--widget-meta-title\">
                        <a href=\"{{post.url}}\" class=\"w-meta\">{{ post.published_at|date('d m, Y') }}</a>
                        <a href=\"{{post.url}}\" class=\"w-title\">{{post.title}}</a>
                    </div>
                </div>
                {% endfor %}
            </div>
        </div>
    </div>
    {% endif %}
    {% if blogTags.tags|length %}
    <div class=\"widget\">
        <div class=\"bizagn--widget widget-posts\">
            <div class=\"widget-title\">
                <div class=\"bizagn--widget-title\">
                    <h3>{{'Tags'|_}}</h3>
                </div>
            </div>
            <div class=\"bizagn--widget-content\">
                <ul>
                    {% for tag in blogTags.tags %}
                    <li><a href=\"{{'tags'|page({tag: tag.slug})}}\">{{ tag.name }}</a></li>
                    {% endfor %}
                </ul>
            </div>
        </div>
    </div>
    {% endif %}
    {% if BlogBanner.banners | length %}
    <div class=\"widget mb-4\">
        {% for banner in BlogBanner.banners %}
        <a href=\"{{banner.url}}\" target=\"_blank\"><img src=\"{{banner.background_img.getPath()}}\" width=\"100%\" alt=\"\"></a>
        {% endfor %}
    </div>
    {% endif %}
    {% if socials.fb_widget %}
    <div class=\"widget\">
        <div class=\"bizagn--widget widget-posts\">
            <div class=\"widget-title\">
                <div class=\"bizagn--widget-title\">
                    <h3>{{'Chúng tôi trên Facebook'|_}}</h3>
                </div>
            </div>
            {{socials.fb_widget|raw}}
        </div>
    </div>
    {% endif %}
</div>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/blog/sidebar.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 9, "for" => 29);
        static $filters = array("page" => 6, "_" => 8, "escape" => 9, "length" => 19, "slice" => 49, "date" => 56, "raw" => 98);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['page', '_', 'escape', 'length', 'slice', 'date', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
