<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/blogs.htm */
class __TwigTemplate_7a37a2b852d109c7abd6ca01d910b78b226aeba8b1fd2819c71d7e90bf7e23f6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["posts"] = twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 1);
        // line 2
        echo "<div class=\"bizagn--page-title\">
    <div class=\"bizagn--page-bc\">
        <ul>
            <li><a href=\"";
        // line 5
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Trang chủ"]);
        echo "</a></li>
            <li>";
        // line 6
        if (($context["category"] ?? null)) {
            echo "<a href=\"{'blogs'|page({c: ''})}}\">";
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tin tức"]);
            echo "</a>";
        } else {
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tin tức"]);
        }
        // line 7
        echo "</li>
            ";
        // line 8
        if (($context["category"] ?? null)) {
            // line 9
            echo "            <li>";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "name", [], "any", false, false, true, 9), 9, $this->source), "html", null, true);
            echo "</li>
            ";
        }
        // line 11
        echo "        </ul>
    </div>
</div>
<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-8\">
                <div class=\"row bizagn--blog-section\">
                    ";
        // line 19
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, true, 19), "search", [], "any", false, false, true, 19)) {
            // line 20
            echo "                    <div class=\"col-12\">
                        <div class=\"sort-box mb-3\">
                            <div class=\"sort-box-item\">
                                <div class=\"sort-box__tab\">
                                    <h6 class=\"m-0\">";
            // line 24
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tìm kiếm cho"]);
            echo " \"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, true, 24), "search", [], "any", false, false, true, 24), 24, $this->source), "html", null, true);
            echo "\"";
            if (($context["category"] ?? null)) {
                // line 25
                echo "                                        ";
                echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["trong danh mục"]);
                echo " \"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "name", [], "any", false, false, true, 25), 25, $this->source), "html", null, true);
                echo "\"";
            }
            echo ":</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        }
        // line 31
        echo "                    ";
        if (( !twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["this"] ?? null), "page", [], "any", false, false, true, 31), "search", [], "any", false, false, true, 31) && ($context["category"] ?? null))) {
            // line 32
            echo "                    <div class=\"col-12\">
                        <div class=\"sort-box mb-3\">
                            <div class=\"sort-box-item\">
                                <div class=\"sort-box__tab\">
                                    <h6 class=\"m-0\">";
            // line 36
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tìm kiếm trong danh mục"]);
            echo " \"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["category"] ?? null), "name", [], "any", false, false, true, 36), 36, $this->source), "html", null, true);
            echo "\":</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        }
        // line 42
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 43
            echo "                    <div class=\"col-md-6\">
                        ";
            // line 44
            $context['__cms_partial_params'] = [];
            $context['__cms_partial_params']['post'] = $context["post"]            ;
            echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post"            , $context['__cms_partial_params']            , true            );
            unset($context['__cms_partial_params']);
            // line 45
            echo "                    </div>
                    ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 47
            echo "                    <div class=\"col-12\">
                        <div class=\"sort-box mb-3 border-danger\">
                            <div class=\"sort-box-item\">
                                <div class=\"sort-box__tab\">
                                    <h6 class=\"m-0 text-danger\">";
            // line 51
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Không có bài viết nào"]);
            echo "</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "                </div>
                ";
        // line 58
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 58), "lastPage", [], "any", false, false, true, 58) > 1)) {
            // line 59
            echo "                <div class=\"bizagn--pagination\">
                    <a
                        href=\"";
            // line 61
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 61), "currentPage", [], "any", false, false, true, 61) > 1)) {
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs");
                echo "?page=";
                echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 61), "currentPage", [], "any", false, false, true, 61) - 1), "html", null, true);
                if (($context["search"] ?? null)) {
                    echo "&search=";
                    echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["search"] ?? null), 61, $this->source), "html", null, true);
                }
            } else {
                echo "javascript:;";
            }
            echo "\"><i
                            class=\"fa fa-angle-left\"></i></a>
                    ";
            // line 63
            $context["applyDot"] = true;
            // line 64
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(1, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 64), "lastPage", [], "any", false, false, true, 64)));
            foreach ($context['_seq'] as $context["_key"] => $context["page"]) {
                // line 65
                echo "                    ";
                if (((($context["page"] == 1) || ($context["page"] == twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 65), "lastPage", [], "any", false, false, true, 65))) || (($context["page"] >= (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 65), "currentPage", [], "any", false, false, true, 65) - 1)) && (                // line 66
$context["page"] <= (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 66), "currentPage", [], "any", false, false, true, 66) + 1))))) {
                    echo " ";
                    if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 66), "currentPage", [], "any", false, false, true, 66) == $context["page"])) {
                        echo " <span
                        class=\"current\">";
                        // line 67
                        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["page"], 67, $this->source), "html", null, true);
                        echo "</span>
                        ";
                    } else {
                        // line 69
                        echo "                        <a
                            href=\"";
                        // line 70
                        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs");
                        echo "?page=";
                        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["page"], 70, $this->source), "html", null, true);
                        if (($context["search"] ?? null)) {
                            echo "&search=";
                            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["search"] ?? null), 70, $this->source), "html", null, true);
                        }
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed($context["page"], 70, $this->source), "html", null, true);
                        echo "</a>
                        ";
                    }
                    // line 72
                    echo "                        ";
                    $context["applyDot"] = true;
                    // line 73
                    echo "                        ";
                } else {
                    // line 74
                    echo "                        ";
                    if ((($context["applyDot"] ?? null) == true)) {
                        // line 75
                        echo "                        <span class=\"current\">...</span>
                        ";
                        // line 76
                        $context["applyDot"] = false;
                        // line 77
                        echo "                        ";
                    }
                    // line 78
                    echo "                        ";
                }
                // line 79
                echo "                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['page'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 80
            echo "                        <a
                            href=\"";
            // line 81
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 81), "currentPage", [], "any", false, false, true, 81) != twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 81), "lastPage", [], "any", false, false, true, 81))) {
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs");
                echo "?page=";
                echo twig_escape_filter($this->env, (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 81), "currentPage", [], "any", false, false, true, 81) + 1), "html", null, true);
                if (($context["search"] ?? null)) {
                    echo "&search=";
                    echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["search"] ?? null), 81, $this->source), "html", null, true);
                }
            } else {
                echo "javascript:;";
            }
            echo "\"><i
                                class=\"fa fa-angle-right\"></i></a>
                </div>
                ";
        }
        // line 85
        echo "            </div>

            <div class=\"col-lg-4\">
                ";
        // line 88
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['category'] = ($context["category"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/sidebar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 89
        echo "            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/blogs.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 89,  278 => 88,  273 => 85,  256 => 81,  253 => 80,  247 => 79,  244 => 78,  241 => 77,  239 => 76,  236 => 75,  233 => 74,  230 => 73,  227 => 72,  214 => 70,  211 => 69,  206 => 67,  200 => 66,  198 => 65,  193 => 64,  191 => 63,  176 => 61,  172 => 59,  170 => 58,  167 => 57,  155 => 51,  149 => 47,  143 => 45,  138 => 44,  135 => 43,  129 => 42,  118 => 36,  112 => 32,  109 => 31,  95 => 25,  89 => 24,  83 => 20,  81 => 19,  71 => 11,  65 => 9,  63 => 8,  60 => 7,  52 => 6,  46 => 5,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set posts = blogPosts.posts %}
<div class=\"bizagn--page-title\">
    <div class=\"bizagn--page-bc\">
        <ul>
            <li><a href=\"{{'index'|page}}\">{{'Trang chủ'|_}}</a></li>
            <li>{% if category %}<a href=\"{'blogs'|page({c: ''})}}\">{{'Tin tức'|_}}</a>{% else %}{{'Tin tức'|_}}{%
                endif %}</li>
            {% if category %}
            <li>{{category.name}}</li>
            {% endif %}
        </ul>
    </div>
</div>
<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-8\">
                <div class=\"row bizagn--blog-section\">
                    {% if this.page.search %}
                    <div class=\"col-12\">
                        <div class=\"sort-box mb-3\">
                            <div class=\"sort-box-item\">
                                <div class=\"sort-box__tab\">
                                    <h6 class=\"m-0\">{{'Tìm kiếm cho'|_}} \"{{this.page.search}}\"{% if category %}
                                        {{'trong danh mục'|_}} \"{{category.name}}\"{% endif %}:</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    {% endif %}
                    {% if not this.page.search and category %}
                    <div class=\"col-12\">
                        <div class=\"sort-box mb-3\">
                            <div class=\"sort-box-item\">
                                <div class=\"sort-box__tab\">
                                    <h6 class=\"m-0\">{{'Tìm kiếm trong danh mục'|_}} \"{{category.name}}\":</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    {% endif %}
                    {% for post in posts %}
                    <div class=\"col-md-6\">
                        {% partial 'blog/post' post=post %}
                    </div>
                    {% else %}
                    <div class=\"col-12\">
                        <div class=\"sort-box mb-3 border-danger\">
                            <div class=\"sort-box-item\">
                                <div class=\"sort-box__tab\">
                                    <h6 class=\"m-0 text-danger\">{{ 'Không có bài viết nào'|_ }}</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    {% endfor %}
                </div>
                {% if blogPosts.posts.lastPage > 1 %}
                <div class=\"bizagn--pagination\">
                    <a
                        href=\"{% if blogPosts.posts.currentPage > 1 %}{{ 'blogs'|page}}?page={{blogPosts.posts.currentPage - 1}}{% if search %}&search={{search}}{% endif %}{% else %}javascript:;{% endif %}\"><i
                            class=\"fa fa-angle-left\"></i></a>
                    {% set applyDot = true %}
                    {% for page in 1..blogPosts.posts.lastPage %}
                    {% if page==1 or page == blogPosts.posts.lastPage or (page >= blogPosts.posts.currentPage-1
                    and page <= blogPosts.posts.currentPage+1) %} {% if blogPosts.posts.currentPage==page %} <span
                        class=\"current\">{{page}}</span>
                        {% else %}
                        <a
                            href=\"{{ 'blogs'|page}}?page={{page}}{% if search %}&search={{search}}{% endif %}\">{{page}}</a>
                        {% endif %}
                        {% set applyDot = true %}
                        {% else %}
                        {% if applyDot == true %}
                        <span class=\"current\">...</span>
                        {% set applyDot = false %}
                        {% endif %}
                        {% endif %}
                        {% endfor %}
                        <a
                            href=\"{% if blogPosts.posts.currentPage != blogPosts.posts.lastPage %}{{ 'blogs'|page}}?page={{blogPosts.posts.currentPage + 1}}{% if search %}&search={{search}}{% endif %}{% else %}javascript:;{% endif %}\"><i
                                class=\"fa fa-angle-right\"></i></a>
                </div>
                {%endif %}
            </div>

            <div class=\"col-lg-4\">
                {% partial 'blog/sidebar' category=category %}
            </div>
        </div>
    </div>
</section>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/blogs.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 6, "for" => 42, "partial" => 44);
        static $filters = array("page" => 5, "_" => 5, "escape" => 9);
        static $functions = array("range" => 64);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for', 'partial'],
                ['page', '_', 'escape'],
                ['range']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
