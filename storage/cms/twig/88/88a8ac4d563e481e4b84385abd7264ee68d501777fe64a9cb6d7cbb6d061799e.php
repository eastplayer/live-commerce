<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/testimonials.htm */
class __TwigTemplate_e09f674f4722910bd6f1abae687ca19ac205d2c3795869ca35a50f3cb21408c9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["testimonialBlock"] = twig_get_attribute($this->env, $this->source, ($context["TestimonialBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["testimonialBlock"] ?? null), "testimonials", [], "any", false, false, true, 2))) {
            // line 3
            echo "<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"bizagn--section-title text-center\">
            <p>";
            // line 6
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["testimonialBlock"] ?? null), "subtitle", [], "any", false, false, true, 6), 6, $this->source), "html", null, true);
            echo "</p>
            <h2>";
            // line 7
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["testimonialBlock"] ?? null), "title", [], "any", false, false, true, 7), 7, $this->source), "html", null, true);
            echo "</h2>
        </div>
        <div class=\"row\">
            <div class=\"col-md-8 offset-md-2\">
                <div class=\"bizagn--testi-slider bizagn--owl-btn owl-carousel bizagn--nav-95px\" data-owl-nav=\"true\"
                    data-owl-autoplay=\"false\" data-bg-img=\"";
            // line 12
            echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/testimonial/map-bg.png");
            echo "\">
                    ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["testimonialBlock"] ?? null), "testimonials", [], "any", false, false, true, 13));
            foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
                // line 14
                echo "                    <div class=\"bizagn--testi-slider-item\">
                        <div class=\"bizagn--testi-item text-center\">
                            ";
                // line 16
                if (twig_get_attribute($this->env, $this->source, $context["review"], "image", [], "any", false, false, true, 16)) {
                    // line 17
                    echo "                            <img width=\"100\" src=\"";
                    echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["review"], "image", [], "any", false, false, true, 17), 17, $this->source));
                    echo "\" alt=\"\">
                            ";
                }
                // line 19
                echo "                            <p>“";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["review"], "content", [], "any", false, false, true, 19), 19, $this->source), "html", null, true);
                echo "”</p>
                            <div class=\"client-pos\"> <span>";
                // line 20
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["review"], "name", [], "any", false, false, true, 20), 20, $this->source), "html", null, true);
                echo " / </span> ";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["review"], "title", [], "any", false, false, true, 20), 20, $this->source), "html", null, true);
                echo "</div>
                        </div>
                    </div>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "                </div>
            </div>
        </div>
    </div>
</section>
";
        }
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/testimonials.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 24,  85 => 20,  80 => 19,  74 => 17,  72 => 16,  68 => 14,  64 => 13,  60 => 12,  52 => 7,  48 => 6,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set testimonialBlock = TestimonialBlock.data %}
{% if testimonialBlock.testimonials | length %}
<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"bizagn--section-title text-center\">
            <p>{{testimonialBlock.subtitle}}</p>
            <h2>{{testimonialBlock.title}}</h2>
        </div>
        <div class=\"row\">
            <div class=\"col-md-8 offset-md-2\">
                <div class=\"bizagn--testi-slider bizagn--owl-btn owl-carousel bizagn--nav-95px\" data-owl-nav=\"true\"
                    data-owl-autoplay=\"false\" data-bg-img=\"{{'assets/images/testimonial/map-bg.png'|theme}}\">
                    {% for review in testimonialBlock.testimonials %}
                    <div class=\"bizagn--testi-slider-item\">
                        <div class=\"bizagn--testi-item text-center\">
                            {% if review.image %}
                            <img width=\"100\" src=\"{{review.image|media}}\" alt=\"\">
                            {% endif %}
                            <p>“{{ review.content }}”</p>
                            <div class=\"client-pos\"> <span>{{ review.name }} / </span> {{ review.title }}</div>
                        </div>
                    </div>
                    {% endfor %}
                </div>
            </div>
        </div>
    </div>
</section>
{% endif %}", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/testimonials.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 2, "for" => 13);
        static $filters = array("length" => 2, "escape" => 6, "theme" => 12, "media" => 17);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['length', 'escape', 'theme', 'media'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
