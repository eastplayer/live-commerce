<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/footer-scripts.htm */
class __TwigTemplate_ffa339bc5ed40fbb2937fbc1565643462f241f66015f85b7538f0325dc2441ec extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script src=\"";
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/jquery-3.3.1.min.js");
        echo "\"></script>
<script src=\"";
        // line 2
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/bootstrap.bundle.min.js");
        echo "\"></script>
<script src=\"";
        // line 3
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/plugins/owl-carousel/owl.carousel.min.js");
        echo "\"></script>
<script src=\"";
        // line 4
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/plugins/menumaker/menumaker.js");
        echo "\"></script>
<script src=\"";
        // line 5
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/plugins/isotope/isotope.pkgd.min.js");
        echo "\"></script>
<script src=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/plugins/isotope/imagesloaded.pkgd.min.js");
        echo "\"></script>
<script src=\"";
        // line 7
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/plugins/ratinajs/retina.min.js");
        echo "\"></script>
<script src=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/scripts.js");
        echo "\"></script>
<script src=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/js/custom.js");
        echo "\"></script>
<script src=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/main.js");
        echo "\"></script>
";
        // line 11
        $_minify = System\Classes\CombineAssets::instance()->useMinify;
        if ($_minify) {
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.combined-min.js"></script>'.PHP_EOL;
        }
        else {
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.js"></script>'.PHP_EOL;
            echo '<script src="' . Request::getBasePath() . '/modules/system/assets/js/framework.extras.js"></script>'.PHP_EOL;
        }
        echo '<link rel="stylesheet" property="stylesheet" href="' . Request::getBasePath() .'/modules/system/assets/css/framework.extras'.($_minify ? '-min' : '').'.css">'.PHP_EOL;
        unset($_minify);
        // line 12
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('js');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('scripts');
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/footer-scripts.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 12,  80 => 11,  76 => 10,  72 => 9,  68 => 8,  64 => 7,  60 => 6,  56 => 5,  52 => 4,  48 => 3,  44 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<script src=\"{{'assets/js/jquery-3.3.1.min.js'|theme}}\"></script>
<script src=\"{{'assets/js/bootstrap.bundle.min.js'|theme}}\"></script>
<script src=\"{{'assets/plugins/owl-carousel/owl.carousel.min.js'|theme}}\"></script>
<script src=\"{{'assets/plugins/menumaker/menumaker.js'|theme}}\"></script>
<script src=\"{{'assets/plugins/isotope/isotope.pkgd.min.js'|theme}}\"></script>
<script src=\"{{'assets/plugins/isotope/imagesloaded.pkgd.min.js'|theme}}\"></script>
<script src=\"{{'assets/plugins/ratinajs/retina.min.js'|theme}}\"></script>
<script src=\"{{'assets/js/scripts.js'|theme}}\"></script>
<script src=\"{{'assets/js/custom.js'|theme}}\"></script>
<script src=\"{{'assets/main.js'|theme}}\"></script>
{% framework extras %}
{% scripts %}", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/footer-scripts.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("framework" => 11, "scripts" => 12);
        static $filters = array("theme" => 1);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['framework', 'scripts'],
                ['theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
