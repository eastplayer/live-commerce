<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/layouts/main.htm */
class __TwigTemplate_ace2b290c991921bd672178eb896789d3a7336cba09adfac167f8977245988cc extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["includeBlock"] = twig_get_attribute($this->env, $this->source, ($context["IncludeBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        $context["companyBlock"] = twig_get_attribute($this->env, $this->source, ($context["CompanyBlock"] ?? null), "data", [], "any", false, false, true, 2);
        // line 3
        echo "<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    ";
        // line 10
        if (twig_get_attribute($this->env, $this->source, ($context["companyBlock"] ?? null), "title", [], "any", false, false, true, 10)) {
            // line 11
            echo "    <title>";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["companyBlock"] ?? null), "title", [], "any", false, false, true, 11), 11, $this->source), "html", null, true);
            echo "</title>
    ";
        }
        // line 13
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["companyBlock"] ?? null), "description", [], "any", false, false, true, 13)) {
            // line 14
            echo "    <meta name=\"description\" content=\"";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["companyBlock"] ?? null), "description", [], "any", false, false, true, 14), 14, $this->source), "html", null, true);
            echo "\">
    ";
        }
        // line 16
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["companyBlock"] ?? null), "favicon", [], "any", false, false, true, 16)) {
            // line 17
            echo "    <link rel=\"icon\" type=\"image/png\" href=\"";
            echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["companyBlock"] ?? null), "favicon", [], "any", false, false, true, 17), 17, $this->source));
            echo "\" />
    ";
        }
        // line 19
        echo "    ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("layout/header-scripts"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 20
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["includeBlock"] ?? null), "header_include", [], "any", false, false, true, 20)) {
            // line 21
            echo "    ";
            echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["includeBlock"] ?? null), "header_include", [], "any", false, false, true, 21), 21, $this->source);
            echo "
    ";
        }
        // line 23
        echo "    ";
        $context['__cms_component_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->componentFunction("seo"        , $context['__cms_component_params']        );
        unset($context['__cms_component_params']);
        // line 24
        echo "</head>

<body>
    ";
        // line 27
        if (twig_get_attribute($this->env, $this->source, ($context["includeBlock"] ?? null), "body_include", [], "any", false, false, true, 27)) {
            // line 28
            echo "    ";
            echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["includeBlock"] ?? null), "body_include", [], "any", false, false, true, 28), 28, $this->source);
            echo "
    ";
        }
        // line 30
        echo "    ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("layout/header"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 31
        echo "    ";
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFunction();
        // line 32
        echo "    ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("layout/footer"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 33
        echo "    ";
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("layout/footer-scripts"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 34
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["includeBlock"] ?? null), "footer_include", [], "any", false, false, true, 34)) {
            // line 35
            echo "    ";
            echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["includeBlock"] ?? null), "footer_include", [], "any", false, false, true, 35), 35, $this->source);
            echo "
    ";
        }
        // line 37
        echo "</body>

</html>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/layouts/main.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 37,  131 => 35,  128 => 34,  123 => 33,  118 => 32,  115 => 31,  110 => 30,  104 => 28,  102 => 27,  97 => 24,  92 => 23,  86 => 21,  83 => 20,  78 => 19,  72 => 17,  69 => 16,  63 => 14,  60 => 13,  54 => 11,  52 => 10,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set includeBlock = IncludeBlock.data %}
{% set companyBlock = CompanyBlock.data %}
<!DOCTYPE html>
<html lang=\"en\">

<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    {% if companyBlock.title %}
    <title>{{companyBlock.title}}</title>
    {% endif %}
    {% if companyBlock.description %}
    <meta name=\"description\" content=\"{{companyBlock.description}}\">
    {% endif %}
    {% if companyBlock.favicon %}
    <link rel=\"icon\" type=\"image/png\" href=\"{{companyBlock.favicon|media}}\" />
    {% endif %}
    {% partial 'layout/header-scripts' %}
    {% if includeBlock.header_include %}
    {{includeBlock.header_include|raw}}
    {% endif %}
    {% component 'seo' %}
</head>

<body>
    {% if includeBlock.body_include %}
    {{includeBlock.body_include|raw}}
    {% endif %}
    {% partial 'layout/header' %}
    {% page %}
    {% partial 'layout/footer' %}
    {% partial 'layout/footer-scripts' %}
    {% if includeBlock.footer_include %}
    {{includeBlock.footer_include|raw}}
    {% endif %}
</body>

</html>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/layouts/main.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 10, "partial" => 19, "component" => 23, "page" => 31);
        static $filters = array("escape" => 11, "media" => 17, "raw" => 21);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'partial', 'component', 'page'],
                ['escape', 'media', 'raw'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
