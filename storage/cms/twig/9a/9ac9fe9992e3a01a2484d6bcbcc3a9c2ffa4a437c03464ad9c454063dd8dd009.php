<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/blog.htm */
class __TwigTemplate_1a1de4e24053bb35feb2b7c9c946909e2049785943651b5a5f31a1d53a0febef extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["post"] = twig_get_attribute($this->env, $this->source, ($context["blogPost"] ?? null), "post", [], "any", false, false, true, 1);
        // line 2
        echo "<div class=\"bizagn--page-title\">
    <div class=\"bizagn--page-bc\">
        <ul>
            <li><a href=\"";
        // line 5
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("index");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Trang chủ"]);
        echo "</a></li>
            <li><a href=\"{'blogs'|page({c: ''})}}\">";
        // line 6
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tin tức"]);
        echo "</a></li>
            <li>";
        // line 7
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, true, 7), 7, $this->source), "html", null, true);
        echo "</li>
        </ul>
    </div>
</div>
<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-8\">
                <div class=\"post-details-cover\">
                    <div class=\"post-thumb-cover\">
                        ";
        // line 17
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "featured_images", [], "any", false, false, true, 17), "count", [], "any", false, false, true, 17)) {
            // line 18
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "featured_images", [], "any", false, false, true, 18), 0, 1));
            foreach ($context['_seq'] as $context["_key"] => $context["image"]) {
                // line 19
                echo "                        <div class=\"post-thumb\">
                            <img src=\"";
                // line 20
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["image"], "getThumb", [0 => 1110, 1 => 700, 2 => "crop"], "method", false, false, true, 20), 20, $this->source), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, true, 20), 20, $this->source), "html", null, true);
                echo "\" class=\"img-fluid\"
                                data-rjs=\"2\">
                        </div>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['image'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "                        ";
        }
        // line 25
        echo "                        <div class=\"post-meta-info\">
                            ";
        // line 26
        if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "categories", [], "any", false, false, true, 26), "count", [], "any", false, false, true, 26)) {
            // line 27
            echo "                            <p class=\"cats\">
                                ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "categories", [], "any", false, false, true, 28));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 29
                echo "                                <a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs", ["c" => twig_get_attribute($this->env, $this->source, $context["category"], "slug", [], "any", false, false, true, 29)]);
                echo "\">";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, true, 29), 29, $this->source), "html", null, true);
                echo "</a>";
                if ( !twig_get_attribute($this->env, $this->source,                 // line 30
$context["loop"], "last", [], "any", false, false, true, 30)) {
                    echo ", ";
                }
                // line 31
                echo "                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "                            </p>
                            ";
        }
        // line 34
        echo "                            <div class=\"title\">
                                <h2>";
        // line 35
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "title", [], "any", false, false, true, 35), 35, $this->source), "html", null, true);
        echo "</h2>
                            </div>
                            <ul class=\"nav meta align-items-center\">
                                <li class=\"meta-date\"><a href=\"#\">";
        // line 38
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "published_at", [], "any", false, false, true, 38), 38, $this->source), "d m, Y"), "html", null, true);
        echo "</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class=\"post-content-cover my-drop-cap\">
                        ";
        // line 43
        echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "content_html", [], "any", false, false, true, 43), 43, $this->source);
        echo "
                    </div>
                    ";
        // line 45
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "tags", [], "any", false, false, true, 45))) {
            // line 46
            echo "                    <div class=\"post-all-tags\">
                        ";
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["post"] ?? null), "tags", [], "any", false, false, true, 47));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 48
                echo "                        <a href=\"";
                echo $this->extensions['Cms\Twig\Extension']->pageFilter("tags", ["tag" => twig_get_attribute($this->env, $this->source, $context["tag"], "slug", [], "any", false, false, true, 48)]);
                echo "\">";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["tag"], "name", [], "any", false, false, true, 48), 48, $this->source), "html", null, true);
                echo "</a>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 50
            echo "                    </div>
                    ";
        }
        // line 52
        echo "                    <div class=\"post-about-author-box\">
                        <div class=\"author-desc\">
                            <div class=\"social-icons\">
                                ";
        // line 55
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/share"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 56
        echo "                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-4\">
                ";
        // line 62
        $context['__cms_partial_params'] = [];
        $context['__cms_partial_params']['category'] = ($context["category"] ?? null)        ;
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/sidebar"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 63
        echo "            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/blog.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  215 => 63,  210 => 62,  202 => 56,  198 => 55,  193 => 52,  189 => 50,  178 => 48,  174 => 47,  171 => 46,  169 => 45,  164 => 43,  156 => 38,  150 => 35,  147 => 34,  143 => 32,  129 => 31,  125 => 30,  119 => 29,  102 => 28,  99 => 27,  97 => 26,  94 => 25,  91 => 24,  79 => 20,  76 => 19,  71 => 18,  69 => 17,  56 => 7,  52 => 6,  46 => 5,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set post = blogPost.post %}
<div class=\"bizagn--page-title\">
    <div class=\"bizagn--page-bc\">
        <ul>
            <li><a href=\"{{'index'|page}}\">{{'Trang chủ'|_}}</a></li>
            <li><a href=\"{'blogs'|page({c: ''})}}\">{{'Tin tức'|_}}</a></li>
            <li>{{post.title}}</li>
        </ul>
    </div>
</div>
<section class=\"pt-120 pb-120\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-lg-8\">
                <div class=\"post-details-cover\">
                    <div class=\"post-thumb-cover\">
                        {% if post.featured_images.count %}
                        {% for image in post.featured_images|slice(0, 1) %}
                        <div class=\"post-thumb\">
                            <img src=\"{{ image.getThumb(1110, 700, 'crop') }}\" alt=\"{{post.title}}\" class=\"img-fluid\"
                                data-rjs=\"2\">
                        </div>
                        {% endfor %}
                        {% endif %}
                        <div class=\"post-meta-info\">
                            {% if post.categories.count %}
                            <p class=\"cats\">
                                {% for category in post.categories %}
                                <a href=\"{{ 'blogs'|page({c:category.slug}) }}\">{{ category.name }}</a>{% if not
                                loop.last %}, {% endif %}
                                {% endfor %}
                            </p>
                            {% endif %}
                            <div class=\"title\">
                                <h2>{{post.title}}</h2>
                            </div>
                            <ul class=\"nav meta align-items-center\">
                                <li class=\"meta-date\"><a href=\"#\">{{ post.published_at|date('d m, Y') }}</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class=\"post-content-cover my-drop-cap\">
                        {{ post.content_html|raw }}
                    </div>
                    {% if post.tags|length %}
                    <div class=\"post-all-tags\">
                        {% for tag in post.tags %}
                        <a href=\"{{'tags'|page({tag: tag.slug})}}\">{{ tag.name }}</a>
                        {% endfor %}
                    </div>
                    {% endif %}
                    <div class=\"post-about-author-box\">
                        <div class=\"author-desc\">
                            <div class=\"social-icons\">
                                {% partial 'blog/share' %}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\"col-lg-4\">
                {% partial 'blog/sidebar' category=category %}
            </div>
        </div>
    </div>
</section>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/pages/blog.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 17, "for" => 18, "partial" => 55);
        static $filters = array("page" => 5, "_" => 5, "escape" => 7, "slice" => 18, "date" => 38, "raw" => 43, "length" => 45);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for', 'partial'],
                ['page', '_', 'escape', 'slice', 'date', 'raw', 'length'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
