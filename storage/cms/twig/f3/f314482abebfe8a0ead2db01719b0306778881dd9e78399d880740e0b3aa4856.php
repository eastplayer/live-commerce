<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/blogs.htm */
class __TwigTemplate_62c1aa768bbac8856fb0d906f7954491d232cf9b79f7fca786614ec72c87fda9 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["posts"] = twig_get_attribute($this->env, $this->source, ($context["blogPosts"] ?? null), "posts", [], "any", false, false, true, 1);
        // line 2
        if (twig_length_filter($this->env, ($context["posts"] ?? null))) {
            // line 3
            echo "<section class=\"pt-120 pb-70\" data-bg-img=\"";
            echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/blogs/bg.png");
            echo "\">
    <div class=\"container\">
        <div class=\"bizagn--section-title\">
            <p>";
            // line 6
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tin tức"]);
            echo "</p>
            <h2>";
            // line 7
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Xem tin mới từ chúng tôi"]);
            echo "</h2>
        </div>

        <div class=\"row bizagn--blog-section\">
            ";
            // line 11
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["posts"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                // line 12
                echo "            <div class=\"col-md-6\">
                ";
                // line 13
                $context['__cms_partial_params'] = [];
                $context['__cms_partial_params']['post'] = $context["post"]                ;
                echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("blog/post"                , $context['__cms_partial_params']                , true                );
                unset($context['__cms_partial_params']);
                // line 14
                echo "            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "            <a href=\"";
            echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs", ["c" => ""]);
            echo "\" class=\"bizagn--rm-btn\">";
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Xem thêm"]);
            echo " <i
                    class=\"fa fa-angle-right\"></i></a>
        </div>
    </div>
</section>
";
        }
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/blogs.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 16,  73 => 14,  68 => 13,  65 => 12,  61 => 11,  54 => 7,  50 => 6,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set posts = blogPosts.posts %}
{% if posts | length %}
<section class=\"pt-120 pb-70\" data-bg-img=\"{{'assets/images/blogs/bg.png'|theme}}\">
    <div class=\"container\">
        <div class=\"bizagn--section-title\">
            <p>{{'Tin tức'|_}}</p>
            <h2>{{'Xem tin mới từ chúng tôi'|_}}</h2>
        </div>

        <div class=\"row bizagn--blog-section\">
            {% for post in posts %}
            <div class=\"col-md-6\">
                {% partial 'blog/post' post=post %}
            </div>
            {% endfor %}
            <a href=\"{{'blogs'|page({c: ''})}}\" class=\"bizagn--rm-btn\">{{'Xem thêm'|_}} <i
                    class=\"fa fa-angle-right\"></i></a>
        </div>
    </div>
</section>
{% endif %}", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/blogs.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 2, "for" => 11, "partial" => 13);
        static $filters = array("length" => 2, "theme" => 3, "_" => 6, "page" => 16);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for', 'partial'],
                ['length', 'theme', '_', 'page'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
