<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/footer.htm */
class __TwigTemplate_4798fe906ed718fe4b05dec799e083aa79cc6e9093af948b80ed864e3e0e267a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["companyBlock"] = twig_get_attribute($this->env, $this->source, ($context["CompanyBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        $context["socialsBlock"] = twig_get_attribute($this->env, $this->source, ($context["SocialsBlock"] ?? null), "data", [], "any", false, false, true, 2);
        // line 3
        $context["contactBlock"] = twig_get_attribute($this->env, $this->source, ($context["ContactBlock"] ?? null), "data", [], "any", false, false, true, 3);
        // line 4
        echo "<footer class=\"bizagn--footer\">
    <div class=\"bizagn--footer-top\" data-bg-img=\"";
        // line 5
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/footer-map.png");
        echo "\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-4 col-md-6\">
                    <div class=\"bizagn--widget footer-widget\">
                        <div class=\"bizagn--widget-content\">
                            <a href=\"#\">
                                <img src=\"";
        // line 12
        echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["companyBlock"] ?? null), "logo", [], "any", false, false, true, 12), 12, $this->source));
        echo "\" alt=\"\" class=\"img-fluid\" data-rjs=\"2\">
                            </a> <br> <br> <br>
                            <p>";
        // line 14
        echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["companyBlock"] ?? null), "slogan", [], "any", false, false, true, 14), 14, $this->source), "html", null, true);
        echo "</p>
                            ";
        // line 15
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["socialsBlock"] ?? null), "socials", [], "any", false, false, true, 15))) {
            // line 16
            echo "                            <div class=\"widget-social\">
                                ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["socialsBlock"] ?? null), "socials", [], "any", false, false, true, 17));
            foreach ($context['_seq'] as $context["_key"] => $context["social"]) {
                // line 18
                echo "                                <a target=\"_blank\" href=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["social"], "url", [], "any", false, false, true, 18), 18, $this->source), "html", null, true);
                echo "\"><i class=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["social"], "icon", [], "any", false, false, true, 18), 18, $this->source), "html", null, true);
                echo "\"></i></a>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['social'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "                            </div>
                            ";
        }
        // line 22
        echo "                        </div>
                    </div>
                </div>
                <div class=\"col-lg-4 col-md-6\">
                    <div class=\"bizagn--widget footer-widget\">
                        <div class=\"bizagn--widget-title\">
                            <h3>";
        // line 28
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Liên hệ"]);
        echo "</h3>
                        </div>
                        <div class=\"bizagn--widget-content\">
                            <ul class=\"bizagn--menu\">
                                ";
        // line 32
        if (twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "address", [], "any", false, false, true, 32)) {
            // line 33
            echo "                                <li class=\"mb-2\"><span>";
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Địa chỉ"]);
            echo ": </span> ";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "address", [], "any", false, false, true, 33), 33, $this->source), "html", null, true);
            echo "</li>
                                ";
        }
        // line 35
        echo "                                ";
        if ((twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "email_anchor", [], "any", false, false, true, 35) && twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "email", [], "any", false, false, true, 35))) {
            // line 36
            echo "                                <li class=\"mb-2\"><span>";
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Địa chỉ Email"]);
            echo ": </span> <a
                                        href=\"mailto:";
            // line 37
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "email", [], "any", false, false, true, 37), 37, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "email_anchor", [], "any", false, false, true, 37), 37, $this->source), "html", null, true);
            echo "</a></li>
                                ";
        }
        // line 39
        echo "                                ";
        if ((twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "phone_anchor", [], "any", false, false, true, 39) && twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "phone", [], "any", false, false, true, 39))) {
            // line 40
            echo "                                <li class=\"mb-2\"><span>";
            echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Số điện thoại"]);
            echo ": </span> <a
                                        href=\"tel:";
            // line 41
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "phone", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["contactBlock"] ?? null), "phone_anchor", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
            echo "</a></li>
                                ";
        }
        // line 43
        echo "                            </ul>
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-4 col-md-12\">
                    <div class=\"bizagn--widget footer-widget\">
                        <div class=\"bizagn--widget-title\">
                            <h3>";
        // line 50
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Đường dẫn"]);
        echo "</h3>
                        </div>
                        <div class=\"bizagn--widget-content\">
                            <p class=\"mb-2\"><a href=\"";
        // line 53
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("contact");
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Liên hệ"]);
        echo "</a></p>
                            <p class=\"mb-2\"><a href=\"";
        // line 54
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("blogs", ["c" => ""]);
        echo "\">";
        echo call_user_func_array($this->env->getFilter('_')->getCallable(), ["Tin tức"]);
        echo "</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"bizagn--footer-bottom\">
        <div class=\"container\">
            <div class=\"bizagn--footer-cradit text-center\">
                <p>Copyright © ";
        // line 64
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " Live Commerce. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>
<div class=\"back-to-top d-flex align-items-center justify-content-center\">
    <span><i class=\"fa fa-angle-up\"></i></span>
</div>";
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 64,  167 => 54,  161 => 53,  155 => 50,  146 => 43,  139 => 41,  134 => 40,  131 => 39,  124 => 37,  119 => 36,  116 => 35,  108 => 33,  106 => 32,  99 => 28,  91 => 22,  87 => 20,  76 => 18,  72 => 17,  69 => 16,  67 => 15,  63 => 14,  58 => 12,  48 => 5,  45 => 4,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set companyBlock = CompanyBlock.data %}
{% set socialsBlock = SocialsBlock.data %}
{% set contactBlock = ContactBlock.data %}
<footer class=\"bizagn--footer\">
    <div class=\"bizagn--footer-top\" data-bg-img=\"{{'assets/images/footer-map.png'|theme}}\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-lg-4 col-md-6\">
                    <div class=\"bizagn--widget footer-widget\">
                        <div class=\"bizagn--widget-content\">
                            <a href=\"#\">
                                <img src=\"{{companyBlock.logo|media}}\" alt=\"\" class=\"img-fluid\" data-rjs=\"2\">
                            </a> <br> <br> <br>
                            <p>{{companyBlock.slogan}}</p>
                            {% if socialsBlock.socials | length %}
                            <div class=\"widget-social\">
                                {% for social in socialsBlock.socials %}
                                <a target=\"_blank\" href=\"{{social.url}}\"><i class=\"{{social.icon}}\"></i></a>
                                {% endfor %}
                            </div>
                            {% endif %}
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-4 col-md-6\">
                    <div class=\"bizagn--widget footer-widget\">
                        <div class=\"bizagn--widget-title\">
                            <h3>{{'Liên hệ'|_}}</h3>
                        </div>
                        <div class=\"bizagn--widget-content\">
                            <ul class=\"bizagn--menu\">
                                {% if contactBlock.address %}
                                <li class=\"mb-2\"><span>{{'Địa chỉ'|_}}: </span> {{contactBlock.address}}</li>
                                {% endif %}
                                {% if contactBlock.email_anchor and contactBlock.email %}
                                <li class=\"mb-2\"><span>{{'Địa chỉ Email'|_}}: </span> <a
                                        href=\"mailto:{{contactBlock.email}}\">{{contactBlock.email_anchor}}</a></li>
                                {% endif %}
                                {% if contactBlock.phone_anchor and contactBlock.phone %}
                                <li class=\"mb-2\"><span>{{'Số điện thoại'|_}}: </span> <a
                                        href=\"tel:{{contactBlock.phone}}\">{{contactBlock.phone_anchor}}</a></li>
                                {% endif %}
                            </ul>
                        </div>
                    </div>
                </div>
                <div class=\"col-lg-4 col-md-12\">
                    <div class=\"bizagn--widget footer-widget\">
                        <div class=\"bizagn--widget-title\">
                            <h3>{{'Đường dẫn'|_}}</h3>
                        </div>
                        <div class=\"bizagn--widget-content\">
                            <p class=\"mb-2\"><a href=\"{{'contact'|page}}\">{{'Liên hệ'|_}}</a></p>
                            <p class=\"mb-2\"><a href=\"{{'blogs'|page({c: ''})}}\">{{'Tin tức'|_}}</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"bizagn--footer-bottom\">
        <div class=\"container\">
            <div class=\"bizagn--footer-cradit text-center\">
                <p>Copyright © {{'now'|date('Y')}} Live Commerce. All Rights Reserved.</p>
            </div>
        </div>
    </div>
</footer>
<div class=\"back-to-top d-flex align-items-center justify-content-center\">
    <span><i class=\"fa fa-angle-up\"></i></span>
</div>", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/footer.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 15, "for" => 17);
        static $filters = array("theme" => 5, "media" => 12, "escape" => 14, "length" => 15, "_" => 28, "page" => 53, "date" => 64);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['theme', 'media', 'escape', 'length', '_', 'page', 'date'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
