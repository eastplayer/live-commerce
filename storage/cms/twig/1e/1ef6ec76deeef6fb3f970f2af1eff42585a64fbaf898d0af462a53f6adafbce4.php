<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/services.htm */
class __TwigTemplate_c4c0894bf98537ded0ee123f263b87602af22a4e2932b29ad85763d227d0548c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["servicesBlock"] = twig_get_attribute($this->env, $this->source, ($context["ServicesBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["servicesBlock"] ?? null), "services", [], "any", false, false, true, 2))) {
            // line 3
            echo "<!-- <section class=\"pt-120 bg-f8\">
    <div class=\"container\">
        <div class=\"bizagn--section-title\">
            <p>";
            // line 6
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["servicesBlock"] ?? null), "title", [], "any", false, false, true, 6), 6, $this->source), "html", null, true);
            echo "</p>
            <h2>";
            // line 7
            echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["servicesBlock"] ?? null), "subtitle", [], "any", false, false, true, 7), 7, $this->source);
            echo "</h2>
        </div>
        <div class=\"bizagn--service-slider bizagn--owl-btn owl-carousel bizagn--nav-95px\" data-owl-items=\"3\"
            data-owl-nav=\"true\" data-owl-margin=\"30\" data-owl-autoplay=\"false\"
            data-owl-responsive='{\"0\": {\"items\": \"1\"},\"768\": {\"items\": \"2\"}, \"992\": {\"items\": \"3\"}}'>
            ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["servicesBlock"] ?? null), "services", [], "any", false, false, true, 12));
            foreach ($context['_seq'] as $context["_key"] => $context["service"]) {
                // line 13
                echo "            <div class=\"bizagn--service-slider-item\">
                <div class=\"bizagn--service-item\">
                    ";
                // line 15
                if (twig_get_attribute($this->env, $this->source, $context["service"], "image", [], "any", false, false, true, 15)) {
                    // line 16
                    echo "                    <img src=\"";
                    echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["service"], "image", [], "any", false, false, true, 16), 16, $this->source));
                    echo "\" alt=\"\" class=\"img-fluid svg\">
                    ";
                }
                // line 18
                echo "                    <h3>";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["service"], "title", [], "any", false, false, true, 18), 18, $this->source), "html", null, true);
                echo "</h3>
                    <p>";
                // line 19
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["service"], "overview", [], "any", false, false, true, 19), 19, $this->source), "html", null, true);
                echo "</p>
                </div>
            </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['service'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "        </div>
    </div>
</section> -->
<section class=\"pt-120 pb-90 bg-f8\">
    <div class=\"container\">
        <div class=\"bizagn--section-title\">
            <p>";
            // line 29
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["servicesBlock"] ?? null), "title", [], "any", false, false, true, 29), 29, $this->source), "html", null, true);
            echo "</p>
            <h2>";
            // line 30
            echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["servicesBlock"] ?? null), "subtitle", [], "any", false, false, true, 30), 30, $this->source);
            echo "</h2>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-6\">
                <div class=\"bizagn--faq-items accordion\" id=\"bizagn--accordion\">
                    ";
            // line 35
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, ($context["servicesBlock"] ?? null), "services", [], "any", false, false, true, 35));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["service"]) {
                // line 36
                echo "                    <div class=\"bizagn--faq-item\">
                        <div ";
                // line 37
                if (twig_get_attribute($this->env, $this->source, $context["service"], "image", [], "any", false, false, true, 37)) {
                    echo "data-image=\"";
                    echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["service"], "image", [], "any", false, false, true, 37), 37, $this->source));
                    echo "\" ";
                }
                // line 38
                echo "                            onclick=\"changeImage(this)\" class=\"target-area";
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, true, 38) == 1)) {
                    echo " active";
                }
                echo "\"
                            id=\"target-area\">
                            <h3 data-toggle=\"collapse\" data-target=\"#expand-area-";
                // line 40
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, true, 40), 40, $this->source), "html", null, true);
                echo "\" aria-expanded=\"true\"
                                aria-controls=\"expand-area-";
                // line 41
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
                echo "\">
                                <span>";
                // line 42
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, true, 42), 42, $this->source), "html", null, true);
                echo ".</span> ";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["service"], "title", [], "any", false, false, true, 42), 42, $this->source), "html", null, true);
                echo "
                            </h3>
                        </div>
                        <div id=\"expand-area-";
                // line 45
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, true, 45), 45, $this->source), "html", null, true);
                echo "\"
                            class=\"collapse expand-area service-item";
                // line 46
                if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, true, 46) == 1)) {
                    echo " show";
                }
                echo "\"
                            aria-labelledby=\"target-area\" data-parent=\"#bizagn--accordion\">
                            <p class=\"font-weight-bold p-0 my-2\">";
                // line 48
                echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["service"], "overview", [], "any", false, false, true, 48), 48, $this->source);
                echo "</p>
                            ";
                // line 49
                echo $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["service"], "description", [], "any", false, false, true, 49), 49, $this->source);
                echo "
                        </div>
                    </div>
                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['service'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "                </div>
            </div>
            <div class=\"col-lg-5 offset-lg-1 d-flex align-items-bottom justify-content-center bizagn--faq-img\">
                ";
            // line 56
            if (twig_get_attribute($this->env, $this->source, (($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 = twig_get_attribute($this->env, $this->source, ($context["servicesBlock"] ?? null), "services", [], "any", false, false, true, 56)) && is_array($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4) || $__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4 instanceof ArrayAccess ? ($__internal_f607aeef2c31a95a7bf963452dff024ffaeb6aafbe4603f9ca3bec57be8633f4[0] ?? null) : null), "image", [], "any", false, false, true, 56)) {
                // line 57
                echo "                <div class=\"bizagn--faq-img-cover\">
                    <img id=\"serviceImage\" src=\"";
                // line 58
                echo $this->extensions['System\Twig\Extension']->mediaFilter($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, (($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 = twig_get_attribute($this->env, $this->source, ($context["servicesBlock"] ?? null), "services", [], "any", false, false, true, 58)) && is_array($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144) || $__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144 instanceof ArrayAccess ? ($__internal_62824350bc4502ee19dbc2e99fc6bdd3bd90e7d8dd6e72f42c35efd048542144[0] ?? null) : null), "image", [], "any", false, false, true, 58), 58, $this->source));
                echo "\" alt=\"\" class=\"img-fluid\">
                </div>
                ";
            }
            // line 61
            echo "            </div>
        </div>
    </div>
</section>
";
        }
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/services.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  210 => 61,  204 => 58,  201 => 57,  199 => 56,  194 => 53,  176 => 49,  172 => 48,  165 => 46,  161 => 45,  153 => 42,  149 => 41,  145 => 40,  137 => 38,  131 => 37,  128 => 36,  111 => 35,  103 => 30,  99 => 29,  91 => 23,  81 => 19,  76 => 18,  70 => 16,  68 => 15,  64 => 13,  60 => 12,  52 => 7,  48 => 6,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set servicesBlock = ServicesBlock.data %}
{% if servicesBlock.services | length %}
<!-- <section class=\"pt-120 bg-f8\">
    <div class=\"container\">
        <div class=\"bizagn--section-title\">
            <p>{{servicesBlock.title}}</p>
            <h2>{{servicesBlock.subtitle|raw}}</h2>
        </div>
        <div class=\"bizagn--service-slider bizagn--owl-btn owl-carousel bizagn--nav-95px\" data-owl-items=\"3\"
            data-owl-nav=\"true\" data-owl-margin=\"30\" data-owl-autoplay=\"false\"
            data-owl-responsive='{\"0\": {\"items\": \"1\"},\"768\": {\"items\": \"2\"}, \"992\": {\"items\": \"3\"}}'>
            {% for service in servicesBlock.services %}
            <div class=\"bizagn--service-slider-item\">
                <div class=\"bizagn--service-item\">
                    {% if service.image %}
                    <img src=\"{{service.image|media}}\" alt=\"\" class=\"img-fluid svg\">
                    {% endif %}
                    <h3>{{service.title}}</h3>
                    <p>{{service.overview}}</p>
                </div>
            </div>
            {% endfor %}
        </div>
    </div>
</section> -->
<section class=\"pt-120 pb-90 bg-f8\">
    <div class=\"container\">
        <div class=\"bizagn--section-title\">
            <p>{{servicesBlock.title}}</p>
            <h2>{{servicesBlock.subtitle|raw}}</h2>
        </div>
        <div class=\"row\">
            <div class=\"col-lg-6\">
                <div class=\"bizagn--faq-items accordion\" id=\"bizagn--accordion\">
                    {% for service in servicesBlock.services %}
                    <div class=\"bizagn--faq-item\">
                        <div {% if service.image %}data-image=\"{{service.image|media}}\" {% endif %}
                            onclick=\"changeImage(this)\" class=\"target-area{% if loop.index == 1 %} active{% endif %}\"
                            id=\"target-area\">
                            <h3 data-toggle=\"collapse\" data-target=\"#expand-area-{{loop.index}}\" aria-expanded=\"true\"
                                aria-controls=\"expand-area-{{loop.index}}\">
                                <span>{{loop.index}}.</span> {{service.title}}
                            </h3>
                        </div>
                        <div id=\"expand-area-{{loop.index}}\"
                            class=\"collapse expand-area service-item{% if loop.index == 1 %} show{% endif %}\"
                            aria-labelledby=\"target-area\" data-parent=\"#bizagn--accordion\">
                            <p class=\"font-weight-bold p-0 my-2\">{{service.overview|raw}}</p>
                            {{service.description|raw}}
                        </div>
                    </div>
                    {% endfor %}
                </div>
            </div>
            <div class=\"col-lg-5 offset-lg-1 d-flex align-items-bottom justify-content-center bizagn--faq-img\">
                {% if servicesBlock.services[0].image %}
                <div class=\"bizagn--faq-img-cover\">
                    <img id=\"serviceImage\" src=\"{{servicesBlock.services[0].image|media}}\" alt=\"\" class=\"img-fluid\">
                </div>
                {% endif %}
            </div>
        </div>
    </div>
</section>
{% endif %}", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/services.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 2, "for" => 12);
        static $filters = array("length" => 2, "escape" => 6, "raw" => 7, "media" => 16);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['length', 'escape', 'raw', 'media'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
