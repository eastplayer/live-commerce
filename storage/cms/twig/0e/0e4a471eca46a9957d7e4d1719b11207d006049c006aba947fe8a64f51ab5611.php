<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/header-scripts.htm */
class __TwigTemplate_a9e76ace4d3b76a61cb262795c913c77f44bc6c7b854515a9a281bfc8b6c23f0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
<link href=\"https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap\"
    rel=\"stylesheet\">
<link href=\"https://fonts.googleapis.com/css2?family=Raleway:wght@400;600;700&display=swap\" rel=\"stylesheet\">
<link rel=\"stylesheet\" href=\"";
        // line 6
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/bootstrap.min.css");
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 7
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/font-awesome.min.css");
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 8
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/plugins/owl-carousel/owl.carousel.min.css");
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 9
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/plugins/animate/animate.min.css");
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 10
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/style.css");
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/responsive.css");
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 12
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/css/custom.css");
        echo "\">
<link rel=\"stylesheet\" href=\"";
        // line 13
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/theme.css");
        echo "\">
";
        // line 14
        echo $this->env->getExtension('Cms\Twig\Extension')->assetsFunction('css');
        echo $this->env->getExtension('Cms\Twig\Extension')->displayBlock('styles');
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/header-scripts.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 14,  74 => 13,  70 => 12,  66 => 11,  62 => 10,  58 => 9,  54 => 8,  50 => 7,  46 => 6,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<link rel=\"preconnect\" href=\"https://fonts.googleapis.com\">
<link rel=\"preconnect\" href=\"https://fonts.gstatic.com\" crossorigin>
<link href=\"https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800;900&display=swap\"
    rel=\"stylesheet\">
<link href=\"https://fonts.googleapis.com/css2?family=Raleway:wght@400;600;700&display=swap\" rel=\"stylesheet\">
<link rel=\"stylesheet\" href=\"{{'assets/css/bootstrap.min.css'|theme}}\">
<link rel=\"stylesheet\" href=\"{{'assets/css/font-awesome.min.css'|theme}}\">
<link rel=\"stylesheet\" href=\"{{'assets/plugins/owl-carousel/owl.carousel.min.css'|theme}}\">
<link rel=\"stylesheet\" href=\"{{'assets/plugins/animate/animate.min.css'|theme}}\">
<link rel=\"stylesheet\" href=\"{{'assets/css/style.css'|theme}}\">
<link rel=\"stylesheet\" href=\"{{'assets/css/responsive.css'|theme}}\">
<link rel=\"stylesheet\" href=\"{{'assets/css/custom.css'|theme}}\">
<link rel=\"stylesheet\" href=\"{{'assets/theme.css'|theme}}\">
{% styles %}", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/layout/header-scripts.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("styles" => 14);
        static $filters = array("theme" => 6);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['styles'],
                ['theme'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
