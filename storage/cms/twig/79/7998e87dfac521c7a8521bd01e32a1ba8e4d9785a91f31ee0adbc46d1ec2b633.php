<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* /www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/cta.htm */
class __TwigTemplate_5db93e7c156f9a3cddd5053b6cc32d4396e7269fed29f823e8dd1227a517405c extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["ctaBlock"] = twig_get_attribute($this->env, $this->source, ($context["CtaBlock"] ?? null), "data", [], "any", false, false, true, 1);
        // line 2
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["ctaBlock"] ?? null), "title", [], "any", false, false, true, 2))) {
            // line 3
            echo "<section class=\"pt-120 pb-120 bg-red\" data-bg-img=\"";
            echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/cta-bg.png");
            echo "\">
    <div class=\"container\">
        <div class=\"row align-items-center\">
            <div class=\"col-lg-7 col-md-8 offset-lg-1\">
                <h2>";
            // line 7
            echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["ctaBlock"] ?? null), "title", [], "any", false, false, true, 7), 7, $this->source), "html", null, true);
            echo "</h2>
                ";
            // line 8
            if (twig_get_attribute($this->env, $this->source, ($context["ctaBlock"] ?? null), "subtitle", [], "any", false, false, true, 8)) {
                // line 9
                echo "                <p>";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["ctaBlock"] ?? null), "subtitle", [], "any", false, false, true, 9), 9, $this->source), "html", null, true);
                echo "</p>
                ";
            }
            // line 11
            echo "            </div>
            <div class=\"col-lg-3 col-md-4 offset-lg-1\">
                ";
            // line 13
            if ((twig_get_attribute($this->env, $this->source, ($context["ctaBlock"] ?? null), "url", [], "any", false, false, true, 13) && twig_get_attribute($this->env, $this->source, ($context["ctaBlock"] ?? null), "url_anchor", [], "any", false, false, true, 13))) {
                // line 14
                echo "                <a href=\"";
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["ctaBlock"] ?? null), "url", [], "any", false, false, true, 14), 14, $this->source), "html", null, true);
                echo "\" class=\"btn bizagn--base-btn white-btn\">
                    <i class=\"fa fa-arrow-right svg left\"></i>
                    <!-- <img src=\"";
                // line 16
                echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/btn-right-arrow.svg");
                echo "\" alt=\"\" class=\"svg left\"> -->
                    ";
                // line 17
                echo twig_escape_filter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["ctaBlock"] ?? null), "url_anchor", [], "any", false, false, true, 17), 17, $this->source), "html", null, true);
                echo "
                    <i class=\"fa fa-arrow-left svg right\"></i>
                    <!-- <img src=\"";
                // line 19
                echo $this->extensions['Cms\Twig\Extension']->themeFilter("assets/images/btn-right-arrow.svg");
                echo "\" alt=\"\" class=\"svg right\"> -->
                </a>
                ";
            }
            // line 22
            echo "            </div>
        </div>
    </div>
</section>
";
        }
    }

    public function getTemplateName()
    {
        return "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/cta.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 22,  84 => 19,  79 => 17,  75 => 16,  69 => 14,  67 => 13,  63 => 11,  57 => 9,  55 => 8,  51 => 7,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% set ctaBlock = CtaBlock.data %}
{% if ctaBlock.title | length %}
<section class=\"pt-120 pb-120 bg-red\" data-bg-img=\"{{'assets/images/cta-bg.png'|theme}}\">
    <div class=\"container\">
        <div class=\"row align-items-center\">
            <div class=\"col-lg-7 col-md-8 offset-lg-1\">
                <h2>{{ctaBlock.title}}</h2>
                {% if ctaBlock.subtitle %}
                <p>{{ctaBlock.subtitle}}</p>
                {% endif %}
            </div>
            <div class=\"col-lg-3 col-md-4 offset-lg-1\">
                {% if ctaBlock.url and ctaBlock.url_anchor %}
                <a href=\"{{ ctaBlock.url }}\" class=\"btn bizagn--base-btn white-btn\">
                    <i class=\"fa fa-arrow-right svg left\"></i>
                    <!-- <img src=\"{{'assets/images/btn-right-arrow.svg'|theme}}\" alt=\"\" class=\"svg left\"> -->
                    {{ ctaBlock.url_anchor }}
                    <i class=\"fa fa-arrow-left svg right\"></i>
                    <!-- <img src=\"{{'assets/images/btn-right-arrow.svg'|theme}}\" alt=\"\" class=\"svg right\"> -->
                </a>
                {% endif %}
            </div>
        </div>
    </div>
</section>
{% endif %}", "/www/wwwroot/aiosales.vothanhdanh.info/themes/live-commerce/partials/index/cta.htm", "");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 2);
        static $filters = array("length" => 2, "theme" => 3, "escape" => 7);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['length', 'theme', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
