<?php namespace Pcms\BlogtagsPlugin\Updates;

use October\Rain\Database\Updates\Migration;
use Schema;
use System\Classes\PluginManager;
use Illuminate\Support\Facades\DB;

class CreateTagsTable extends Migration
{

    private $dbType;

    public function up()
    {
        $this->dbType = DB::connection()->getPdo()->getAttribute(\PDO::ATTR_DRIVER_NAME);
        Schema::create('pcms_blogtags_tags', function($table)
        {
            $this->dbSpecificSetup($table);

            $table->increments('id');
            $table->string('name')->unique()->nullable();
            $table->string('slug')->unique()->nullable();
            $table->timestamps();
        });
        if(PluginManager::instance()->hasPlugin('RainLab.Blog'))
        {
            Schema::create('pcms_blogtags_post_tag', function($table)
            {
                $this->dbSpecificSetup($table);

                $table->integer('tag_id')->unsigned()->nullable()->default(null);
                $table->integer('post_id')->unsigned()->nullable()->default(null);
                $table->index(['tag_id', 'post_id']);
            });
        }
        if(PluginManager::instance()->hasPlugin('Pcms.Ecommerce')){
            Schema::create('pcms_blogtags_product_tag', function($table)
            {
                $this->dbSpecificSetup($table);

                $table->integer('tag_id')->unsigned()->nullable()->default(null);
                $table->integer('product_id')->unsigned()->nullable()->default(null);
                $table->index(['tag_id', 'product_id']);
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('pcms_blogtags_tags');
        if(PluginManager::instance()->hasPlugin('RainLab.Blog'))
        {
            Schema::dropIfExists('pcms_blogtags_post_tag');
        }
        if(PluginManager::instance()->hasPlugin('Pcms.Ecommerce'))
        {
            Schema::dropIfExists('pcms_blogtags_product_tag');
        }
    }


    /**
     * @param $table
     */
    private function dbSpecificSetup(&$table)
    {
        switch ($this->dbType) {
            case 'pgsql':
                break;
            case 'mysql':
                //@todo SET sql_mode='ANSI_QUOTES';
                $table->engine = 'InnoDB';
                break;
        }
    }


}
