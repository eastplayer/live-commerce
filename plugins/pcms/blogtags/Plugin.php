<?php namespace Pcms\BlogTags;

use Backend;
use Pcms\BlogTags\Models\Tag;
use Config;
use Event;
use System\Classes\PluginBase;
use RainLab\Blog\Controllers\Posts as PostsController;
use RainLab\Blog\Models\Post as PostModel;
use System\Classes\PluginManager;

/**
 * BlogTags Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * @var array   Require the RainLab.Blog plugin
     */
    public $require = ['RainLab.Blog'];

    /**
     * Returns information about this plugin
     *
     * @return  array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'pcms.blogtags::lang.plugin.name',
            'description' => 'pcms.blogtags::lang.plugin.description',
            'author'      => 'Pcms',
            'icon'        => 'icon-tags'
        ];
    }

    /**
     * Register components
     *
     * @return  array
     */
    public function registerComponents()
    {
        return [
            'Pcms\BlogTags\Components\BlogTags'      => 'blogTags',
            'Pcms\BlogTags\Components\BlogTagSearch' => 'blogTagSearch',
            'Pcms\BlogTags\Components\BlogRelated'   => 'blogRelated',
            'Pcms\BlogTags\Components\TagRelated'   => 'TagRelated'
        ];
    }

    public function boot()
    {
        // extend the blog navigation
        Event::listen('backend.menu.extendItems', function($manager) {
           $manager->addSideMenuItems('RainLab.Blog', 'blog', [
                'tags' => [
                    'label' => 'pcms.blogtags::lang.navigation.tags',
                    'icon'  => 'icon-tags',
                    'code'  => 'tags',
                    'owner' => 'RainLab.Blog',
                    'url'   => Backend::url('pcms/blogtags/tags')
                ]
            ]);
        });

        // extend the post model
        PostModel::extend(function($model) {
            $model->belongsToMany['tags'] = [
                'Pcms\BlogTags\Models\Tag',
                'table' => 'pcms_blogtags_post_tag',
                'order' => 'name'
            ];
        });

        // extend the post form
        PostsController::extendFormFields(function($form, $model, $context) {
            if (!$model instanceof PostModel) {
                return;
            }

            $form->addSecondaryTabFields([
                'tags' => [
                    'label' => 'pcms.blogtags::lang.form.label',
                    'mode'  => 'relation',
                    'tab'   => 'rainlab.blog::lang.post.tab_categories',
                    'type'  => 'taglist'
                ]
            ]);
        });
        if (PluginManager::instance()->hasPlugin('Pcms.Ecommerce')) {
            \Pcms\Ecommerce\Models\Product::extend(function($model) {
                $model->belongsToMany['tags'] = [
                    'Pcms\BlogTags\Models\Tag',
                    'table' => 'pcms_blogtags_product_tag',
                    'order' => 'name'
                ];
            });
            \Pcms\Ecommerce\Controllers\Products::extendFormFields(function($form, $model, $context) {
                if (!$model instanceof Product) {
                    return;
                }
                $form->addTabFields([
                    'tags' => [
                        'label' => 'pcms.blogtags::lang.form.label',
                        'mode'  => 'relation',
                        'tab'   => 'Categories & Lists',
                        'type'  => 'taglist'
                    ]
                ]);
            });
        }
    }
}
