<?php namespace Pcms\BlogTags\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use DB;
use RainLab\Blog\Models\Post;
use System\Classes\PluginManager;

class TagRelated extends ComponentBase
{
    public $posts = [];
    public $products = [];
    public $model = null;
    public function componentDetails()
    {
        return [
            'name'        => 'Related Posts & product',
            'description' => 'Provides related blog posts & product'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'             => 'rainlab.blog::lang.settings.post_slug',
                'description'       => 'rainlab.blog::lang.settings.post_slug_description',
                'default'           => '{{ :slug }}',
                'type'              => 'string'
            ],
            'results' => [
                'title'             => 'Results',
                'description'       => 'Number of related posts to display (zero displays all related posts).',
                'type'              => 'string',
                'default'           => '5',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'The results must be a number.',
                'showExternalParam' => false
            ],
            'orderBy' => [
                'title'             => 'Sort by',
                'description'       => 'The value used to sort related posts.',
                'type'              => 'dropdown',
                'options' => [
                    false           => 'Relevance (# of shared tags)',
                    'title'         => 'Title',
                    'published_at'  => 'Published at',
                    'updated_at'    => 'Updated at',
                ],
                'default'           => false,
                'showExternalParam' => false
            ],
            'direction' => [
                'title'             => 'Order',
                'description'       => 'The order to sort related posts in.',
                'type'              => 'dropdown',
                'options' => [
                    'asc'           => 'Ascending',
                    'desc'          => 'Descending',
                ],
                'default'           => 'desc',
                'showExternalParam' => false
            ],
            'page' => [
                'title'       => 'Post page',
                'description' => 'Page to show linked posts',
                'type'        => 'dropdown',
                'default'     => 'blog/post',
                'group'       => 'Links',
            ],
            'type' => [
                'title'       => 'Type',
                'description' => 'post/product',
                'default'     => 'post',
            ],
            'collection' => [
                'title'       => 'Type',
                'description' => 'post/product',
                'default'     => 'post',
            ],
        ];
    }

    public function getPostPageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    /**
     * Load post and start building query for related posts
     */
    public function onRun()
    {
        if($this->property('collection') == 'product' && PluginManager::instance()->hasPlugin('Pcms.Ecommerce')){
            $this->model = \Pcms\Ecommerce\Models\Product::where('is_published', true);
        } else {
            $this->model = Post::where('published', true);
        }
        if($this->property('type') == 'post') $this->getPosts();
        if($this->property('type') == 'product' && PluginManager::instance()->hasPlugin('Pcms.Ecommerce')) $this->getProducts();
    }
    public function getProducts(){
        $product = $this->model->where('slug', $this->property('slug'))
            ->with('tags')
            ->first();

        // Abort if there is no source, or it has no tags
        if (!$product || (!$tagIds = $product->tags->lists('id')))
            return;
        $query = \Pcms\Ecommerce\Models\Product::where('id', '<>', $product->id)
            ->whereHas('tags', function($tag) use ($tagIds) {
                $tag->whereIn('id', $tagIds);
            })
            ->with('tags');
        $subQuery = DB::raw('(
            select count(*)
            from `pcms_blogtags_product_tag`
            where `pcms_blogtags_product_tag`.`product_id` = `pcms_ecommerce_products`.`id`
            and `pcms_blogtags_product_tag`.`tag_id` in ('.implode(', ', $tagIds).')
        )');
        $key = $this->property('orderBy') ?: $subQuery;
        $query->orderBy($key, $this->property('direction'));

        if ($take = intval($this->property('results')))
            $query->take($take);

        $this->products = $query->get();
    }
    public function getPosts(){
        $post = $this->model->where('slug', $this->property('slug'))
            ->with('tags')
            ->first();
        // Abort if there is no source, or it has no tags
        if (!$post || (!$tagIds = $post->tags->lists('id')))
            return;

        // Start building our query for related posts
        $query = Post::isPublished()
            ->where('id', '<>', $post->id)
            ->whereHas('tags', function($tag) use ($tagIds) {
                $tag->whereIn('id', $tagIds);
            })
            ->with('tags');

        // Sort the related posts
        $subQuery = DB::raw('(
            select count(*)
            from `pcms_blogtags_post_tag`
            where `pcms_blogtags_post_tag`.`post_id` = `rainlab_blog_posts`.`id`
            and `pcms_blogtags_post_tag`.`tag_id` in ('.implode(', ', $tagIds).')
        )');
        $key = $this->property('orderBy') ?: $subQuery;
        $query->orderBy($key, $this->property('direction'));

        // Limit the number of results
        if ($take = intval($this->property('results')))
            $query->take($take);

        // Execute the query
        $posts = $query->get();

        /*
         * Add a "url" helper attribute for linking to each post
        */
        $posts->each(function($post)
        {
           $post->setUrl($this->property('page'), $this->controller);
        });

        $this->posts = $posts;
    }

}
