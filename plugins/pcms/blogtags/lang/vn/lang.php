<?php
return [
    'plugin' => [
        'name' => 'Blog Tags',
        'description' => 'Thêm tags vào blog.',
    ],
    'form' => [
        'label' => 'Tags',
        'name_invalid' => 'Tag chỉ bao gồm ký tự, chữ và số.',
        'name_required' => 'Thẻ tag là bắt buộc.',
        'name_unique' => 'Thẻ tag này đã được sử dụng.',
    ],
    'navigation' => [
        'tags' => 'Tags',
    ]
];
