(function (win, doc) {
  
  pcms = win.pcms || {}
  pcms.seo = {
    charCountHandler: function(target) {
      $target = $(target);
      let $helpBlock = $target.next(".help-block");
      if(!$helpBlock.length){
        $helpBlock = $target.parent().parent().children(".help-block");
      }
      let isTwig = /\{{2}.*\}{2}/.test($target.val());
      let count = isTwig ? 'unrechable' : $target.val().length;
      
      let min = $target.data('min');
      let max = $target.data('max');
      
      $helpBlock.html(`
        Số ký tự: <b>${count}</b>
        |
        Khoảng khuyến khích: ${min} - ${max}
      `)
      
      let $number = $helpBlock.find('b');

      if (count < max && count > min) {
        $number.css({color:'green'})
      } else if (isTwig) {
        $number.css({color: 'orange'})
      } else {
        $number.css({color: 'red'})
      }
    }
  } 
    
    
  var listeners = [], 
    doc = win.document, 
    MutationObserver = win.MutationObserver || win.WebKitMutationObserver,
    observer;
  
  function ready(selector, fn) {
    // Store the selector and callback to be monitored
    listeners.push({
          selector: selector,
          fn: fn
      });
      if (!observer) {
          // Watch for changes in the document
          observer = new MutationObserver(check);
          observer.observe(doc.documentElement, {
              childList: true,
              subtree: true
          });
        }
      // Check if the element is currently in the DOM
      check();
  }
  
  function check() {
    // Check the DOM for elements matching a stored selector
    for (var i = 0, len = listeners.length, listener, elements; i < len; i++) {
      listener = listeners[i];
        // Query for elements matching the specified selector
        elements = doc.querySelectorAll(listener.selector);
        for (var j = 0, jLen = elements.length, element; j < jLen; j++) {
            element = elements[j];
            // Make sure the callback isn't invoked with the 
            // same element more than once
            if (!element.ready) {
                element.ready = true;
                // Invoke the callback with the element
                listener.fn.call(element, element);
            }
        }
    }
  }

  // Expose stuff
  win.ready = ready;
  win.pcms = pcms;
      
  // execute listeners
  win.ready('[data-counter]', (el) => {
    pcms.seo.charCountHandler(el)
    el.oninput = event => pcms.seo.charCountHandler(el);
  });
  win.ready('.meta_title_title', (el) => {
    $(el).find(".field-multilingual .ml-btn").css({'color': '#7b7b7b'});
    $(el).find(".help-block").css({'color': '#ffffff'});
    $(el).removeClass('span-full');
    $(el).addClass('span-left');
    $(el).find('input[type="text"]').attr('type', '');
  });
  win.ready('.meta_decription_textarae', (el) => {
    $(el).find(".field-multilingual .ml-btn").css({'color': '#7b7b7b'});
    $(el).find(".help-block").css({'color': '#ffffff'});
    $(el).css({'margin-bottom': '16px'});
    $(el).removeClass('span-full');
    $(el).addClass('span-right');
  });
})(window, document);
    