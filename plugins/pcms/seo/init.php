<?php

use System\Classes\PluginManager;

// Extend the frontend controller to minify HTML with the Minify middleware.
\Cms\Classes\CmsController::extend(function($controller) {
    $controller->middleware('Pcms\Seo\Middleware\MinifyHtml');
});

// add js dependencies in the backend
\Event::listen('backend.page.beforeDisplay', function($controller, $action, $params) {
    $controller->addJs('/plugins/pcms/seo/assets/pcms.seo.js');
    $controller->addJs('https://cdn.jsdelivr.net/npm/vue');
});


Pcms\Seo\Models\Settings::extend(function($model) {
    $model->bindEvent('model.afterSave', function() use ($model) {
        $htaccess = $model->value["htaccess"];
        File::put(base_path(".htaccess"), $htaccess);
    });
});


// make our Post fields jsonable
if(PluginManager::instance()->hasPlugin('RainLab.Blog'))
{
    \RainLab\Blog\Models\Post::extend(function($model) {
        // $model->addJsonable('pcms_seo_options');
    });
}
