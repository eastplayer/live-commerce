<?php return [
    'plugin' => [
        'name' => 'SEO',
        'description' => 'Quản lý SEO website toàn diện',
    ],
    'schemas' => [
        'id' => 'ID',
        'name' => 'Tên',
        'created_at' => 'Tạo lúc',
        'updated_at' => 'Cập nhật lúc',
        'template' => 'Mãu',
    ],
    'permissions' => [
        'config' => [
            'label' => 'Quản lý cài đặt'
        ],
        'keywords' => [
            'label' => 'Quản lý từ khóa'
        ],
        'og' => [
            'label' => 'Quản lý Open Graph'
        ],
        'sitemap' => [
            'label' => 'Quản lý Sitemap'
        ],
        'meta' => [
            'label' => 'Quản lý Meta tags'
        ],
        'schema' => [
            'label' => 'Quản lý Schema'
        ]
    ],
    'settings' => [
        'label' => 'Cài đặt SEO',
        'description' => 'Quản lý các cài đặt SEO'
    ],
    'keyword' => [
        'menu_label' => 'Từ khóa',
        'list_title' => 'Quản lý từ khóa',
        'name' => 'Tên',
        'status' => 'Trạng thái',
        'status_in_use' => 'Sử dụng',
        'status_not_in_use' => 'Không sử dụng',
        'create_title' => 'Thêm từ khóa',
        'edit_label' => 'Sửa từ khóa',
        'preview_title' => 'Xem từ khóa'
    ],
    'clear_cache' => 'Xóa bộ nhớ cache',
    'settings' => [
        'label' => 'Cài đặt SEO',
        'description'=> 'Các cài đặt liên quan đến SEO trang web',
        'tabs' => [
            'meta' => [
                'label' => 'Meta',
                'enable_site_meta' => [
                    'label' => 'Cho phép sử dụng thẻ title và description'
                ],
                'enable_sitemap' => [
                    'label' => 'Cho phép tạo file sitemap.xml'
                ],
                'site_name' => [
                    'label' => 'Tên trang web'
                ],
                'site_name_position' => [
                    'label' => 'Vị trí tên trang web',
                    'prefix' => 'Phía trước',
                    'suffix' => 'Phía sau',
                    'nowhere' => 'Không hiện tên trang web',
                    'comment' => 'Chọn cách thể hiện thẻ title của trang web'
                ],
                'site_name_separator' => [
                    'label' => 'Dấu phần tách',
                    'comment' => 'Ký tự phân tách tiêu đề. VD: Tiêu đề | Tên trang web'
                ],
                'site_description' => [
                    'label' => 'Thẻ meta description mặc định',
                    'placeholder' => 'Thẻ meta description sẽ được sử dụng khi meta description trang truy cập không định nghĩa'
                ],
                'site_keywords' => [
                    'label' => 'Thẻ keywords chính',
                    'comment' => 'Định nghĩa các từ khóa chính của trang web'
                ]
            ],
            'head' => [
                'label' => '<head>',
                'extra_meta' => [
                    'label' => 'Thêm các thẻ meta khác vào thẻ <head>'
                ]
            ],
            'robots' => [
                'label' => 'Robots',
                'enable_robots_txt' => [
                    'label' => 'Cho phép tạo robots.txt'
                ],
                'enable_robots_meta' => [
                    'label' => 'Cho phép hiện thẻ meta robots'
                ],
                'robots_txt' => [
                    'label' => 'Nội dung trang robots.txt'
                ]
            ],
            'minify' => [
                'label' => 'Minify',
                'minify_html' => [
                    'label' => 'Tự động nén các file HTML',
                    'comment' => 'Nén file HTML phía front end và tự động xóa bộ nhớ Cache khi có thay đổi'
                ],
                'minify_css' => [
                    'label' => 'Tự động nén các file CSS',
                    'comment' => 'Sử dụng: {{ "path/to/css" | theme | minifycss }}'
                ],
                'minify_js' => [
                    'label' => 'Tự động nén các file JS',
                    'comment' => 'Sử dụng: {{ "path/to/js" | theme | minifyjs }}'
                ],
                'no_minify_for_dev' => [
                    'label' => 'Không nén khi ở chế độ ENV=dev',
                    'comment' => 'Thay đổi biến ENV trong file .env'
                ]
            ],
            'favicon' => [
                'label' => 'Favicon',
                'favicon_enabled' => [
                    'label' => 'Cho phép tạo favicon.ico',
                    'comment' => 'Tự động tạo điều hường /favicon.ico'
                ],
                'favicon_16' => [
                    'label' => 'Tự đồng cắt file Favicon thành 16x16',
                ],
                'favicon' => [
                    'label' => 'Chọn ảnh Favicon',
                    'prompt' => 'Click %s để tìm hình ảnh trong thư viện'
                ]
            ],
            'htaccess' => [
                'label' => '.htaccess',
                'htaccess' => [
                    'label' => 'Sửa file .htaccess',
                    'comment' => '<b class="text-danger">Chú ý:</b> Hãy cẩn thận vì nó ảnh hưởng trực tiếp đến dữ liệu của bạn.'
                ]
            ],
            'open_graph' => [
                'label' => 'Open Graph',
                'enable_og' => [
                    'label' => 'Cho phép sử dụng Open Graph',
                    'comment' => 'Thêm thẻ meta og vào thẻ <head>'
                ],
                'site_image' => [
                    'label' => 'Hình ảnh trang web sử dụng cho Open Graph',
                    'prompt' => 'Kéo thả hoặc ấn vào đây'
                ],
                'fb_app_id' => [
                    'label' => 'Facebook App ID',
                    'comment' => 'Thẻ meta og fb:app_id'
                ],
                'og_locale' => [
                    'label' => 'Thẻ og:locale',
                    'comment' => 'Cho biết ngôn ngữ trang web đối với mạng xã hội. <a target="__blank" href="http://ogp.me/#optional">Xem thêm.</a>'
                ]
            ]
        ]
    ],
    'seo' => [
        'tabs' => [
            'sitemap' => 'Sitemap',
            'schemas' => 'Schemas',
            'open_graph' => 'Open Graph'
        ],
        'meta_title' => [
            'label' => 'Thẻ title',
            'comment' => 'Thẻ title',
            'placeholder' => 'Thẻ title. Sử dụng tiêu đề nếu bỏ trống'
        ],
        'meta_description' => [
            'label' => 'Thẻ description',
            'comment' => 'Thẻ description',
            'placeholder' => 'Thẻ description. Lấy description mặc định nếu bỏ trống'
        ],
        'meta_keywords' => [
            'label' => 'Thẻ keywords',
            'comment' => 'Thẻ keywords',
            'placeholder' => 'Thẻ keywords. Bao gồm cả các từ khóa chính'
        ],
        'canonical_url' => [
            'label' => 'Thẻ đường dẫn',
            'comment' => 'Thẻ chỉ định đường dẫn chính',
            'placeholder' => 'Thẻ đường dẫn'
        ],
        'robot_index' => [
            'label' => 'Robot đánh chỉ số',
            'comment' => 'Cho công cụ tim kiếm biết đánh chỉ số trang này',
            'index' => 'index',
            'noindex' => 'noindex'
        ],
        'robot_follow' => [
            'label' => 'Robot truy vấn liên kết',
            'comment' => 'Cho công cụ tim kiếm biết truy vấn liên kết trang này',
            'follow' => 'follow',
            'nofollow' => 'nofollow'
        ],
        'robot_advanced' => [
            'label' => 'Thẻ robot khác',
            'comment' => 'Thêm các chỉ thị robot khác cho trang này, cách nhau bằng dấy phẩy',
            'placeholder' => 'VD: all, noarchive, nosnippet,...'
        ],
        'enabled_in_sitemap' => [
            'label' => 'Cho phép hiện trong sitemap.xml',
            'comment' => 'Cho phép hiện trang này trong sitemap.xml'
        ],
        'model_class' => [
            'label' => 'Liên kêt Model',
            'comment' => 'Cho biết trang này được cấu thành bởi một Model',
            'placeholder' => 'VD: Author\Plugin\Models\ModelClass'
        ],
        'use_updated_at' => [
            'label' => 'Sử dụng trường "updated_at" từ Model như là thời gian sửa lần cuối',
            'comment' => 'Nếu trường "updated_at" không tồn tại, sẽ lấy thời gian sửa file làm thời gian sửa lần cuối <b>Page::$mtime</b>',
        ],
        'lastmod' => [
            'label' => 'Sửa lần cuối lúc',
            'comment' => 'Cho công cụ tìm kiếm biết ngày và giờ trang này được sửa',
        ],
        'changefreq' => [
            'label' => 'Tần xuất thay đổi',
            'comment' => 'Cho công cụ tìm kiếm biết bao lâu thì nội dung trang này được thay đổi một lần',
            'always' => 'Luôn luôn',
            'hourly' => 'Hàng giờ',
            'weekly' => 'Hàng tuần',
            'monthly' => 'Hàng tháng',
            'yearly' => 'Hàng năm',
            'never' => 'Không bao giờ'
        ],
        'priority' => [
            'label' => 'Mức độ quan trọng',
            'comment' => 'Cho công cụ tìm kiếm biết tầm quan trọng của trang',
        ],
        'schemas' => [
            'label' => 'Viết nội dung schemas thêm cho trang này'
        ],
        'og_title' => [
            'label' => 'Thẻ og:title',
            'comment' => 'Khuyến khích 50-60 ký tự',
            'placeholder' => 'Sử dụng nội dung meta title nếu để trống'
        ],
        'og_description' => [
            'label' => 'Thẻ og:description',
            'comment' => 'Độ dài tốt nhất là 155',
            'placeholder' => 'Sử dụng nội dung meta description nếu để trống'
        ],
        'og_type' => [
            'label' => 'Thẻ og:type',
            'comment' => 'Cho biết kiểu nội dung trang',
            'placeholder' => 'VD: website, article, video, product,...'
        ],
        'og_image' => [
            'label' => 'Thẻ og:image',
            'comment' => '',
        ],
        'og_ref_image' => [
            'label' => 'Thẻ og:image với ảnh không cố định',
            'comment' => 'Sẽ đè lên thẻ og:image',
            'placeholder' => '{{ example.image }}'
        ]
    ]
];