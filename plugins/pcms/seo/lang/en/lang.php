<?php return [
    'plugin' => [
        'name' => 'SEO',
        'description' => 'Quản lý SEO website toàn diện',
    ],
    'schemas' => [
        'id' => 'ID',
        'name' => 'Tên',
        'created_at' => 'Tạo lúc',
        'updated_at' => 'Cập nhật lúc',
        'template' => 'Mãu',
    ],
    'permissions' => [
        'config' => [
            'label' => 'Quản lý cài đặt'
        ],
        'keywords' => [
            'label' => 'Quản lý từ khóa'
        ],
        'og' => [
            'label' => 'Quản lý Open Graph'
        ],
        'sitemap' => [
            'label' => 'Quản lý Sitemap'
        ],
        'meta' => [
            'label' => 'Quản lý Meta tags'
        ],
        'schema' => [
            'label' => 'Quản lý Schema'
        ]
    ],
    'settings' => [
        'label' => 'Cài đặt SEO',
        'description' => 'Quản lý các cài đặt SEO'
    ],
    'keyword' => [
        'menu_label' => 'Từ khóa',
        'list_title' => 'Quản lý từ khóa',
        'name' => 'Tên',
        'status' => 'Trạng thái',
        'status_in_use' => 'Sử dụng',
        'status_not_in_use' => 'Không sử dụng',
        'create_title' => 'Thêm từ khóa',
        'edit_label' => 'Sửa từ khóa',
        'preview_title' => 'Xem từ khóa'
    ],
    'clear_cache' => 'Xóa bộ nhớ cache',
    'settings' => [
        'label' => 'Cài đặt SEO',
        'description'=> 'Các cài đặt liên quan đến SEO trang web',
        'tabs' => [
            'meta' => [
                'label' => 'Meta',
                'enable_site_meta' => [
                    'label' => 'Cho phép sử dụng thẻ title và description'
                ],
                'enable_sitemap' => [
                    'label' => 'Cho phép tạo file sitemap.xml'
                ],
                'site_name' => [
                    'label' => 'Tên trang web'
                ],
                'site_name_position' => [
                    'label' => 'Vị trí tên treang web',
                    'prefix' => 'Phía trước',
                    'suffix' => 'Phía sau',
                    'nowhere' => 'Không hiện tên trang web',
                    'comment' => 'Chọn cách thể hiện thẻ title của trang web'
                ],
                'site_name_separator' => [
                    'label' => 'Dấu phần tách',
                    'comment' => 'Ký tự phân tách tiêu đề. VD: Tiêu đề | Tên trang web'
                ],
                'site_description' => [
                    'label' => 'Thẻ meta description mặc định',
                    'placeholder' => 'Thẻ meta description sẽ được sử dụng khi meta description trang truy cập không định nghĩa'
                ],
                'site_keywords' => [
                    'label' => 'Thẻ keywords chính',
                    'comment' => 'Định nghĩa các từ khóa chính của trang web'
                ]
            ],
            'head' => [
                'label' => '<head>',
                'extra_meta' => [
                    'label' => 'Thêm các thẻ meta khác vào thẻ <head>'
                ]
            ],
            'robots' => [
                'label' => 'Robots',
                'enable_robots_txt' => [
                    'label' => 'Cho phép tạo robots.txt'
                ],
                'enable_robots_meta' => [
                    'label' => 'Cho phép hiện thẻ meta robots'
                ],
                'robots_txt' => [
                    'label' => 'Nội dung trang robots.txt'
                ]
            ],
            'minify' => [
                'label' => 'Minify',
                'minify_html' => [
                    'label' => 'Tự động nén các file HTML',
                    'comment' => 'Nén file HTML phía front end và tự động xóa bộ nhớ Cache khi có thay đổi'
                ],
                'minify_css' => [
                    'label' => 'Tự động nén các file CSS',
                    'comment' => 'Sử dụng: {{ "path/to/css" | theme | minifycss }}'
                ],
                'minify_js' => [
                    'label' => 'Tự động nén các file JS',
                    'comment' => 'Sử dụng: {{ "path/to/js" | theme | minifyjs }}'
                ],
                'no_minify_for_dev' => [
                    'label' => 'Không nén khi ở chế độ ENV=dev',
                    'comment' => 'Thay đổi biến ENV trong file .env'
                ]
            ],
            'favicon' => [
                'label' => 'Favicon',
                'favicon_enabled' => [
                    'label' => 'Cho phép tạo favicon.ico',
                    'comment' => 'Tự động tạo điều hường /favicon.ico'
                ],
                'favicon_16' => [
                    'label' => 'Tự đồng cắt file Favicon thành 16x16',
                ],
                'favicon' => [
                    'label' => 'Chọn ảnh Favicon',
                    'prompt' => 'Click %s để tìm hình ảnh trong thư viện'
                ]
            ],
            'htaccess' => [
                'label' => '.htaccess',
                'htaccess' => [
                    'label' => 'Sửa file .htaccess',
                    'comment' => '<b class="text-danger">Chú ý:</b> Hãy cẩn thận vì nó ảnh hưởng trực tiếp đến dữ liệu của bạn.'
                ]
            ],
            'open_graph' => [
                'label' => 'Open Graph',
                'enable_og' => [
                    'label' => 'Cho phép sử dụng Open Graph',
                    'comment' => 'Thêm thẻ meta og vào thẻ <head>'
                ],
                'site_image' => [
                    'label' => 'Hình ảnh trang web sử dụng cho Open Graph',
                    'prompt' => 'Kéo thả hoặc ấn vào đây'
                ],
                'fb_app_id' => [
                    'label' => 'Facebook App ID',
                    'comment' => 'Thẻ meta og fb:app_id'
                ],
                'og_locale' => [
                    'label' => 'Thẻ og:locale',
                    'comment' => 'Cho biết ngôn ngữ trang web đối với mạng xã hội. <a target="__blank" href="http://ogp.me/#optional">Xem thêm.</a>'
                ]
            ]
        ]
    ]
];