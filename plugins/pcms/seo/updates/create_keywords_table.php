<?php namespace Pcms\Reviews\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateKeywordTable extends Migration
{
    public function up()
    {
        Schema::create('pcms_seo_keywords', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 300)->nullable();
            $table->boolean('in_use')->default(true);
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pcms_seo_keywords');
    }
}
