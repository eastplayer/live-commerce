<?php namespace Pcms\Seo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;
use System\Classes\PluginManager;

class ExtendRainlabBlogPostsTable extends Migration
{

    public function up()
    {
        if(PluginManager::instance()->hasPlugin('RainLab.Blog'))
        {
            Schema::table('rainlab_blog_posts', function($table)
            {
                $table->text('meta_title')->nullable();
                $table->text('meta_description')->nullable();
                $table->text('meta_keywords')->nullable();
                $table->text('canonical_url')->nullable();
                $table->text('robot_index')->nullable();
                $table->text('robot_follow')->nullable();
                $table->text('robot_advanced')->nullable();
                $table->text('enabled_in_sitemap')->nullable();
                $table->text('schemas')->nullable();
                $table->text('og_title')->nullable();
                $table->text('og_description')->nullable();
                $table->text('og_type')->nullable();
                $table->text('og_image')->nullable();
                $table->text('og_ref_image')->nullable();
            });
        }
    }

    public function down()
    {
        if(PluginManager::instance()->hasPlugin('RainLab.Blog'))
        {
            Schema::table('rainlab_blog_posts', function($table)
            {
                $table->dropColumn('meta_title');
                $table->dropColumn('meta_description');
                $table->dropColumn('meta_keywords');
                $table->dropColumn('canonical_url');
                $table->dropColumn('robot_index');
                $table->dropColumn('robot_follow');
                $table->dropColumn('robot_advanced');
                $table->dropColumn('enabled_in_sitemap');
                $table->dropColumn('schemas');
                $table->dropColumn('og_title');
                $table->dropColumn('og_description');
                $table->dropColumn('og_type');
                $table->dropColumn('og_image');
                $table->dropColumn('og_ref_image');
            });
        }

    }

}
