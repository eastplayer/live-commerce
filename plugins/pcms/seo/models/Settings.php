<?php

namespace Pcms\Seo\Models;

use Model;
use Lang;
use Pcms\Seo\Models\Keyword;

class Settings extends Model{

    public $implement = [
        'System.Behaviors.SettingsModel',
        '@RainLab.Translate.Behaviors.TranslatableModel'
    ];

    // A unique code
    public $settingsCode = 'pcms_seo_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';

    protected $cache = [];

    public $translatable = ['site_name', 'site_description', 'og_locale'];

    public function getPageOptions() {
        return \Cms\Classes\Page::getNameList();
    }

    public function getSiteKeywordsOptions() {
        $locale = Lang::getLocale();
        $keywords = Keyword::where('in_use', true)->get();
        $keywordsArray = [];
        foreach($keywords as $keyword){
            $keywordsArray[$keyword->id] = $keyword->getAttributeTranslated('name', $locale);
        }
        return $keywordsArray;
    }

    public function initSettingsData() {
        $this->htaccess = \File::get(base_path(".htaccess")) ;
    }
}