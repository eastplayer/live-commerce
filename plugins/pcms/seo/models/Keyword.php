<?php

namespace Pcms\Seo\Models;

use App;
use Carbon\Carbon;
use Config;
use Model;
use October\Rain\Database\Traits\SoftDelete as SoftDeleteTrait;
use October\Rain\Database\Traits\Sortable as SortableTrait;
use October\Rain\Database\Traits\Validation as ValidationTrait;
use Request;
use Str;
use Lang;

/**
 * Reviews class.
 *
 * @package Pcms\Reviews\Models
 */
class Keyword extends Model
{
    use SoftDeleteTrait;

    use ValidationTrait;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $table = 'pcms_seo_keywords';

    public $rules = [
        'name' => 'required|max:300',
        'in_use'   => 'boolean'
    ];

    public $translatable = ['name'];

    public $dates = ['created_at', 'updated_at'];

    public $belongsTo = [
        'user' => ['Backend\Models\User']
    ];

    public function scopeInUse($query)
    {
        return $query->where('in_use', false);
    }
    
}
