<?php

namespace Pcms\Seo;

use System\Classes\PluginBase;
use Cms\Classes\Page;
use System\Models\File;
use Cms\Classes\Theme;
use System\Classes\SettingsManager;

use System\Classes\PluginManager;
use Pcms\Seo\Classes\Helper;
use Cms\Classes\Controller;
use Pcms\Seo\Models\Keyword;
use RainLab\Blog\Models\Post;

use Lang;
use Backend;
use Pcms\Seo\Models\Settings;
use Event;

/**
 * Pcms Plugin Information File
 */
class Plugin extends PluginBase
{
    public $require = [
        'Pcms.Extensions',
    ];

    public function registerNavigation()
    {
        return [
            'seo' => [
                'label' => Lang::get('pcms.seo::lang.plugin.name'),
                'url' => Backend::url('pcms/seo/keywords'),
                'icon' => 'icon-search',
                'permissions' => ['pcms.seo.*'],
                'sideMenu' => [
                    'keywords' => [
                        'label' => Lang::get('pcms.seo::lang.keyword.menu_label'),
                        'permissions' => ['pcms.seo.keywords'],
                        'url' => Backend::url('pcms/seo/keywords'),
                        'icon' => 'icon-search-plus',
                        'order' => 100,
                    ],
                ],
            ]
        ];
    }

    public function registerComponents()
    {
        return [
            'Pcms\Seo\Components\Seo' => 'seo',
            'Pcms\Seo\Components\SchemaVideo' => 'schemaVideo',
            'Pcms\Seo\Components\SchemaArticle' => 'schemaArticle',
            'Pcms\Seo\Components\SchemaProduct' => 'schemaProduct',
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'pcms.seo::lang.settings.label',
                'description' => 'pcms.seo::lang.settings.description',
                'icon'        => 'icon-search',
                'category'    =>  SettingsManager::CATEGORY_CMS,
                'class'       => 'Pcms\Seo\Models\Settings',
                'order'       => 100,
                'permissions' => ['pcms.seo.config']
            ]
        ];
    }

    public function registerMarkupTags()
    {
        $helper = new Helper();
        $minifier = \Pcms\Seo\Classes\Minifier::class;
        $schema = \Pcms\Seo\Classes\Schema::class;
        return [
            'filters' => [
                'minifyjs' => [$minifier, 'minifyJs'],
                'minifycss' => [$minifier, 'minifyCss'],
                'pcms_seo_schema' => [$schema, 'toScript'],
                'seotitle'    => [$helper, 'generateTitle'],
                'removenulls' => [$helper, 'removeNullsFromArray'],
                'fillparams'  => ['Pcms\Seo\Classes\Helper', 'replaceUrlPlaceholders'],
                'url' => [$helper, 'url'],
            ]
        ];
    }

    public function registerPageSnippets()
    {
        return [
            '\Pcms\Seo\Components\SchemaVideo' => 'schemaVideo',
            '\Pcms\Seo\Components\SchemaArticle' => 'schemaArticle',
            '\Pcms\Seo\Components\SchemaProduct' => 'schemaProduct',
        ];
    }

    public function registerFormWidgets()
    {
        return [];
    }

    public function boot()
    {
        Event::listen('backend.form.extendFieldsBefore', function($widget) {
            if (!($widget->getController() instanceof \RainLab\Blog\Controllers\Posts
            && $widget->model instanceof \RainLab\Blog\Models\Post)){
                return;
            }
            $widget->secondaryTabs['fields']['content']['type'] = 'richeditor';
        }); 
    }


    public function register()
    {
        \Event::listen('backend.form.extendFields', function ($widget) {
            if ($widget->isNested === false) {

                if (!($theme = Theme::getEditTheme()))
                    throw new ApplicationException(Lang::get('cms::lang.theme.edit.not_found'));

                if (
                    PluginManager::instance()->hasPlugin('RainLab.Pages')
                    && $widget->model instanceof \RainLab\Pages\Classes\Page
                ) {
                    $settings = $this->staticSeoFields();
                    $widget->getField('viewBag[meta_title]')->comment('Page title')->attributes($settings['viewBag[meta_title]']['attributes']);
                    $widget->getField('viewBag[meta_description]')->comment('Page description')->attributes($settings['viewBag[meta_description]']['attributes']);
                    // $widget->removeField('viewBag[meta_title]');
                    // $widget->removeField('viewBag[meta_description]');
                    unset($settings['viewBag[meta_title]']);
                    unset($settings['viewBag[meta_description]']);
                    $widget->addFields(array_except($settings, [
                        'viewBag[model_class]',
                        'viewBag[fill_params]',
                    ]), 'primary');
                }

                if (
                    PluginManager::instance()->hasPlugin('RainLab.Blog')
                    && $widget->model instanceof \RainLab\Blog\Models\Post
                ) {
                    $widget->addFields(array_except($this->blogSeoFields(), [
                        'model_class',
                        'fill_params',
                        'lastmod',
                        'use_updated_at',
                        'changefreq',
                        'priority',
                        'meta_title',
                        'meta_description'
                    ]), 'secondary');
                }

                if (!$widget->model instanceof \Cms\Classes\Page) return;
                $settings = $this->cmsSeoFields();
                $widget->getField('settings[meta_title]')->comment('Page title')->attributes($settings['settings[meta_title]']['attributes']);
                $widget->getField('settings[meta_description]')->comment('Page description')->attributes($settings['settings[meta_description]']['attributes']);
                // $widget->removeField('settings[meta_title]');
                // $widget->removeField('settings[meta_description]');
                unset($settings['settings[meta_title]']);
                unset($settings['settings[meta_description]']);
                $widget->addFields($settings, 'primary');

            }

        });
        \Event::listen('backend.form.extendFieldsBefore', function($widget) {
            if (
                PluginManager::instance()->hasPlugin('RainLab.Blog')
                && $widget->model instanceof \RainLab\Blog\Models\Post
            ) {
                $fields = $this->blogSeoFields();
                if(isset($fields['meta_title'])) $widget->fields['meta_title'] = $fields['meta_title'];
                if(isset($fields['meta_description'])) $widget->fields['meta_description'] = $fields['meta_description'];
            }
        });
        Post::extend(function($post) {
            $post->translatable[] = 'meta_title';
            $post->translatable[] = 'meta_description';
        });
        
    }

    private function getAvailableKeywords(){
        $locale = Lang::getLocale();
        $keywords = Keyword::where('in_use', true)->get();
        $keywordsArray = [];
        foreach($keywords as $keyword){
            $keywordsArray[$keyword->id] = $keyword->getAttributeTranslated('name', $locale);
        }
        return $keywordsArray;
    }

    private function blogSeoFields()
    {
        $settings = collect($this->seoFields())->mapWithKeys(function ($item, $key) {
            return ["$key" => $item];
        })->toArray();
        $settings['meta_keywords']['options'] = $this->getAvailableKeywords();
        return $settings;
    }
    private function staticSeoFields()
    {
        $settings =collect($this->seoFields())->mapWithKeys(function ($item, $key) {
            return ["viewBag[$key]" => $item];
        })->toArray();
        $settings['viewBag[meta_keywords]']['options'] = $this->getAvailableKeywords();
        return $settings;
    }
    private function cmsSeoFields()
    {
        $settings = collect($this->seoFields())->mapWithKeys(function ($item, $key) {
            return ["settings[$key]" => $item];
        })->toArray();
        $settings['settings[meta_keywords]']['options'] = $this->getAvailableKeywords();
        return $settings;
    }
    private function seoFields()
    {
        $user = \BackendAuth::getUser();
        // remove form fields when current users doesn't have access
        return array_except(
            \Yaml::parseFile(plugins_path('pcms/seo/config/seofields.yaml')),
            array_merge(
                [],
                !$user->hasPermission(["pcms.seo.og"]) ? [
                    "og_title",
                    "og_description",
                    "og_image",
                    "og_type",
                    "og_ref_image"
                ] : [],
                !$user->hasPermission(["pcms.seo.sitemap"]) ? [
                    "enabled_in_sitemap",
                    "model_class",
                    "fill_params",
                    "use_updated_at",
                    "lastmod",
                    "changefreq",
                    "priority",
                ] : [],
                !$user->hasPermission(["pcms.seo.meta"]) ? [
                    "meta_title",
                    "meta_description",
                    "canonical_url",
                    "robot_index",
                    "robot_follow",
                    "robot_advanced",
                ] : [],
                !$user->hasPermission(["pcms.seo.schema"]) ? [
                    "schemas"
                ] : []
            )
        );
    }
    public function registerListColumnTypes()
    {
        return [
            'in_use_status' => function($value) { return '<div class="text-center"><span class="'. ($value==1 ? 'oc-icon-circle text-success' : 'text-muted oc-icon-circle text-draft') .'">' . ($value==1 ? e(trans('pcms.seo::lang.keyword.status_in_use')) : e(trans('pcms.seo::lang.keyword.status_not_in_use')) ) . '</span></div>'; }
        ];
    }
}
