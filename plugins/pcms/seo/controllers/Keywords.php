<?php

namespace Pcms\Seo\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Pcms\Seo\Models\Keyword as Kw;
use Flash;

class Keywords extends Controller
{
    public $implement = [
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Pcms.Seo', 'seo', 'keywords');
    }

    public function details($id)
    {
        $keyword = Kw::findOrFail($id);
        return $this->makePartial('details', [
            'keyword' => $$keyword
        ]);
    }

    public function formBeforeCreate($model)
    {
        $model->user = $this->user->id;
    }
}
