<?php namespace Pcms\Seo\Components;

use Cms\Classes\ComponentBase;

use Lang;
use Request;
use Redirect;
use Pcms\Seo\Models\Settings;
use Pcms\Seo\Models\Keyword;
use Pcms\Seo\Classes\Helper;
use Cms\Classes\Theme;
use Cms\Classes\Page;
use RainLab\Translate\Models\Locale as LocaleModel;
use Cms\Components\ViewBag;

class Seo extends ComponentBase
{
    public $settings;
    public $disable_schema;

    // setup the viewBag for the component
    public function onRender()
    {
        $this->settings = Settings::instance();
        $locale = Lang::getLocale();
        if (! $this->page['viewBag']) $this->page['viewBag'] = new ViewBag;
        if($this->page->page->hasComponent('blogPost'))
        {
            $post = $this->page['post'];
            if(isset($post->attributes)){
                $this->page['viewBag']->setProperties(array_merge(
                    $this->page["viewBag"]->getProperties(), 
                    $post->attributes
                ));
                $this->page['viewBag']->setProperty('title', $post->getAttributeTranslated('title', $locale));
                $this->page['viewBag']->setProperty('meta_title', $post->getAttributeTranslated('meta_title', $locale));
                $this->page['viewBag']->setProperty('meta_description', $post->getAttributeTranslated('meta_description', $locale));
                $this->page['viewBag']->setProperty('meta_image', sizeof($post->featured_images)?$post->featured_images[0]->getThumb(500, 500, 'crop'):'');
            }else{
                return Redirect::to('/');
            }
        } else if($this->page->page->hasComponent('blogPosts') && $this->page['category'] && (!isset($this->page->page->{$this->page->page->hasComponent('blogPosts')}['ignoreSeo'])||!$this->page->page->{$this->page->page->hasComponent('blogPosts')}['ignoreSeo']))
        {
            $category = $this->page['category'];
            $this->page['viewBag']->setProperties(array_merge(
                $this->page["viewBag"]->getProperties(), 
                [
                    'meta_title' => $category->getAttributeTranslated('name', $locale),
                    'meta_description' => $category->getAttributeTranslated('description', $locale)
                ]
            ));
        } 
        else if (isset($this->page->apiBag['staticPage'])) { // static page
            $this->page['viewBag']->setProperties(array_merge($this->page['viewBag']->getProperties(), $this->page->controller->vars['page']->viewBag));
            // $this->page['viewBag'] =  $this->page->controller->vars['page']->viewBag;
        } else { // cms page
            $this->page['viewBag']->setProperties( array_merge($this->page['viewBag']->getProperties(), $this->page->settings)) ;
        }
        $this->disable_schema = $this->property('disable_schema');

        $this->page['keywords'] = [];
        $keywordIds = [];
        if($this->settings->site_keywords){
            $keywordIds = $this->settings->site_keywords;
        }
        if($this->page->meta_keywords){
            if(is_array($this->page->meta_keywords)){
                $keywordIds = array_merge($keywordIds, $this->page->meta_keywords);
            } else {
                $keywordIds = array_merge($keywordIds, explode(",", $this->page->meta_keywords));
            }
        }
        if($this->page['viewBag']->meta_keywords){
            $keywordIds = array_merge($keywordIds, explode(",", is_array($this->page['viewBag'])?isset($this->page['viewBag']['meta_keywords']):$this->page['viewBag']->meta_keywords));
        }
        if(sizeof($keywordIds)>0){
            $kwModels = Keyword::whereIn('id', $keywordIds)->where('in_use', true)->get();
            $keywords = [];
            foreach($kwModels as $kw){
                array_push($keywords, $kw->getAttributeTranslated('name', $locale));
            }
            $this->page['keywords'] = $keywords;
        }
        $helper = new Helper();
        $this->page['site_name'] = $helper->generateTitle($this->page['viewBag']->meta_title?$this->page['viewBag']->meta_title:$this->page['viewBag']->title);
        $locales = LocaleModel::listEnabled();
        $listLocale = [];
        foreach($locales as $key=>$value){
            if($key != $locale){
                array_push($listLocale, $this->settings->getAttributeTranslated('og_locale', $key));
            }
        }
        $this->page['meta_locales'] = $listLocale;
        if($this->page->meta_image){
            $this->page['viewBag']->setProperty('meta_image', $this->page->meta_image);
        }
    }

    public function componentDetails()
    {
        return [
            'name'        => 'SEO',
            'description' => 'Renders SEO meta tags in place'
        ];
    }

    public function defineProperties()
    {
        return [
            'disable_schema' => [
                'title' => 'Disable schemas',
                'description' => 'Enable this if you do not want to output schema scripts from the seo component.',
                'type' => 'checkbox'
            ]
        ];
    }
}
