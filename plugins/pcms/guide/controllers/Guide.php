<?php namespace Pcms\Guide\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Pcms\Guide\Models\Settings;

class Guide extends Controller
{
    public $requiredPermissions = [
        'pcms.guide.view' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Pcms.Guide', 'guide', 'guide');
    }
    public function index()
    {
        $this->pageTitle = "Hướng dẫn sử dụng";
        $settings = Settings::instance();
        $this->vars['settings'] = $settings;
    }
}
