<?php namespace Pcms\Guide;

use System\Classes\PluginBase;
use Backend\Facades\Backend;
use System\Classes\SettingsManager;

class Plugin extends PluginBase
{
    
    public function pluginDetails()
    {
        return [
            'name' => 'Guide',
            'description' => 'Guide',
            'author' => 'Pcms',
            'icon' => 'icon-question'
        ];
    }

    public function registerPermissions()
    {
        return [
            'pcms.guide.settings' => [
                'label' => 'Guide setting',
            ],
            'pcms.guide.view' => [
                'label' => 'Guide view',
            ],
        ];
    }

    public function registerNavigation()
    {
        return [
            'guide' => [
                'label' => 'HDSD',
                'url' => Backend::url('pcms/guide/guide'),
                'icon' => 'icon-question',
                'permissions' => ['pcms.guide.view'],
                'order' => 0,
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'HDSD',
                'description' => 'Cài đặt HDSD',
                'icon'        => 'icon-search',
                'category'    =>  SettingsManager::CATEGORY_CMS,
                'class'       => 'Pcms\Guide\Models\Settings',
                'order'       => 100,
                'permissions' => ['pcms.guide.settings']
            ]
        ];
    }
}
