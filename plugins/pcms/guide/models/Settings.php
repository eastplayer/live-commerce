<?php

namespace Pcms\Guide\Models;

use Model;

class Settings extends Model{

    public $implement = [
        'System.Behaviors.SettingsModel'
    ];
    public $settingsCode = 'pcms_guide_settings';
    public $settingsFields = 'fields.yaml';
}