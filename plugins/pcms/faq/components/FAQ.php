<?php

namespace Pcms\FAQ\Components;

use Pcms\FAQ\Classes\JsonLD;
use Pcms\FAQ\Models\FAQItem;
use Cms\Classes\ComponentBase;
use Cms\Classes\Controller;
use Psy\Util\Json;

class FAQ extends ComponentBase
{
    public $faq;

    public function componentDetails()
    {
        return [
            'name' => 'pcms.faq::lang.components.faq.name',
            'description' => 'pcms.faq::lang.components.faq.description'
        ];
    }

    public function defineProperties()
    {
        return [
            'add_jsonld' => [
                'title' => 'pcms.faq::lang.components.faq.params.add_jsonld.name',
                'description' => 'pcms.faq::lang.components.faq.params.add_jsonld.description',
                'type' => 'checkbox',
                'default' => true
            ]
        ];
    }

    public function init()
    {
        $properties = $this->getProperties();
        if ($properties['add_jsonld'] == 1) {
            \Event::listen('cms.page.display', function (Controller $controller, $url, $page, $result) {
                $res = '<script type="application/ld+json">';
                $res .= Json::encode(JsonLD::generate());
                $res .= '</script>';

                return str_ireplace('</head>', $res . '</head>', $result);
            });
        }
    }

    public function onRun()
    {
        $this->faq = $this->page['faq'] = FAQItem::isActive()->get();
    }
}
