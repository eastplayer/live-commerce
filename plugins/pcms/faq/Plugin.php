<?php

namespace Pcms\FAQ;

use Pcms\FAQ\Models\FAQItem;
use Backend\Facades\Backend;
use System\Classes\PluginBase;
use System\Classes\PluginManager;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name' => 'pcms.faq::lang.plugin.name',
            'description' => 'pcms.faq::lang.plugin.description',
            'author' => 'pcms.faq::lang.plugin.author',
            'icon' => 'icon-question'
        ];
    }

    public function registerComponents()
    {
        return [
            'Pcms\FAQ\Components\FAQ' => 'faq',
        ];
    }

    public function registerPermissions()
    {
        return [
            'pcms.faq.edit' => [
                'label' => 'pcms.faq::lang.plugin.name',
            ],
        ];
    }

    public function registerNavigation()
    {
        return [
            'faq' => [
                'label' => 'FAQ',
                'url' => Backend::url('pcms/faq/faqitems'),
                'icon' => 'icon-question',
                'iconSvg' => '/plugins/pcms/faq/assets/images/faq-icon.svg',
                'permissions' => ['pcms.faq.*'],
            ],
        ];
    }

    public function boot()
    {
        $pluginManager = PluginManager::instance();
        if (
            $pluginManager->hasPlugin('RainLab.Translate') &&
            !$pluginManager->isDisabled('RainLab.Translate')
        ) {
            FAQItem::extend(function ($model) {
                $model->implement[] = 'RainLab.Translate.Behaviors.TranslatableModel';
            });
        }
    }
}
