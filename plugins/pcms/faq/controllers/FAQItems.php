<?php

namespace Pcms\FAQ\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class FAQItems extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ReorderController'
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'pcms.faq.edit'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Pcms.FAQ', 'faq');
    }
}
