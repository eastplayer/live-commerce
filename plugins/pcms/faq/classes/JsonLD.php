<?php

namespace Pcms\FAQ\Classes;

use Pcms\FAQ\Models\FAQItem;

class JsonLD
{
    public static function generate()
    {
        $faqItems = FAQItem::isActive()->get();

        $result = [
            '@context' => 'https://schema.org',
            '@type' => 'FAQPage',
        ];

        foreach ($faqItems as $faqItem) {
            $result['mainEntity'][] = [
                '@type' => 'Question',
                'name' => $faqItem->question,
                'acceptedAnswer' => [
                    '@type' => 'Answer',
                    'text' => $faqItem->answer
                ]
            ];
        }

        return $result;
    }
}