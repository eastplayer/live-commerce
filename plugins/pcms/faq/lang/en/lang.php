<?php return [
    'plugin' => [
        'name' => 'FAQ',
        'description' => 'Plugin provides a FAQ section',
        'author' => 'Andrei Shilov',
    ],
    'components' => [
        'faq' => [
            'name' => 'FAQ',
            'description' => 'Component displays the FAQ on the page',
            'params' => [
                'add_jsonld' => [
                    'name' => 'Add JSON-LD',
                    'description' => 'Add JSON-LD FAQ schema to the head section'
                ]
            ]
        ],
    ],
    'models' => [
        'faqitem' => [
            'name' => 'FAQ Item',
            'name_plural' => 'FAQ Items',
            'attributes' => [
                'id' => 'id',
                'is_active' => 'Is Active',
                'question' => 'Question',
                'answer' => 'Answer',
            ],
            'fields' => [
                'is_active' => [
                    'label' => 'Is Active'
                ],
                'question' => [
                    'label' => 'Question'
                ],
                'answer' => [
                    'label' => 'Answer'
                ],
            ],
            'columns' => [
                'is_active' => [
                    'label' => 'Is Active'
                ],
                'question' => [
                    'label' => 'Question'
                ],
                'answer' => [
                    'label' => 'Answer'
                ],
            ],
        ],
    ]
];