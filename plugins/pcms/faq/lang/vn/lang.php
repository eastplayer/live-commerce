<?php return [
    'plugin' => [
        'name' => 'FAQ',
        'description' => 'Thêm FAQ',
        'author' => 'Pcms',
    ],
    'components' => [
        'faq' => [
            'name' => 'FAQ',
            'description' => 'Hiện FAQ',
            'params' => [
                'add_jsonld' => [
                    'name' => 'Thêm JSON-LD',
                    'description' => 'Thêm JSON-LD FAQ schema vào trang'
                ]
            ]
        ],
    ],
    'models' => [
        'faqitem' => [
            'name' => 'FAQ',
            'name_plural' => 'FAQ',
            'attributes' => [
                'id' => 'id',
                'is_active' => 'Hiện',
                'question' => 'Câu hỏi',
                'answer' => 'Trả lời',
            ],
            'fields' => [
                'is_active' => [
                    'label' => 'Hiện'
                ],
                'question' => [
                    'label' => 'Câu hỏi'
                ],
                'answer' => [
                    'label' => 'Trả lời'
                ],
            ],
            'columns' => [
                'is_active' => [
                    'label' => 'Hiện'
                ],
                'question' => [
                    'label' => 'Câu hỏi'
                ],
                'answer' => [
                    'label' => 'Trả lời'
                ],
            ],
        ],
    ]
];