<?php

namespace Pcms\FormBuilder;

use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Pcms\FormBuilder\Models\Submission as Sub;
use Lang;
use Event;
use Backend;

class Plugin extends PluginBase
{
    // public $require = ['Pcms.Core'];

    public const EVENTS_PREFIX = 'pcms.formbuilder.';

    public function pluginDetails()
    {
        return [
            'name'        => 'pcms.formbuilder::lang.plugin.name',
            'description' => 'pcms.formbuilder::lang.plugin.description',
            'author'      => 'Pcms',
            'icon'        => 'oc-icon-list-alt'
        ];
    }

    public function registerNavigation()
    {
        return [
            'formbuilder' => [
                'label' => Lang::get('pcms.formbuilder::lang.plugin.name'),
                'url' => Backend::url('pcms/formbuilder/submission'),
                'icon' => 'oc-icon-list-alt',
                'permissions' => ['pcms.formbuilder.*'],
                'counter'     => Sub::isNew()->count(),
                'sideMenu' => [
                    'submissions' => [
                        'label' => Lang::get('pcms.formbuilder::lang.submissions.menu_label'),
                        'permissions' => ['pcms.formbuilder.access_submissions'],
                        'url' => Backend::url('pcms/formbuilder/submission'),
                        'icon' => 'icon-database',
                        'order' => 200,
                    ],
                    'forms' => [
                        'label' => Lang::get('pcms.formbuilder::lang.forms.menu_label'),
                        'permissions' => ['pcms.formbuilder.access_forms'],
                        'url' => Backend::url('pcms/formbuilder/form'),
                        'icon' => 'icon-sitemap',
                        'order' => 100,
                    ],
                ],
            ]
        ];
    }

    public function registerComponents()
    {
        return [
            'Pcms\FormBuilder\Components\CustomForm' => 'customForm'
        ];
    }

    public function registerPermissions()
    {
        return [
            'pcms.formbuilder.access_forms' => [
                'tab' => 'pcms.formbuilder::lang.plugin.name',
                'label' => 'pcms.formbuilder::lang.permissions.forms'
            ],
            'pcms.formbuilder.access_submissions' => [
                'tab' => 'pcms.formbuilder::lang.plugin.name',
                'label' => 'pcms.formbuilder::lang.permissions.submissions'
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'pcms.formbuilder::lang.settings.menu_label',
                'description' => 'pcms.formbuilder::lang.settings.menu_description',
                'category'    => SettingsManager::CATEGORY_CMS,
                'icon' => 'oc-icon-list-alt',
                'class' => 'Pcms\FormBuilder\Models\Settings',
                'permissions' => ['pcms.formbuilder.access_forms'],
            ]
        ];
    }

    public function registerListColumnTypes()
    {


        return [
            'sub_status' => function($value) { return '<div class="text-center"><span class="'. ($value==1 ? 'oc-icon-circle text-success' : 'text-muted oc-icon-circle text-draft') .'">' . ($value==1 ? e(trans('pcms.formbuilder::lang.models.all.status.new')) : e(trans('pcms.formbuilder::lang.models.all.status.read')) ) . '</span></div>'; }
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'pcms.formbuilder::mail.autoreply' => 'pcms.formbuilder::lang.mail.templates.autoreply',
            'pcms.formbuilder::mail.notification' => 'pcms.formbuilder::lang.mail.templates.notification',
        ];
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/views', 'pcms.formbuilder');

        Event::listen('backend.page.beforeDisplay', function ($controller, $action, $params) {
            if ($controller instanceof \Pcms\FormBuilder\Controllers\Form) {
                // Check this is the settings page for this plugin:
                if ($action === 'update' || $action === 'create') {
                    // Add CSS (minor patch)
                    $controller->addCss('/plugins/pcms/formbuilder/assets/settings-patch.css');
                    $controller->addJs('/plugins/pcms/formbuilder/assets/settings-patch.js');
                }
            }
        });
    }
}
