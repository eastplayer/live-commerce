<?php return [
    'plugin' => [
        'name' => 'Biểu mẫu',
        'description' => 'Tạo và quản lý các biểu mẫu',
    ],
    'tabs' => [
        'fields' => 'Thành phần',
        'privacy' => 'Bảo mật',
        'antispam' => 'Chống thư rác',
        'styling' => 'Styling',
        'emailing' => 'Gửi Mail',
        'caching' => 'Caching',
        'action' => 'Hành động',
        'validation' => 'Xác thực',
        'options' => 'Tùy chọn',
        'html_attributes' => 'Thuộc tính HTML',
    ],
    'forms' => [
        'menu_label' => 'Biểu mẫu',
        'field' => [
            'title' => 'Title',
            'code' => 'Code',
        ],
    ],
    'submissions' => [
        'menu_label' => 'Điền biểu mẫu',
        'scoreboard' => [
            'new_count' => 'Mới',
            'read_count' => 'Đã đọc',
            'new_description' => 'Lượt điền',
            'mark_read' => 'Đánh dấu đã đọc',
            'mark_read_confirm' => 'Bạn có chắc muốn đánh dấu đã đọc các mục đã chọn?',
            'mark_read_success' => 'Đánh dấu đã đọc thành công'
        ]
    ],
    'permissions' => [
        'forms' => 'Cho phép quản lý biểu mẫu',
        'submissions' => 'Cho phép xem nội dung điền biểu mẫu'
    ],
    'settings' => [
        'menu_label' => 'Biểu mẫu',
        'menu_description' => 'Cài đặt biểu mẫu'
    ],
    'sections' => [
        'buttons' => [
            'label' => 'Nút Styling',
        ],
        'recaptcha' => [
            'label' => 'Google ReCAPTCHA',
        ],
        'ip_restriction' => [
            'label' => 'Hạn chế IP',
        ],
        'auto_reply' => [
            'label' => 'Trả lời tự động qua Email',
        ],
        'styling' => [
            'label' => 'Biểu mẫu Styling',
            'comment' => 'All of the below can be overridden, per form and/or field',
        ],
        'validation' => [
            'label' => 'Validation Styling',
            'comment' => 'All of the below can be overridden, per form'
        ],
        'notifications' => [
            'label' => 'Thông báo tự động qua Email',
        ],
    ],
    'mail' => [
        'templates' => [
            'autoreply' => 'Pcms Forms - Auto-reply Message',
            'notification' => 'Pcms Forms - Notification Message',
        ]
    ],
    'customForm' => [
        'formCode' => [
            'title' => 'Use Form',
            'description' => 'Define the form to use by its code',
        ],
        'validation' => [
            'recaptchaFailed' => 'You must complete the reCAPTCHA verification',
            'noData' => 'No data supplied',
            'invalidNotificationRecipients' => 'Notification recipients list contains invalid email address',
            'noAutoReplyEmailField' => 'Auto-reply Email field could not be resolved',
            'noAutoReplyNameField' => 'Auto-reply Name field could not be resolved',
        ]
    ],
    'models' => [
        'all' => [
            'created_at' => [
                'label' => 'Tạo lúc',
            ],
            'url' => [
                'label' => 'Đường dẫn',
            ],
            'ip' => [
                'label' => 'Địa chỉ IP',
            ],
            'status' => [
                'label' => 'Trạng thái',
                'new' => 'Mới',
                'read' => 'Đã xem'
            ],
            'updated_at' => [
                'label' => 'Cập nhật lúc',
            ],
            'sort_order' => [
                'label' => 'Display Order',
            ],
            'override' => [
                'label' => 'Override system value',
                'comment' => 'On: Override | Off: Inherit'
            ],
        ],
        'settings' => [
            'enable_caching' => [
                'label' => 'Enable Caching',
                'comment' => 'Choose whether or not cache the rendered form'
            ],
            'cache_lifetime' => [
                'label' => 'Cache Lifetime',
                'comment' => 'Amount of minutes to cache the rendered form for'
            ],
            'form_class' => [
                'label' => 'Class của biểu mẫu',
                'comment' => 'CSS Class cho biểu mẫu này',
            ],
            'field_class' => [
                'label' => 'Field Class',
                'comment' => 'CSS Class for the Field',
            ],
            'row_class' => [
                'label' => 'Row Class',
                'comment' => 'CSS Class for the Row',
            ],
            'group_class' => [
                'label' => 'Group Class',
                'comment' => 'CSS Class for the Group',
            ],
            'label_class' => [
                'label' => 'Label Class',
                'comment' => 'CSS Class for the Label',
            ],
            'submit_class' => [
                'label' => 'Submit Class',
                'comment' => 'CSS Class of the submit button',
            ],
            'submit_text' => [
                'label' => 'Submit Text',
                'comment' => 'Text to display in the submit button',
            ],
            'enable_cancel' => [
                'label' => 'Enable Cancel',
                'comment' => 'Will go back to referer if clicked',
            ],
            'cancel_class' => [
                'label' => 'Cancel Class',
                'comment' => 'CSS Class of the cancel button (if enabled)',
            ],
            'cancel_text' => [
                'label' => 'Cancel Text',
                'comment' => 'Text to display in the cancel button (if enabled)',
            ],
            'saves_data' => [
                'label' => 'Save Data?',
                'comment' => 'Choose whether or not to save submission data to the database (recommended)',
            ],
            'store_ips' => [
                'label' => 'Có lưu địa chỉ IP không?',
                'comment' => 'Chọn nếu cần lưu trữ IP người dùng khi họ gửi biểu mẫu',
            ],
            'enable_recaptcha' => [
                'label' => 'Enable Recpatcha',
                'comment' => 'Should this Form require recpatcha?',
            ],
            'recaptcha_public_key' => [
                'label' => 'ReCAPTCHA Public Key',
                'comment' => 'Google ReCAPTCHA (Public) API Key',
            ],
            'recaptcha_secret_key' => [
                'label' => 'ReCAPTCHA Secret Key',
                'comment' => 'Google ReCAPTCHA (Private) API Key',
            ],
            'enable_ip_restriction' => [
                'label' => 'Cho phép hạn chế IP',
                'comment' => 'Chọn bật giới hạn IP cho biểu mẫu này',
            ],
            'max_requests_per_day' => [
                'label' => 'Số lần gửi tối đa mỗi ngày',
                'comment' => 'Số lần gửi tối đa mỗi ngày',
            ],
            'throttle_message' => [
                'label' => 'Thông báo gửi vượt giới hạn',
                'comment' => 'Nội dung thông báo khi người dùng đã vượt quá số lần cho phép gửi tối đa',
            ],
            'queue_emails' => [
                'label' => 'Hàng đợi Email',
                'comment' => 'Chọn nếu muốn đưa email vào hàng đợi thay vì gửi ngay tức thì (Khuyến nghị)'
            ],
            'send_notifications' => [
                'label' => 'Gửi thông báo',
                'comment' => 'Bạn sẽ nhận được thông báo bằng email mỗi khi có một lượt gửi mới',
            ],
            'notification_template' => [
                'label' => 'Mẫu Email thông báo',
                'comment' => 'Mã mẫu email thông báo (Cài đặt > Mẫu email)',
            ],
            'notification_recipients' => [
                'label' => 'Người nhận thông báo',
                'comment' => 'Dấu phẩy giữa các email',
            ],
            'auto_reply' => [
                'label' => 'Trả lời tự động',
                'comment' => 'Gửi email đến người gửi biểu mẫu.',
            ],
            'auto_reply_email_field_id' => [
                'label' => 'Trường Email',
                'comment' => 'Chọn thành phần bạn xem như nó là trường Email người nhận',
            ],
            'auto_reply_name_field_id' => [
                'label' => 'Trường tên',
                'comment' => 'Chọn thành phần bạn xem như là trường tên người nhận',
            ],
            'auto_reply_template' => [
                'label' => 'Mẫu email trả lời tự động',
                'comment' => 'Mã mẫu Email trả lời tự động (Cài đặt > Mẫu email)',
            ],
            'field_error_class' => [
                'label' => 'Field Error Class',
                'comment' => 'CSS Class to apply to a field on error',
            ],
            'field_success_class' => [
                'label' => 'Field Success Class',
                'comment' => 'CSS Class to apply to a field on success',
            ],
            'label_error_class' => [
                'label' => 'Label Error Class',
                'comment' => 'CSS Class to apply to a label on error',
            ],
            'label_success_class' => [
                'label' => 'Label Success Class',
                'comment' => 'CSS Class to apply to a label on success',
            ],
            'form_error_class' => [
                'label' => 'Form Error Class',
                'comment' => 'CSS Class to apply to a form on error',
            ],
            'form_success_class' => [
                'label' => 'Form Success Class',
                'comment' => 'CSS Class to apply to a form on success',
            ],
            'on_success' => [
                'label' => 'Hành động khi gửi thành công',
                'comment' => 'Choose what to do when the form is successfully submitted',
                'options' => [
                    'hide' => 'Ẩn biểu mẫu',
                    'clear' => 'Xóa trắng biểu mẫu',
                    'redirect' => 'Chuyển đến đường dẫn'
                ],
            ],
            'on_success_message' => [
                'label' => 'Thông báo gửi thành công',
                'comment' => 'Thông báo này sẽ hiện lên khi người dùng gửi thành công',
                'placeholder' => 'Đã gửi thành công'
            ],
            'on_success_redirect' => [
                'label' => 'URL chuyển hướng khi thành công',
                'comment' => 'Điền đường dẫn người dùng sẽ được đưa đến khi gửi thành công',
                'placeholder' => '/cam-on'
            ]
        ],
        'form' => [
            'title' => [
                'label' => 'Tiêu đề biểu mẫu',
                'comment' => ''
            ],
            'code' => [
                'label' => 'Mã biểu mẫu',
                'comment' => 'Mã biểu mẫu là duy nhất'
            ],
            'description' => [
                'label' => 'Mô tả biểu mẫu',
                'comment' => 'Một mô tả nhỏ về mục đích sử dụng biểu mẫu'
            ],
        ],
        'field' => [
            'name' => [
                'label' => 'Tên',
                'comment' => 'Được sử dụng làm nhãn cho thành phần này',
            ],
            'code' => [
                'label' => 'Mã',
                'comment' => 'Được sử dụng để định danh thành phần này trong biểu mẫu',
            ],
            'type' => [
                'label' => 'Kiểu',
                'comment' => 'Chọn kiểu của thành phần này',
            ],
            'description' => [
                'label' => 'Mô tả',
                'comment' => 'Tùy chọn. Mô tả ngắn về thành phần',
            ],
            'placeholder' => [
                'label' => 'Placeholder',
                'comment' => 'Điền thuộc tính placeholder. Đối với thẻ select, nó sẽ như một giá trị.',
            ],
            'default' => [
                'label' => 'Mặc định',
                'comment' => 'Giá trị mặc định. Đối với thẻ select/radio/checkboxes, sử dụng mã của lựa chọn.',
            ],
            'show_description' => [
                'label' => 'Hiện mô tả',
                'comment' => 'Hiện mô tả của thành phần dưới nhãn của thành phần',
            ],
            'required' => [
                'label' => 'Bắt buộc',
                'comment' => 'Đồng nghĩa với việc thêm quy tắc xác thực "bắt buộc"',
            ],
            'validation_rules' => [
                'label' => 'Quy tắc nhập',
                'comment' => 'Xem thêm cách điền: https://octobercms.com/docs/services/validation#available-validation-rules',
            ],
            'validation_message' => [
                'label' => 'Thông báo khi nhập sai',
                'comment' => 'Thông báo này sẽ hiện khi người dùng nhập sai yêu cầu của thành phần',
            ],
            'options' => [
                'label' => 'Tùy chọn',
                'comment' => 'Dành cho các lựa chọn cảu thẻ select, radio hay checkbox',
                'prompt' => 'Thêm tùy chọn',
                'fields' => [
                    'option_label' => [
                        'label' => 'Nhãn tùy chọn',
                        'comment' => 'Sử dụng như là nhãn của tùy chọn này'
                    ],
                    'option_code' => [
                        'label' => 'Mã tùy chọn',
                        'comment' => 'Sử dụng để định danh tùy chọn này'
                    ],
                    'is_optgroup' => [
                        'label' => 'Thêm tùy chọn con',
                        'comment' => 'Thêm tùy chọn phụ sẽ biến tùy chọn này thành một nhóm tùy chọn (cụ thể là `<optgroup>`)'
                    ],
                    'options' => [
                        'label' => 'Tùy chọn con',
                        'comment' => 'Thêm tùy chọn phụ vào nhóm tùy chọn này',
                        'fields' => [
                            'option_label' => [
                                'label' => 'Nhãn tùy chọn',
                                'comment' => 'Sử dụng như là nhãn của tùy chọn này'
                            ],
                            'option_code' => [
                                'label' => 'Mã tùy chọn',
                                'comment' => 'Sử dụng để định danh tùy chọn này'
                            ]
                        ]
                    ],
                ]
            ],
            'html_attributes' => [
                'label' => 'Thuộc tính HTML',
                'comment' => 'Add/override custom attributes for the field (input, select, textarea).',
                'prompt' => 'Thêm thuộc tính',
                'fields' => [
                    'attribute_name' => [
                        'label' => 'Tên thuộc tính',
                        'comment' => 'Ví dụ data-id, title, class,...',
                    ],
                    'attribute_value' => [
                        'label' => 'Giá trị thuộc tính',
                        'comment' => '',
                    ],
                ]
            ],
            'show_in_email_autoreply' => [
                'label' => 'Show in autoreply emails',
                'comment' => 'Should this field\'s value show in the autoreply emails',
            ],
            'show_in_email_notification' => [
                'label' => 'Show in notification emails',
                'comment' => 'Should this field\'s value show in notification emails',
            ],
        ]
    ]
];
