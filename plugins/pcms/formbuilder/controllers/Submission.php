<?php

namespace Pcms\FormBuilder\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Pcms\FormBuilder\Models\Submission as Sub;
use Flash;

class Submission extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
    ];

    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Pcms.FormBuilder', 'formbuilder', 'submissions');
    }

    public function details($id)
    {
        $sub = Sub::with(['form'])->findOrFail($id);
        $sub->new_message = 0;
        $sub->save();

        return $this->makePartial('details', [
            'submission' => $sub
        ]);
    }

    /**
     * Generate messages statsitics
     * @param $part
     */
    public function getRecordsStats($part)
    {

        switch ($part) {

            case 'all_count':
                return Sub::count();
                break;

            case 'read_count':
                return Sub::isRead()->count();
                break;

            case 'new_count':
                return Sub::isNew()->count();
                break;

            default:
                return NULL;
                break;
        }
    }

    /**
     * Mark messages as read
     * @param $record
     */
    public function onMarkRead()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {
            foreach ($checkedIds as $item) {
                if (!$record = Sub::find($item)) {
                    continue;
                }
                $record->new_message = 0;
                $record->save();
            }
            Flash::success(e(trans('pcms.formbuilder::lang.submissions.scoreboard.mark_read_success')));
            return $this->listRefresh();
        }
    }
}
