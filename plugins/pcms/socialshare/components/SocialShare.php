<?php

    namespace Pcms\SocialShare\Components;

    use Lang;
    use Cms\Classes\ComponentBase;

    class SocialShare extends ComponentBase {

        use \Pcms\SocialShare\Classes\Shared;

        public $defaultSort = ['facebook', 'twitter', 'linkedin', 'tumblr', 'pinterest', 'reddit', 'email'];

        public function componentDetails() {
            return [
                'name'        => 'pcms.socialshare::lang.components.socialshare.name',
                'description' => 'pcms.socialshare::lang.components.socialshare.description'
            ];
        }

        public function onRun() {
            $this->onRunShared();

        }

        public function onRender() {
        }

        public function defineProperties() {
            $properties = $this->definePropertiesShared();
            return $properties;
        }

    }

?>
