<?php namespace Pcms\SocialShare;

use System\Classes\PluginBase;

class Plugin extends PluginBase {
    // public $require = ['Pcms.Core'];

    public function pluginDetails() {
        return [
            'name'        => 'pcms.socialshare::lang.plugin.name',
            'description' => 'pcms.socialshare::lang.plugin.description',
            'author'      => 'Pcms',
            'icon'        => 'icon-share-alt'
        ];
    }

    public function registerComponents() {
        return [
            'Pcms\SocialShare\Components\SocialShare' => 'socialShare'
        ];
    }

}
