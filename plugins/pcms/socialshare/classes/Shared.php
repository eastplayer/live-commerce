<?php

    namespace Pcms\SocialShare\Classes;

    use Lang;
    use Pcms\SocialShare\Classes\ButtonsParameters as Buttons;

    trait Shared {

        public function onRunShared() {

            # GET BUTTONS PARAMETERS
            $title = $this->property('js') ? '___title___' : $this->page->title;
            $url   = $this->property('js') ? '___url___'   : url($this->currentPageUrl());

            $props = $this->getProperties();
            if($this->property('custom_order')) {
                $order = $this->customSortButtons($props);
            } else {
                $order = $this->defaultSort;
            }

            $this->properties['buttons_order'] = $order;
            $variables = $this->getVariables($props);
            $this->properties['buttons_parameters'] = Buttons::getParameters($title, $url, $variables);

        }

        public function definePropertiesShared() {

            $buttons = $this->defaultSort;

            $properties['js'] = $this->getPropertyJS();

            $properties['custom_order'] = $this->getPropertyCustomOrder();

            $i = 1;
            foreach($buttons as $button) {
                $properties[$button] = $this->getPropertyButtons($button);
                $properties['label_' . $button] = $this->getPropertyLabel($button, '');
                $properties['order_' . $button] = $this->getPropertyOrder($button, $i++, count($buttons));
                $properties['image_' . $button] = $this->getPropertyImage($button, '');
                $properties['icon_' . $button] = $this->getPropertyIcon($button, '');
                $properties['class_' . $button] = $this->getPropertyClass($button, '');
            }
            return $properties;
        }

        public static function getVariables($properties) {
            $variables = [];
            foreach($properties as $prop => $value) {
                $is = explode('_', $prop);
                if(($is[0] != 'class' && $is[0] != 'image' && $is[0] != 'label' && $is[0] != 'icon') || !isset($is[1])) { continue; }
                if(!isset($variables[$is[1]])) $variables[$is[1]] = [
                    'title' => Lang::get('pcms.socialshare::lang.shared.share_on').' '. Lang::get('pcms.socialshare::lang.settings.'.$is[1]),
                ];
                if($is[0] == 'class'){
                    $variables[$is[1]]['class'] = 'pcms-social-share-link '.$value;
                } else if($is[0] == 'image'){
                    $variables[$is[1]]['image'] = $value;
                } else if($is[0] == 'label'){
                    $variables[$is[1]]['label'] = $value;
                } else if($is[0] == 'icon'){
                    $variables[$is[1]]['icon'] = $value;
                }
            }
            return $variables;
        }

        public static function getPropertyJS() {
            return [
                'title'             => Lang::get('pcms.socialshare::lang.shared.js_title'),
                'description'       => Lang::get('pcms.socialshare::lang.shared.js_descr'),
                'type'              => 'checkbox',
                'default'           => false,
                'showExternalParam' => false
            ];
        }

        public static function getPropertyButtons($button) {
            return [
                'title'             => Lang::get('pcms.socialshare::lang.settings.'.$button),
                'description'       => 'Display ' . Lang::get('pcms.socialshare::lang.settings.'.$button) . ' button',
                'default'           => true,
                'type'              => 'checkbox',
                'showExternalParam' => false,
                'group'             => Lang::get('pcms.socialshare::lang.shared.buttons_group')
            ];
        }

        public static function getPropertyCustomOrder() {
            return [
                'title'             => Lang::get('pcms.socialshare::lang.shared.order_custom'),
                'description'       => Lang::get('pcms.socialshare::lang.shared.order_customd'),
                'default'           => false,
                'type'              => 'checkbox',
                'showExternalParam' => false,
                'group'             => Lang::get('pcms.socialshare::lang.shared.order_group'),
            ];
        }

        public static function getPropertyOrder($button, $position, $max) {
            return [
                'title'             => Lang::get('pcms.socialshare::lang.settings.'.$button),
                'description'       => Lang::get('pcms.socialshare::lang.shared.order_descr'),
                'default'           => $position,
                'type'              => 'string',
                'showExternalParam' => false,
                'group'             => Lang::get('pcms.socialshare::lang.shared.order_group'),
                'validation'        => [
                    'integer' => [
                        'message' => Lang::get('pcms.socialshare::lang.shared.order_valid'),
                        'min'     => ['value' => 1],
                        'max'     => ['value' => $max]
                    ]
                ]
            ];
        }

        public static function getPropertyClass($button, $class) {
            return [
                'title'             => Lang::get('pcms.socialshare::lang.settings.'.$button),
                'description'       => Lang::get('pcms.socialshare::lang.shared.class_descr'),
                'default'           => $class,
                'showExternalParam' => false,
                'type'              => 'string',
                'group'             => Lang::get('pcms.socialshare::lang.shared.class_group'),
            ];
        }

        public static function getPropertyIcon($button, $icon) {
            return [
                'title'             => Lang::get('pcms.socialshare::lang.settings.'.$button),
                'description'       => Lang::get('pcms.socialshare::lang.shared.icon_descr'),
                'default'           => $icon,
                'showExternalParam' => false,
                'type'              => 'string',
                'group'             => Lang::get('pcms.socialshare::lang.shared.icon_group'),
            ];
        }

        public static function getPropertyImage($button, $image) {
            return [
                'title'             => Lang::get('pcms.socialshare::lang.settings.'.$button),
                'description'       => Lang::get('pcms.socialshare::lang.shared.image_descr'),
                'default'           => $image,
                'showExternalParam' => false,
                'type'              => 'string',
                'group'             => Lang::get('pcms.socialshare::lang.shared.image_group'),
            ];
        }

        public static function getPropertyLabel($button, $label) {
            return [
                'title'             => Lang::get('pcms.socialshare::lang.settings.'.$button),
                'description'       => Lang::get('pcms.socialshare::lang.shared.label_descr'),
                'default'           => $label,
                'showExternalParam' => false,
                'type'              => 'string',
                'group'             => Lang::get('pcms.socialshare::lang.shared.label_group'),
            ];
        }

        public static function customSortButtons($properties) {
            foreach($properties as $prop => $value) {
                $is = explode('_', $prop);
                if($is[0] != 'order') { continue; }
                $order[$is[1]] = $value;
            }
            asort($order);
            return array_keys($order);
        }

    }
