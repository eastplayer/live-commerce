<?php

    namespace Pcms\SocialShare\Classes;

    use Lang;

    class ButtonsParameters {

        public static function getParameters($title, $url, $variables) {

            $parameters = [

                'facebook' => [
                    'href'  => 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode($url),
                    'title' => $variables['facebook']['title'],
                    'class' => $variables['facebook']['class'],
                    'label' => $variables['facebook']['label'],
                    'icon'  => $variables['facebook']['icon'],
                    'image' => $variables['facebook']['image']
                ],

                'twitter' => [
                    'href'  => 'https://twitter.com/share?url=' . urlencode($url) . '&text=' . urlencode($title),
                    'title' => $variables['twitter']['title'],
                    'class' => $variables['twitter']['class'],
                    'label' => $variables['twitter']['label'],
                    'icon'  => $variables['twitter']['icon'],
                    'image' => $variables['twitter']['image']
                ],

                'linkedin' => [
                    'href'  => 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode($url) . '&title=' . urlencode($title),
                    'title' => $variables['linkedin']['title'],
                    'class' => $variables['linkedin']['class'],
                    'label' => $variables['linkedin']['label'],
                    'icon'  => $variables['linkedin']['icon'],
                    'image' => $variables['linkedin']['image']
                ],

                'tumblr' => [
                    'href'  => 'https://www.tumblr.com/share?v=3&u=' . urlencode($url) . '&t=' . urlencode($title) . '&s=',
                    'title' => $variables['tumblr']['title'],
                    'class' => $variables['tumblr']['class'],
                    'label' => $variables['tumblr']['label'],
                    'icon'  => $variables['tumblr']['icon'],
                    'image' => $variables['tumblr']['image']
                ],

                'pinterest' => [
                    'href'  => 'https://pinterest.com/pin/create/button/?url=' . urlencode($url) . '&description=' . urlencode($title),
                    'title' => $variables['pinterest']['title'],
                    'class' => $variables['pinterest']['class'],
                    'label' => $variables['pinterest']['label'],
                    'icon'  => $variables['pinterest']['icon'],
                    'image' => $variables['pinterest']['image']
                ],

                'reddit' => [
                    'href'  => 'https://www.reddit.com/submit?url=' . urlencode($url) . '&title=' . urlencode($title),
                    'title' => $variables['reddit']['title'],
                    'class' => $variables['reddit']['class'],
                    'label' => $variables['reddit']['label'],
                    'icon'  => $variables['reddit']['icon'],
                    'image' => $variables['reddit']['image']
                ],

                'email' => [
                    'href'  => 'mailto:?subject=' . urlencode($title) . '&body=' . urlencode($title) . ':%20' . urlencode($url),
                    'title' => $variables['email']['title'],
                    'class' => $variables['email']['class'],
                    'label' => $variables['email']['label'],
                    'icon'  => $variables['email']['icon'],
                    'image' => $variables['email']['image']
                ],

            ];

            return $parameters;

        }

    }

?>