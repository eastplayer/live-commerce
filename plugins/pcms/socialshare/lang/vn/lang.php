<?php

    return [

        'plugin' => [
            'name'              => 'Nút chia sẻ',
            'description'       => 'Hiện nút chia sẻ mạng xã hội cho trang',
        ],

        'settings' => [
            'facebook'    => 'Facebook',
            'twitter'     => 'Twitter',
            'linkedin'    => 'LinkedIn',
            'tumblr'      => 'Tumblr',
            'pinterest'   => 'Pinterest',
            'reddit'      => 'Reddit',
            'email'       => 'Email',
        ],

        'components' => [
            'socialshare' => [
                'name'        => 'Nút chia sẻ',
                'description' => 'Hiện nút chia sẻ mạng xã hội cho trang',
            ]
        ],

        'shared' => [
            'share_on'      => 'Chia sẻ trên',
            'buttons_group' => 'Hiện/Ẩn nút',
            'order_custom'  => 'Cho phép tùy chọn thứ tự',
            'order_customd' => 'Hiện các nút theo thứ tự tùy chọn',
            'order_group'   => 'Tùy chọn thứ tự',
            'class_group'   => 'Tùy chọn Class',
            'order_descr'   => 'Chỉ sử dụng số',
            'order_valid'   => 'Số thứ tự tối đa không cho phép',
            'js_title'      => 'Javascript cho Url-Title',
            'js_descr'      => 'Sử dụng Javascript để lấy Url và Title. Hữu ích cho SEO',
            'class_descr'   => 'Thêm class để tùy chỉnh',
            'image_descr'   => 'Tùy chỉnh đường dẫn hình ảnh cho MXH này',
            'image_group'   => 'Tùy chỉnh hình ảnh',
            'label_descr'   => 'Tùy chỉnh nhãn hiển thị cho MXH này',
            'label_group'   => 'Tùy chỉnh nhãn',
            'icon_descr'    => 'Tùy chỉnh icon class cho MXH này',
            'icon_group'    => 'Tùy chỉnh icon',
        ]

    ];

?>