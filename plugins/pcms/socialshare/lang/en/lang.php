<?php

    return [

        'plugin' => [
            'name'              => 'Social share button',
            'description'       => 'Display buttons to share url on social networks',
        ],

        'settings' => [
            'facebook'    => 'Facebook',
            'twitter'     => 'Twitter',
            'linkedin'    => 'LinkedIn',
            'tumblr'      => 'Tumblr',
            'pinterest'   => 'Pinterest',
            'reddit'      => 'Reddit',
            'email'       => 'Email',
        ],

        'components' => [
            'socialshare' => [
                'name'        => 'Social share button',
                'description' => 'Display buttons to share url on social networks',
            ]
        ],

        'shared' => [
            'share_on'      => 'Share on',
            'buttons_group' => 'Show/Hide Buttons',
            'order_custom'  => 'Enable custom order',
            'order_customd' => 'Display buttons with your own custom order',
            'order_group'   => 'Custom Order',
            'order_descr'   => 'Use only numbers',
            'order_valid'   => 'Reach max of order number',
            'js_title'      => 'Use JS for Title & URL',
            'js_descr'      => 'Use JavaScript to generate page Title and URL. Useful for SEO extension',
            'class_group'   => 'Custom Class',
            'class_descr'   => 'Add custom class to share url',
            'image_descr'   => 'Add image icon to this social network',
            'image_group'   => 'Custom image',
            'label_descr'   => 'Add custom label to this social network',
            'label_group'   => 'Custom label',
            'icon_descr'    => 'Add custom icon class to this social network',
            'icon_group'    => 'Custom icon',
        ]

    ];

?>