<?php namespace Pcms\DataBlocks\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use BackendAuth;

class Blocks extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'pcms.datablocks.*'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Pcms.DataBlocks', 'datablocks', 'datablocks-blocks');
        $this->bodyClass = 'compact-container';
    }
}
