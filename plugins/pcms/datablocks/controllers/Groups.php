<?php namespace Pcms\DataBlocks\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Groups extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'pcms.datablocks.manage_groups' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Pcms.DataBlocks', 'datablocks', 'datablocks-groups');
        $this->bodyClass = 'compact-container';
    }
}
