<?php namespace Pcms\DataBlocks\Models;

use Model;
use Cms\Classes\Page;

/**
 * Model
 */
class Group extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    public $table = 'pcms_datablocks_groups';

    public $rules = [
        'name' => 'required|between:3,64',
        'code' => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:pcms_datablocks_groups'],
    ];
    protected $jsonable = ['properties'];
    public $attachMany = [
        'images' => \System\Models\File::class
    ];
}