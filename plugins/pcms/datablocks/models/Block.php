<?php namespace Pcms\DataBlocks\Models;

use Lang;
use Request;
use Model;
use Cms\Classes\Page;
use RainLab\Translate\Classes\Translator;
use October\Rain\Html\Helper as HtmlHelper;
use RainLab\Translate\Models\Locale as LocaleModel;

/**
 * Model
 */
class Block extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Purgeable;
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $table = 'pcms_datablocks_blocks';

    public $rules = [
        'name' => 'required|between:3,64',
        'code' => ['required', 'regex:/^[a-z0-9\/\:_\-\*\[\]\+\?\|]*$/i', 'unique:pcms_datablocks_blocks'],
    ];
    public $translatable = ['data'];
    public $purgeable = ['dataParsed'];
    public $belongsTo = [
        'group' => Group::class
    ];
    public $jsonable = ['data'];
    public function getAttributeTranslated($key, $locale){
        if(strpos($key, 'data[') !== false){
            $translate = Translator::instance();
            $translatableDefault = $translate->getDefaultLocale();
            
            if ($locale == $translatableDefault) {
                $keyArray = HtmlHelper::nameToArray($key);
                return array_get($this, implode('.', $keyArray));
            } else {
                $localeTranslate = $this->getAttributeTranslated('data', $locale);
                if(isset($localeTranslate)){
                    $keyArray = HtmlHelper::nameToArray($key);
                    if(isset($localeTranslate[$keyArray[1]])) return $localeTranslate[$keyArray[1]];
                }
            }
        }
        return parent::getAttributeTranslated($key, $locale);
    }
    public function afterFetch(){
        $dataParsed = $this->data;
        $locale = LocaleModel::where('is_default', true)->first();
        $default = $this->getAttributeTranslated('data', $locale->code);
        foreach($this->group->properties as $property){
            if(!$property['translatable'] && isset($default[$property['name']])){
                $dataParsed[$property['name']] = $default[$property['name']];
            }
        }
        $this->dataParsed = $dataParsed;
    }
}