<?php return [
    'plugin' => [
        'name' => 'Dữ liệu',
        'description' => 'Cho phép lưu trữ dữ liệu theme website',
    ],
    'editor' => [
        'custom_fields_tab' => 'Custom fields',
    ],
    'property' => [
        'type' => 'Loại',
        'comment' => 'Comment',
        'default' => 'Giá trị mặc định',
        'name' => 'Tên trường (vd: footer_text)',
        'label' => 'Label',
        'is_translatable' => 'Đa ngôn ngữ?',
    ],
    'group' => [
        'is_repeater' => 'Repeater?',
        'name' => 'Tên',
        'page' => 'Page',
        'properties' => 'Các trường',
        'code' => 'Mã',
        'updated_at' => 'Updated at',
        'created_at' => 'Created at',
        'position' => 'Tab position',
    ],
    'permession' => [
        'manage_properties' => 'Manage properties',
        'manage_groups' => 'Manage groups',
        'manager_cms_code_content' => 'Edit cms code content',
    ],
    'menu' => [
        'groups' => 'Groups',
    ],
];