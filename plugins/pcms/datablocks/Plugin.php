<?php namespace Pcms\DataBlocks;

use System\Classes\PluginBase;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Cms\Classes\Layout;
use October\Rain\Parse\Syntax\Parser as SyntaxParser;
use DB;
use Request;
use Pcms\DataBlocks\Models\Block;
use Pcms\DataBlocks\Controllers\Blocks;
use Pcms\DataBlocks\Models\Group;
use Pcms\DataBlocks\Models\Property;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
        return [
            'Pcms\DataBlocks\Components\Block' => 'DataBlock',
        ];
    }

    public function registerSettings()
    {
    }
    public function register() {
        if(Request::get('group')){
            if(DB::table('pcms_datablocks_groups')->where('id', Request::get('group'))->first()){
                Block::extend(function($model) {
                    $group = Group::where('id', Request::get('group'))->first();
                    $translatable = $model->translatable;
                    foreach ($group->properties as $value) {
                        if($value['translatable'] == '1') $translatable[] = 'data['.$value['name'].']';
                    }
                    $model->translatable = $translatable;
                });
                \Event::listen('backend.form.extendFieldsBefore', function ($widget) {
                    if (!$widget->model instanceof Block) {
                        return;
                    }
                    if ($widget->isNested) {
                        return;
                    }
                    $group = Group::where('id', Request::get('group'))->first();
                    $widget->fields["group_id"] = [
                        'type' => 'number',
                        'label' => 'group',
                        'default' => Request::get('group'),
                        'value' => Request::get('group'),
                        'cssClass' => 'hidden'
                    ];
                    foreach ($group->properties as $value) {
                        $field = [];
                        if($value['_group'] == 'repeater'){
                            $field = [
                                'label' => $value["label"],
                                'span' => 'full',
                                'form' => ['fields' => []],
                                'type' => 'repeater'
                            ];
                            foreach ($value['items'] as $property) {
                                $field['form']['fields'][$property['name']] = [
                                    'label' => $property['label'],
                                    'type' => $property['type'],
                                    'span' => $property['is_full']?'full':'auto',
                                ];
                                if($property['type'] == 'mediafinder') $field['form']['fields'][$property['name']]['mode'] = 'image';
                                if($property['type'] == 'dropdown'){
                                    $field['form']['fields'][$property['name']]['options'] = [];
                                    if($property['options'] && sizeOf($property['options']) > 0){
                                        foreach($property['options'] as $option){
                                            $field['form']['fields'][$property['name']]['options'][$option['name']] = $option['label'];
                                        }
                                    } else {
                                        $field['form']['fields'][$property['name']]['options'][''] = 'Chưa có lựa chọn';
                                    }
                                }

                            }
                        } else {
                            $field = [
                                'label' => $value["label"],
                                'span' => $value['is_full']?'full':'auto',
                                'type'  => $value['type']
                            ];
                            if($value['type'] == 'mediafinder') $field['mode'] = 'image';
                            if($value['type'] == 'dropdown'){
                                $field['options'] = [];
                                if($value['options'] && sizeOf($value['options']) > 0){
                                    foreach($value['options'] as $option){
                                        $field['options'][$option['name']] = $option['label'];
                                    }
                                } else {
                                    $field['options'][''] = 'Chưa có lựa chọn';
                                }
                            }
                        }
                        $widget->fields['data['.$value['name'].']'] = $field;
                    }
                }); 
            }
        }
    }


    
}
