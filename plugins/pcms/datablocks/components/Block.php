<?php namespace Pcms\DataBlocks\Components;

use Cms\Classes\ComponentBase;
use Pcms\DataBlocks\Models\Block as BlockModel;

class Block extends ComponentBase
{
    public $block;
    public $data;
    public function componentDetails()
    {
        return [
            'name'        => 'Block',
            'description' => 'Lấy data block'
        ];
    }

    public function defineProperties()
    {
        return [
            'code' => [
                'title'       => 'Block code',
                'type'        => 'dropdown',
            ],
        ];
    }

    public function getCodeOptions()
    {
        return BlockModel::get()->lists('name', 'code');
    }

    public function onRun()
    {
        if($data = $this->block = BlockModel::where("code", $this->property('code'))->first()){
            $this->data = $data->dataParsed;
        }
    }
}
