<?php namespace Pcms\DataBlocks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePcmsDataBlocksBlocks extends Migration
{
    public function up()
    {
        Schema::create('pcms_datablocks_blocks', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->integer('group_id')->nullable()->unsigned(false);
            $table->string('code')->nullable();
            $table->text('data');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pcms_datablocks_blocks');
    }
}
