<?php namespace Pcms\DataBlocks\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreatePcmsDataBlocksGroups extends Migration
{
    public function up()
    {
        Schema::create('pcms_datablocks_groups', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('code')->nullable();
            $table->text('properties');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('pcms_datablocks_groups');
    }
}
