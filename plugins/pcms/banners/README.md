```
[banners]
==
{% if banners is not empty %}
<div class="banners">
    <div class="stories">
        {% component 'banners' %}
    </div>
</div>
{% endif %}
```