<?php namespace Pcms\Banners\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class CreateBannerLocationTable extends Migration
{
    public function up()
    {
        Schema::create('pcms_banners_banner_location', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('banner_id')->unsigned()->nullable()->default(null);
            $table->integer('location_id')->unsigned()->nullable()->default(null);
            $table->index(['banner_id', 'location_id']);
            $table->foreign('banner_id')->references('id')->on('pcms_banners_banners')->onDelete('cascade');
            $table->foreign('location_id')->references('id')->on('pcms_banners_locations')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pcms_banners_banner_location');
    }
}
