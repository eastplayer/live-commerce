<?php namespace Pcms\Banners\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateLocationsTable extends Migration
{
    public function up()
    {
        Schema::create('pcms_banners_locations', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('name', 300);
            $table->string('slug', 300);
            $table->text('description', 300)->nullable();
            $table->boolean('enabled')->default(true);
            $table->smallInteger('sort_order')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->integer('user_id')->unsigned()->nullable()->index();

        });
    }

    public function down()
    {
        Schema::dropIfExists('pcms_banners_locations');
    }
}
