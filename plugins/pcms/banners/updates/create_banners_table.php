<?php namespace Pcms\Banners\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBannersTable extends Migration
{
    public function up()
    {
        Schema::create('pcms_banners_banners', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 300)->nullable();
            $table->boolean('approved')->default(true);
            $table->boolean('always')->default(true);
            $table->date('from');
            $table->date('to');
            $table->boolean('sort_order')->nullable();
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->text('content_html')->nullable();
            $table->text('content_css')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pcms_banners_banners');
    }
}
