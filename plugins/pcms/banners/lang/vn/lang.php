<?php
return [
    'app'         => [
        'name'        => 'Banner',
        'description' => 'Thêm banner của khách hàng vào website',
        'nav_label' => 'Banner đang được hiện'
    ],
    'banners'  => [
        'menu_label' => 'Banner',
        'manage_title' => 'Quản lý banner',
        'filter_date' => 'Ngày',
        'filter_location' => 'Vị trí hiện'
    ],
    'banner'  => [
        'create_title' => 'Thêm banner',
        'change_order' => 'Sắp xếp thứ tự',
        'edit_label' => 'Sửa banner',
        'pbanner_title' => 'Xem trước banner'
    ],
    'locations'  => [
        'menu_label' => 'Vị trí hiện',
        'manage_title' => 'Quản lý vị trí hiện'
    ],
    'location'    => [
        'create_title' => 'Thêm vị trí hiện',
        'edit_label' => 'Sửa vị trí hiện',
        'pbanner_title' => 'Xem trước vị trí hiện'
    ],
    'components' => [
        'banners' => [
            'name' => 'Danh sách banner',
            'desc' => 'Hiển thị danh sách banner',
            'location' => 'Vị trí hiện được chọn',
            'location_desc' => 'Chỉ hiện banner từ danh muc được chọn dựa trên slug'
        ]
    ],
    'fields'      => [
        'id'                => 'ID',
        'name'                => 'Tên banner',
        'approved'               => 'Cho phép hiện',
        'always'                => 'Hiện không thời hạn',
        'from'         => 'Hiện vào lúc',
        'to'           => 'Kết thúc lúc',
        'locations'            => 'Vị trí hiện',
        'sort_order'            => 'Thứ tự',
        'created_at' => 'Thời gian tạo',
        'status' => 'Trạng thái',
        'location_name' => 'Tên vị trí hiện',
        'location_slug' => 'Slug',
        'location_enabled' => 'Cho phép hiện',
        'location_description' => 'Mô tả',
        'background_img' => 'Ảnh background',
        'content_html' => 'HTML',
        'content_css' => 'CSS',
        'url' => 'Url'
    ],
    'status' => [
        'expired' => 'Hết hạn',
        'waiting' => 'Chờ hiện',
        'unapproved' => 'Không cho phép',
        'always' => 'Luôn hiện',
        'available' => 'Đang hiện',
    ],
    'tab'         => [
        'general'    => 'Tổng quan',
        'locations'      => 'Vị trí hiện',
    ],
    'permission'  => [
        'access_banners'   => 'Quản lý banner',
        'access_locations' => 'Quản lý vị trí hiện banner',
    ]
];
