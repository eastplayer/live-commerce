<?php namespace Pcms\Banners\Facades;

use Pcms\Banners\Models\Location;
use Pcms\Banners\Models\Banner;

/**
 * Main plugin facade. You should call this methods a priority.
 *
 * @package Pcms\Banners\Facades
 */
class BannersFacade
{
    /** @var Banner $banners */
    private $banners;

    /**
     * BannersFacade constructor.
     *
     * @param Banner $banners
     */
    public function __construct(Banner $banners)
    {
        $this->banners = $banners;
    }

    /**
     * Create new banner.
     *
     * @param array $data
     *
     * @return array
     */
    public function storeBanner(array $data)
    {
        return $this->banners->create($data);
    }

    /**
     * Get approved banners (for displaying at frontend).
     *
     * @param Location $location Filter results by location.
     *
     * @return array
     */
    public function getApprovedBanners($location = null)
    {
        $query = $this->banners->isApproved()->orderBy('sort_order');

        if ($location !== null) {
            $query->whereHas('locations', function($query) use ($location) {
                $query->where('location_id', $location->id);
            });
        }

        return $query->get();
    }

    /**
     * Get non approved banners (for admin approval).
     *
     * @return array
     */
    public function getNonApprovedBanners()
    {
        return $this->banners->notApproved()->orderBy('sort_order')->get();
    }

    /**
     * Find one banner.
     *
     * @param $value
     * @param string $key
     *
     * @return Banner
     */
    public function findOne($value, $key = 'id')
    {
        return $this->banners->where($key, $value)->first();
    }

    public function countAvailableBanners()
    {
        return $this->banners->isApproved()->count();
    }
}
