<?php namespace Pcms\Banners\Components;

use App;
use Cms\Classes\ComponentBase;
use Pcms\Banners\Facades\BannersFacade;
use Pcms\Banners\Models\Location as LocationModel;
use Lang;

class Location extends ComponentBase
{
    private $banners;

    public function componentDetails()
    {
        return [
            'name' => Lang::get('pcms.banners::lang.components.banners.name'),
            'description' => Lang::get('pcms.banners::lang.components.banners.desc')
        ];
    }

    public function defineProperties()
    {
        return [
            'locationFilter' => [
                'title' => Lang::get('pcms.banners::lang.components.banners.location'),
                'description' => Lang::get('pcms.banners::lang.components.banners.location_desc'),
                'type' => 'string',
                'default' => '{{ :location }}',
            ],
        ];
    }

    public function onRun()
    {
        // location filter
        $location = null;
        if ($locationSlug = $this->property('locationFilter')) {
            $location = $this->getLocation($locationSlug);
        }
        $this->page['location'] = $location;
        $this->page['banners'] = $this->banners($location);
    }

    /**
     * Get banners.
     *
     * @param Location $location Filter by location.
     *
     * @return mixed
     */
    public function banners($location = null)
    {
        if ($this->banners === null) {
            $this->banners = $this->getFacade()->getApprovedBanners($location);
        }

        return $this->banners;
    }

    /**
     * Get location by slug.
     *
     * @param $location
     *
     * @return mixed
     */
    public function getLocation($location)
    {
        return LocationModel::where('slug', $location)->first();
    }

    /**
     * Get Facade.
     *
     * @return BannersFacade
     */
    private function getFacade()
    {
        return App::make('pcms.banners.facade');
    }
}
