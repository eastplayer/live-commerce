<?php

namespace Pcms\Banners\Models;

use App;
use Carbon\Carbon;
use Config;
use Model;
use October\Rain\Database\Traits\Sortable as SortableTrait;
use October\Rain\Database\Traits\Validation as ValidationTrait;
use Request;
use Str;
use Lang;
use System\Models\File;

/**
 * Banners class.
 *
 * @package Pcms\Banners\Models
 */
class Banner extends Model
{
    use SortableTrait;

    use ValidationTrait;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /** @var string $table The database table used by the model */
    public $table = 'pcms_banners_banners';

    /** @var array Rules */
    public $rules = [
        'name' => 'required|max:300',
        'background_img'   => 'nullable|image|max:4000',
        'approved' => 'boolean',
    ];

    public $translatable = ['name', 'title', 'content'];

    public $fillable = [
        'name', 'content_html', 'approved',
    ];

    public $dates = ['created_at', 'updated_at', 'deleted_at'];

    public $belongsTo = [
        'user' => ['Backend\Models\User']
    ];

    public $belongsToMany = [
        'locations' => [
            'Pcms\Banners\Models\Location',
            'table' => 'pcms_banners_banner_location',
            'order' => 'name asc',
            'scope' => 'isEnabled',
            'timestamps' => true,
        ]
    ];

    public $attachOne = [
        'background_img' => File::class,
    ];

    public function afterDelete()
    {
        $this->background_img && $this->background_img->delete();

        parent::afterDelete();
    }

    /**
     * Scope for getting approved banners.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeIsApproved($query)
    {
        $now = Carbon::now()->toDateString();
        return $query->where('approved', true)->where('from', '<=', $now)->where(function ($query) use ($now) {
            $query->where('always', true)
                  ->orWhere('to', '>=', $now);
        });
    }

    /**
     * Scope for getting non approved banners.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeNotApproved($query)
    {
        return $query->where('approved', false);
    }

    /**
     * Set machine scope.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeMachine($query)
    {
        $ip = Request::server('REMOTE_ADDR');
        $ip_forwarded = Request::server('HTTP_X_FORWARDED_FOR');
        $user_agent = Request::server('HTTP_USER_AGENT');

        return $query->whereIp($ip)->whereIpForwarded($ip_forwarded)->whereUserAgent($user_agent);
    }

    /**
     * If some message exists in last 30 seconds.
     *
     * @return bool
     */
    public function isExistInLastTime()
    {
        // protection time
        $time = Config::get('pcms.banners::config.protection_time', '-30 seconds');
        $timeLimit = Carbon::parse($time)->toDateTimeString();

        // try to find some message
        $item = self::machine()->where('created_at', '>', $timeLimit)->first();

        return $item !== null;
    }

    public function getToValAttribute()
    {
        if ($this->always){
            return '<span class="banners-status">'.'<span class="badge badge-success">' . Lang::get('pcms.banners::lang.status.always') . '</span>'.'</span>';
        }else{
            return Carbon::parse($this->to)->format('d.m.Y');
        }
    }

    public function getStatusAttribute()
    {
        $status = '<span class="banners-status">';
        $now = Carbon::now()->toDateString();
        if ($this->approved) {
            if ($this->always && $this->from <= $now) {
                $status .= '<span class="badge badge-success">' . Lang::get('pcms.banners::lang.status.available') . '</span>';
            } else if ($this->always && $this->from >= $now) {
                $status .= '<span class="badge badge-secondary">' . Lang::get('pcms.banners::lang.status.waiting') . '</span>';
            } else if ($this->to < $now) {
                $status .= '<span class="badge badge-warning">' . Lang::get('pcms.banners::lang.status.expired') . '</span>';
            } else if ($this->from > $now) {
                $status .= '<span class="badge badge-secondary">' . Lang::get('pcms.banners::lang.status.waiting') . '</span>';
            } else if ($this->from <= $now) {
                $status .= '<span class="badge badge-success">' . Lang::get('pcms.banners::lang.status.available') . '</span>';
            } else {
                $status .= '<span class="badge badge-danger">' . Lang::get('pcms.banners::lang.status.unapproved') . '</span>';
            }
        } else {
            $status .= '<span class="badge badge-danger">' . Lang::get('pcms.banners::lang.status.unapproved') . '</span>';
        }
        $status .= '</span>';
        return $status;
    }
    
    public function scopeFilterLocations($query, $locations)
    {
        return $query->whereHas('locations', function($q) use ($locations) {
            $q->whereIn('pcms_banners_banners.id', $locations);
        });
    }
    
}
