<?php namespace Pcms\Banners\Models;

use Model;
use October\Rain\Database\Traits\SoftDelete as SoftDeletingTrait;
use October\Rain\Database\Traits\Sortable as SortableTrait;
use October\Rain\Database\Traits\Validation as ValidationTrait;

class Location extends Model
{
    use SoftDeletingTrait;

    use SortableTrait;

    use ValidationTrait;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $table = 'pcms_banners_locations';

    public $rules = [
        'name' => 'required|max:255',
        'slug' => 'required|unique:pcms_banners_locations',
        'enabled' => 'boolean',
        'description' => 'max:10000',
    ];

    public $translatable = ['name', 'slug', 'description'];

    public $dates = ['created_at', 'updated_at', 'deleted_at'];

    public $attachOne = [
        'image' => ['System\Models\File'],
    ];

    public $belongsTo = [
        'user' => ['Backend\Models\User']
    ];

    public $belongsToMany = [
        'banners' => ['Pcms\Banners\Models\Banner',
            'table' => 'pcms_banners_banner_location',
            'order' => 'name desc',
            'scope' => 'isEnabled',
            'timestamps' => true,
        ],
    ];

    public function scopeIsEnabled($query)
    {
        return $query->where('enabled', true);
    }
    
}
