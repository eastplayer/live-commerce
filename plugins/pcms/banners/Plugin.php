<?php

namespace Pcms\Banners;

use App;
use Lang;
use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    // public $require = ['Pcms.Core'];
    public function boot()
    {
        $this->app->bind('pcms.banners.facade', 'pcms\Banners\Facades\BannersFacade');
    }

    public function pluginDetails()
    {
        return [
            'name'        => Lang::get('pcms.banners::lang.app.name'),
            'description' => Lang::get('pcms.banners::lang.app.description'),
            'author'      => 'pcms',
            'icon'        => 'oc-icon-crosshairs'
        ];
    }

    public function registerNavigation()
    {
        $this->app->bind('pcms.banners.facade', 'pcms\Banners\Facades\BannersFacade');
        return [
            'banners' => [
                'label' => Lang::get('pcms.banners::lang.app.name'),
                'url' => Backend::url('pcms/banners/banners'),
                'icon' => 'oc-icon-crosshairs',
                'permissions' => ['pcms.banners.*'],
                'order' => 510,
                'counter'     => App::make('pcms.banners.facade')->countAvailableBanners(),
                'counterLabel' => Lang::get('pcms.banners::lang.app.nav_label'),
                'sideMenu' => [
                    'banners' => [
                        'label' => Lang::get('pcms.banners::lang.banners.menu_label'),
                        'url' => Backend::url('pcms/banners/banners'),
                        'icon' => 'oc-icon-image',
                        'permissions' => ['pcms.banners.banners'],
                        'order' => 100,
                    ],
                    'locations' => [
                        'permissions' => ['pcms.banners.locations'],
                        'label' => Lang::get('pcms.banners::lang.locations.menu_label'),
                        'icon' => 'oc-icon-crosshairs',
                        'url' => Backend::url('pcms/banners/locations'),
                        'order' => 200,
                    ],
                ],
            ]
        ];
    }

    public function registerComponents()
    {
        return [
            'Pcms\Banners\Components\Location' => 'bannerLocation',
        ];
    }

    public function registerPermissions()
    {
        return [
            'pcms.banners.access_banners'  => ['tab' => Lang::get('pcms.banners::lang.app.name'), 'label' => Lang::get('pcms.banners::lang.permission.access_banners')],
            'pcms.banners.access_locations'  => ['tab' => Lang::get('pcms.banners::lang.app.name'), 'label' => Lang::get('pcms.banners::lang.permission.access_locations')],
        ];
    }
}
