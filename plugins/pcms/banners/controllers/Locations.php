<?php namespace Pcms\Banners\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Locations Back-end Controller
 */
class Locations extends Controller
{
    public $implement = [
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\ReorderController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'pcms.banners.locations',
    ];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Pcms.Banners', 'banners', 'locations');
    }
    public function formBeforeCreate($model)
    {
        $model->user_id = $this->user->id;
    }

}
