<?php namespace Pcms\Reviews\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class CreateReviewCategoryTable extends Migration
{
    public function up()
    {
        Schema::create('pcms_reviews_review_category', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('review_id')->unsigned()->nullable()->default(null);
            $table->integer('category_id')->unsigned()->nullable()->default(null);
            $table->index(['review_id', 'category_id']);
            $table->foreign('review_id')->references('id')->on('pcms_reviews_reviews')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('pcms_reviews_categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pcms_reviews_review_category');
    }
}
