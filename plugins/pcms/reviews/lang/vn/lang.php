<?php
return [
    'app'         => [
        'name'        => 'Đánh giá',
        'description' => 'Thêm đánh giá của khách hàng vào website',
        'nav_label' => 'Đánh giá đang được hiện'
    ],
    'reviews'  => [
        'menu_label' => 'Đánh giá',
        'manage_title' => 'Quản lý đánh giá',
        'filter_date' => 'Ngày',
        'filter_category' => 'Danh mục'
    ],
    'review'  => [
        'create_title' => 'Thêm đánh giá',
        'change_order' => 'Sắp xếp thứ tự',
        'edit_label' => 'Sửa đánh giá',
        'preview_title' => 'Xem trước đánh giá'
    ],
    'categories'  => [
        'menu_label' => 'Danh mục',
        'manage_title' => 'Quản lý danh mục'
    ],
    'category'    => [
        'create_title' => 'Thêm danh mục',
        'edit_label' => 'Sửa danh mục',
        'preview_title' => 'Xem trước danh mục'
    ],
    'components' => [
        'reviews' => [
            'name' => 'Danh sách đánh giá',
            'desc' => 'Hiển thị danh sách đánh giá',
            'category' => 'Danh mục được chọn',
            'category_desc' => 'Chỉ hiện đánh giá từ danh muc được chọn dựa trên slug'
        ]
    ],
    'fields'      => [
        'id'                => 'ID',
        'name'                => 'Người đánh giá',
        'email'                 => 'Địa chỉ email',
        'title'             => 'Tiêu đề',
        'content'         => 'Nội dung',
        'approved'               => 'Cho phép hiện',
        'always'                => 'Hiện không thời hạn',
        'from'         => 'Hiện vào lúc',
        'to'           => 'Kết thúc lúc',
        'categories'            => 'Danh mục',
        'sort_order'            => 'Thứ tự',
        'created_at' => 'Thời gian tạo',
        'rating' => 'Điểm đánh giá',
        'status' => 'Trạng thái',
        'category_name' => 'Tên danh mục',
        'category_slug' => 'Đường dẫn',
        'category_enabled' => 'Cho phép hiện',
        'category_description' => 'Mô tả',
        'avatar' => 'Ảnh đại diện'
    ],
    'status' => [
        'expired' => 'Hết hạn',
        'waiting' => 'Chờ hiện',
        'unapproved' => 'Không cho phép',
        'always' => 'Luôn hiện',
        'available' => 'Đang hiện',
    ],
    'tab'         => [
        'general'    => 'Tổng quan',
        'categories'      => 'Danh mục',
    ],
    'permission'  => [
        'access_reviews'   => 'Quản lý đánh giá',
        'access_categories' => 'Quản lý danh mục đánh giá',
    ]
];
