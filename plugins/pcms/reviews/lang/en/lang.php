<?php
return [
    'app'         => [
        'name'        => 'Testimonial',
        'description' => 'Add customer\'s testimonials to website',
        'nav_label' => 'Ongoing testimonials'
    ],
    'reviews'  => [
        'menu_label' => 'Testimonial',
        'manage_title' => 'Manage testimonials',
        'filter_date' => 'Date',
        'filter_category' => 'Category'
    ],
    'review'  => [
        'create_title' => 'Add testimonial',
        'change_order' => 'change orders',
        'edit_label' => 'Edit testimonial',
        'preview_title' => 'Preview testimonial'
    ],
    'categories'  => [
        'menu_label' => 'Categories',
        'manage_title' => 'manage categories'
    ],
    'category'    => [
        'create_title' => 'Add category',
        'edit_label' => 'Edit category',
        'preview_title' => 'Preview category'
    ],
    'components' => [
        'reviews' => [
            'name' => 'Testimonials',
            'desc' => 'Display list of testimonial',
            'category' => 'Category Ssug',
            'category_desc' => 'Only show testimonials in category slug'
        ]
    ],
    'fields'      => [
        'id'                => 'ID',
        'name'                => 'Reviewer',
        'email'                 => 'Email',
        'title'             => 'Title',
        'content'         => 'Content',
        'approved'               => 'Enable to show',
        'always'                => 'Always show thí testimonial',
        'from'         => 'Show at',
        'to'           => 'End at',
        'categories'            => 'Categories',
        'sort_order'            => 'Sort order',
        'created_at' => 'Created at',
        'rating' => 'Rating',
        'status' => 'Status',
        'category_name' => 'Category name',
        'category_slug' => 'Slug',
        'category_enabled' => 'Enable to show',
        'category_description' => 'Description',
        'avatar' => 'Avatar'
    ],
    'status' => [
        'expired' => 'Expired',
        'waiting' => 'Coming up',
        'unapproved' => 'Unapproved',
        'always' => 'Always show',
        'available' => 'Ongoing',
    ],
    'tab'         => [
        'general'    => 'General',
        'categories'      => 'Categories',
    ],
    'permission'  => [
        'access_reviews'   => 'Manage testimonials',
        'access_categories' => 'Manage testimonial\'s categories',
    ]
];