```
[reviews]
==
{% if reviews is not empty %}
<div class="reviews">
    <h3>What our customers say about us?</h3>
    <div class="stories">
        {% component 'reviews' %}
    </div>
</div>
{% endif %}
```