<?php

namespace Pcms\Reviews;

use App;
use Lang;
use Backend;
use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    // public $require = ['Pcms.Core'];
    public function boot()
    {
        $this->app->bind('pcms.reviews.facade', 'pcms\Reviews\Facades\ReviewsFacade');
    }

    public function pluginDetails()
    {
        return [
            'name'        => Lang::get('pcms.reviews::lang.app.name'),
            'description' => Lang::get('pcms.reviews::lang.app.description'),
            'author'      => 'pcms',
            'icon'        => 'icon-star-half-o'
        ];
    }

    public function registerNavigation()
    {
        $this->app->bind('pcms.reviews.facade', 'pcms\Reviews\Facades\ReviewsFacade');
        return [
            'reviews' => [
                'label' => Lang::get('pcms.reviews::lang.app.name'),
                'url' => Backend::url('pcms/reviews/reviews'),
                'icon' => 'icon-star-half-o',
                'permissions' => ['pcms.reviews.*'],
                'order' => 510,
                'counter'     => App::make('pcms.reviews.facade')->countAvailableReviews(),
                'counterLabel' => Lang::get('pcms.reviews::lang.app.nav_label'),
                'sideMenu' => [
                    'reviews' => [
                        'label' => Lang::get('pcms.reviews::lang.reviews.menu_label'),
                        'url' => Backend::url('pcms/reviews/reviews'),
                        'icon' => 'icon-star-half-o',
                        'permissions' => ['pcms.reviews.reviews'],
                        'order' => 100,
                    ],
                    'categories' => [
                        'permissions' => ['pcms.reviews.categories'],
                        'label' => Lang::get('pcms.reviews::lang.categories.menu_label'),
                        'icon' => 'icon-folder',
                        'url' => Backend::url('pcms/reviews/categories'),
                        'order' => 200,
                    ],
                ],
            ]
        ];
    }

    public function registerComponents()
    {
        return [
            'Pcms\Reviews\Components\Reviews' => 'reviews',
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'Pcms\Reviews\FormWidgets\StarRating' => 'starrating',
        ];
    }

    public function registerPermissions()
    {
        return [
            'pcms.reviews.access_reviews'  => ['tab' => Lang::get('pcms.reviews::lang.app.name'), 'label' => Lang::get('pcms.reviews::lang.permission.access_reviews')],
            'pcms.reviews.access_categories'  => ['tab' => Lang::get('pcms.reviews::lang.app.name'), 'label' => Lang::get('pcms.reviews::lang.permission.access_categories')],
        ];
    }
}
