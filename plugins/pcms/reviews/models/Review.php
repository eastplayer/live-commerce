<?php

namespace Pcms\Reviews\Models;

use App;
use Carbon\Carbon;
use Config;
use Model;
use October\Rain\Database\Traits\SoftDelete as SoftDeleteTrait;
use October\Rain\Database\Traits\Sortable as SortableTrait;
use October\Rain\Database\Traits\Validation as ValidationTrait;
use Request;
use Str;
use Lang;

/**
 * Reviews class.
 *
 * @package Pcms\Reviews\Models
 */
class Review extends Model
{
    use SoftDeleteTrait;

    use SortableTrait;

    use ValidationTrait;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /** @var string $table The database table used by the model */
    public $table = 'pcms_reviews_reviews';

    /** @var array Rules */
    public $rules = [
        'name' => 'required|max:300',
        'avatar'   => 'nullable|image|max:4000',
        'email' => 'email',
        'rating' => 'numeric',
        'approved' => 'boolean',
        'content' => 'required|min:6|max:3000',
    ];

    public $translatable = ['name', 'title', 'content'];

    public $fillable = [
        'email', 'name', 'title', 'rating', 'content', 'approved',
    ];

    public $dates = ['created_at', 'updated_at', 'deleted_at'];

    public $belongsTo = [
        'user' => ['Backend\Models\User']
    ];

    public $belongsToMany = [
        'categories' => [
            'Pcms\Reviews\Models\Category',
            'table' => 'pcms_reviews_review_category',
            'order' => 'name asc',
            'scope' => 'isEnabled',
            'timestamps' => true,
        ]
    ];

    public $attachOne = [
        'avatar' => \System\Models\File::class
    ];
    /**
     * Before create review.
     */
    public function beforeCreate()
    {
        $this->hash = $this->getUniqueHash();
        $this->locale = App::getLocale();

        $this->ip = Request::server('REMOTE_ADDR');
        $this->ip_forwarded = Request::server('HTTP_X_FORWARDED_FOR');
        $this->user_agent = Request::server('HTTP_USER_AGENT');
    }

    public function afterDelete()
    {
        $this->avatar && $this->avatar->delete();

        parent::afterDelete();
    }

    /**
     * Scope for getting approved reviews.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeIsApproved($query)
    {
        $now = Carbon::now()->toDateString();
        return $query->where('approved', true)->where('from', '<=', $now)->where(function ($query) use ($now) {
            $query->where('always', true)
                  ->orWhere('to', '>=', $now);
        });
    }

    /**
     * Scope for getting non approved reviews.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeNotApproved($query)
    {
        return $query->where('approved', false);
    }

    /**
     * Set machine scope.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeMachine($query)
    {
        $ip = Request::server('REMOTE_ADDR');
        $ip_forwarded = Request::server('HTTP_X_FORWARDED_FOR');
        $user_agent = Request::server('HTTP_USER_AGENT');

        return $query->whereIp($ip)->whereIpForwarded($ip_forwarded)->whereUserAgent($user_agent);
    }

    /**
     * If some message exists in last 30 seconds.
     *
     * @return bool
     */
    public function isExistInLastTime()
    {
        // protection time
        $time = Config::get('pcms.reviews::config.protection_time', '-30 seconds');
        $timeLimit = Carbon::parse($time)->toDateTimeString();

        // try to find some message
        $item = self::machine()->where('created_at', '>', $timeLimit)->first();

        return $item !== null;
    }

    /**
     * Generate unique hash for each review.
     *
     * @return string|null
     */
    public function getUniqueHash()
    {
        $length = Config::get('pcms.reviews::config.hash', 32);
        if ($length == 0) {
            return null;
        }

        return substr(md5('reviews-' . Str::random($length)), 0, $length);
    }

    public function getToValAttribute()
    {
        if ($this->always){
            return '<span class="reviews-status">'.'<span class="badge badge-success">' . Lang::get('pcms.reviews::lang.status.always') . '</span>'.'</span>';
        }else{
            return Carbon::parse($this->to)->format('d.m.Y');
        }
    }

    public function getStatusAttribute()
    {
        $status = '<span class="reviews-status">';
        $now = Carbon::now()->toDateString();
        if ($this->approved) {
            if ($this->always && $this->from <= $now) {
                $status .= '<span class="badge badge-success">' . Lang::get('pcms.reviews::lang.status.available') . '</span>';
            } else if ($this->always && $this->from >= $now) {
                $status .= '<span class="badge badge-secondary">' . Lang::get('pcms.reviews::lang.status.waiting') . '</span>';
            } else if ($this->to < $now) {
                $status .= '<span class="badge badge-warning">' . Lang::get('pcms.reviews::lang.status.expired') . '</span>';
            } else if ($this->from > $now) {
                $status .= '<span class="badge badge-secondary">' . Lang::get('pcms.reviews::lang.status.waiting') . '</span>';
            } else if ($this->from <= $now) {
                $status .= '<span class="badge badge-success">' . Lang::get('pcms.reviews::lang.status.available') . '</span>';
            } else {
                $status .= '<span class="badge badge-danger">' . Lang::get('pcms.reviews::lang.status.unapproved') . '</span>';
            }
        } else {
            $status .= '<span class="badge badge-danger">' . Lang::get('pcms.reviews::lang.status.unapproved') . '</span>';
        }
        $status .= '</span>';
        return $status;
    }
    
    public function scopeFilterCategories($query, $categories)
    {
        return $query->whereHas('categories', function($q) use ($categories) {
            $q->whereIn('pcms_reviews_reviews.id', $categories);
        });
    }
    
}
