<?php namespace Pcms\Reviews\Components;

use App;
use Cms\Classes\ComponentBase;
use Pcms\Reviews\Facades\ReviewsFacade;
use Pcms\Reviews\Models\Category;
use Lang;

class Reviews extends ComponentBase
{
    private $reviews;

    public function componentDetails()
    {
        return [
            'name' => Lang::get('pcms.reviews::lang.components.reviews.name'),
            'description' => Lang::get('pcms.reviews::lang.components.reviews.desc')
        ];
    }

    public function defineProperties()
    {
        return [
            'categoryFilter' => [
                'title' => Lang::get('pcms.reviews::lang.components.reviews.category'),
                'description' => Lang::get('pcms.reviews::lang.components.reviews.category_desc'),
                'type' => 'string',
                'default' => '{{ :category }}',
            ],
        ];
    }

    public function onRun()
    {
        // category filter
        $category = null;
        if ($categorySlug = $this->property('categoryFilter')) {
            $category = $this->getCategory($categorySlug);
        }
        $this->page['category'] = $category;
        $this->page['reviews'] = $this->reviews($category);
    }

    /**
     * Get reviews.
     *
     * @param Category $category Filter by category.
     *
     * @return mixed
     */
    public function reviews($category = null)
    {
        if ($this->reviews === null) {
            $this->reviews = $this->getFacade()->getApprovedReviews($category);
        }

        return $this->reviews;
    }

    /**
     * Get category by slug.
     *
     * @param $category
     *
     * @return mixed
     */
    public function getCategory($category)
    {
        return Category::where('slug', $category)->first();
    }

    /**
     * Get Facade.
     *
     * @return ReviewsFacade
     */
    private function getFacade()
    {
        return App::make('pcms.reviews.facade');
    }
}
