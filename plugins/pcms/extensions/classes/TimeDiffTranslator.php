<?php namespace Pcms\Extensions\Classes;

use App;
use October\Rain\Translation\Translator;
use Symfony\Component\Translation\TranslatorInterface;

class TimeDiffTranslator extends Translator implements TranslatorInterface
{
    public function trans($id, array $parameters = [], $domain = 'messages', $locale = null)
    {
        return $this->get('pcms.extensions::lang.' . $id, $parameters, $locale);
    }

    public function transChoice($id, $number, array $parameters = [], $domain = 'messages', $locale = null)
    {
        return $this->choice('pcms.extensions::lang.' . $id, $number, $parameters, $locale);
    }
}
