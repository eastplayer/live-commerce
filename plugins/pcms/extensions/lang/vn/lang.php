<?php

return [
    'plugin' => [
        'name' => 'Tiện ích mở rộng',
        'description' => 'Thêm bộ lọc cho website'
    ],
    'diff' => [
        'empty' => 'bây giờ',
        'ago' => [
            'year' => '1 năm trước|:count năm trước',
            'month' => '1 tháng trước|:count tháng trước',
            'day' => 'hôm qua|:count ngày trước',
            'hour' => '1 giờ trước|:count giờ trước',
            'minute' => '1 phút trước|:count phút trước',
            'second' => 'vừa xong|:count giây trước',
        ],
        'in' => [
            'second' => 'trong vài giây|trong :count giây',
            'hour' => 'trong 1 giờ|in :count hours',
            'minute' => 'trong 1 phút|trong :count phút',
            'day' => 'trong 1 ngày|trong :count ngày',
            'month' => 'trong 1 tháng|in :count tháng',
            'year' => 'trong 1 năm|trong :count năm',
        ],
    ],
];
